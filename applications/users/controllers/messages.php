<?php
namespace users;

class messages extends \Controller
{

    private $limit = 30;

    function default_method()
    {
        if ($this->id == "create") {
            $this->create_dialog();
        } else {
            switch ($_POST['act']) {
                case "get_form":
                    echo json_encode(array('success' => $this->layout_get("elements/message_form.html", array('id' => intval($_POST['id'])))));
                    break;
                case "delete_dialog":
                    $this->delete_dialog();
                    break;
                case "get_old_messages":
                    $this->get_old_messages();
                    break;
                case "get_form_invite":
                    $this->get_form_invite();
                    break;
                case "get_call":
                    $this->get_call();
                    break;
                case "get_call_window":
                    $this->get_call_window();
                    break;
                case "save_dialog_users":
                    $this->save_dialog_users();
                    break;
                default:
                    $this->show();
            }
        }
    }

    function show()
    {
        $this->set_global("user_menu", "messages");

        if (!$this->id) {
            $query = $this->db->prepare("SELECT id_dialog FROM dialogs_users WHERE id_user=? AND user_exit IS NULL");
            $query->execute(array($_SESSION['user']['id_user']));
            while ($row = $query->fetch()) {
                $dialogs[$row['id_dialog']] = $row;
                $ids[] = $row['id_dialog'];
            }

            if ($ids) {
                $ids = array_unique($ids);
                $ids = implode(",", $ids);

                $query = $this->db->query("select m.id_user,m.message,md.id_dialog,m.created from messages as m
                  LEFT JOIN messages_dialogs as md ON md.id_message=m.id
                  inner join
                  (select m.created,m.id_user,m.message, max(m.id) as maxid from messages as m
                    LEFT JOIN messages_dialogs as md ON md.id_message=m.id
                    group by md.id_dialog
                  ) as b ON m.id = b.maxid
                  where md.id_dialog IN({$ids})");
                while ($row = $query->fetch()) {
                    $dialogs[$row['id_dialog']]['message'] = $row;
                    $dialogs[$row['id_dialog']]['message']['message'] = cut($row['message'], 220);
                }

                uasort($dialogs, array($this, "sort_dialogs"));

                $query = $this->db->query("select id_dialog,u.id_user,u.first_name,u.last_name,u.avatar,u.gender
                    from dialogs_users as du
                    LEFT JOIN users as u ON du.id_user=u.id_user
                    where id_dialog IN({$ids})");
                while ($row = $query->fetch()) {
                    $dialogs[$row['id_dialog']]['users'][$row['id_user']] = $row;
                    $dialogs[$row['id_dialog']]['users'][$row['id_user']]['fio'] = build_user_name($row['first_name'], $row['last_name'], true);
                }
                $not_read = $this->get_not_read_count_group_dialog();
            }

            crumbs("Диалоги", "/users/messages", true);
            $this->layout_show("messages.html", array('dialogs' => $dialogs, 'sum' => $not_read['sum'], 'not_read' => $not_read['arr']));
        }
        else
        {
            $id = intval($this->id);
            if ($id > 0)
            {
                if ($this->user_dialog_exists($_SESSION['user']['id_user'], $this->id))
                {
                    $query = $this->db->prepare("SELECT m.*,u.first_name,u.last_name,u.gender,u.id_user,u.avatar FROM messages AS m
                    LEFT JOIN messages_dialogs AS md ON md.id_message=m.id
                    LEFT JOIN users AS u ON u.id_user=m.id_user
                    WHERE md.id_dialog=? AND md.id_user=? ORDER BY m.created DESC LIMIT 20");
                    $query->execute(array($this->id, $_SESSION['user']['id_user']));
                    while ($row = $query->fetch()) {
                        $messages[$row['id']] = $row;
                        $messages[$row['id']]['fio'] = build_user_name($row['first_name'], $row['last_name']);
                    }
                    if ($messages) $messages = array_reverse($messages);

                    $query = $this->db->prepare("SELECT u.first_name,u.last_name,u.gender,u.id_user,u.avatar
                      FROM dialogs_users AS du
                      LEFT JOIN users AS u ON u.id_user=du.id_user
                      WHERE id_dialog=?");
                    $query->execute(array($this->id));
                    $i = 1;
                    while ($row = $query->fetch()) {
                        $users[$row['id_user']] = $row;
                        $users[$row['id_user']]['fio'] = build_user_name($row['first_name'], $row['last_name']);
                        if ($_SESSION['user']['id_user'] != $row['id_user'] && $i < 5) {
                            $crumbs[] = build_user_name($row['first_name'], $row['last_name']);
                            $i++;
                        }
                    }

                    $count_users = count($users);
                    if ($count_users > 4) {
                        $count_users = count($users) - $i + 1;
                        if ($count_users > 0) $and_other = " и другими ({$count_users})";
                    }

                    crumbs("Диалоги", "/users/messages", true);
                    crumbs("Диалог c " . implode(", ", $crumbs) . $and_other);

                    $query = $this->db->prepare("SELECT count(m.id) AS count FROM messages AS m
                     LEFT JOIN messages_dialogs AS md ON md.id_message=m.id
                     WHERE md.id_dialog=? AND md.id_user=?");
                    $query->execute(array($this->id, $_SESSION['user']['id_user']));
                    $count = $query->fetch();

                    //clear count of new messages from this opponent
                    $query = $this->db->prepare("UPDATE messages_dialogs SET user_read='1' WHERE id_user=? AND id_dialog=? AND user_read IS NULL");
                    $query->execute(array($_SESSION['user']['id_user'], $this->id));

                    $g = new \Global_module();
                    $g->run(false,"count_messages");

                    $data = array('messages' => $messages, 'id_dialog' => $this->id, 'count' => $count['count'], 'users' => $users);
                    $this->layout_show('dialog.html', $data);
                }
                else $this->error_page('404');
            }
            else $this->error_page('404');
        }
    }

    function create_dialog()
    {
        $html = $this->get_form_invite();
        crumbs("Создание диалога");
        $this->layout_show('dialog_create.html', array('html' => $html));
    }

    function get_old_messages()
    {
        $last_message = intval($_POST['last']);

        $query = $this->db->prepare("SELECT m.*,u.first_name,u.last_name,u.gender,u.id_user,u.avatar FROM messages AS m
            LEFT JOIN messages_dialogs AS md ON m.id=md.id_message
            LEFT JOIN users AS u ON u.id_user=m.id_user
            WHERE md.id_dialog=? AND m.id < ? AND md.id_user=? GROUP BY m.id ORDER BY m.created DESC LIMIT 20");
        $query->execute(array($_POST['id_dialog'], $last_message, $_SESSION['user']['id_user']));
        while ($row = $query->fetch()) {
            $row['fio'] = build_user_name($row['first_name'], $row['last_name']);
            $messages[] = $row;
        }
        if ($messages) $messages = array_reverse($messages);

        $res['success']['html'] = $this->layout_get("elements/dialog_message.html", array('messages' => $messages));
        $res['success']['count'] = count($messages);

        echo json_encode($res);
    }

    function delete_dialog()
    {
        $this->db->beginTransaction();

        $query = $this->db->prepare("UPDATE dialogs_users SET user_exit='1' WHERE id_dialog=? AND id_user=?");
        if (!$query->execute(array($_POST['id_dialog'], $_SESSION['user']['id_user'])))
            $res['error'] = "Произошла ошибка при попытке удалить диалог";

        $query = $this->db->prepare("DELETE FROM messages_dialogs WHERE id_dialog=? AND id_user=?");
        if (!$query->execute(array($_POST['id_dialog'], $_SESSION['user']['id_user'])))
            $res['error'] = "Произошла ошибка при попытке удалить диалог";

        if ($res['error']) $this->db->rollBack();
        else {
            $this->db->commit();
            $res['success'] = true;
        }

        echo json_encode($res);
    }

    function get_not_read_count($id_user=false,$time=false)
    {
        if (!$id_user) $id_user = $_SESSION['user']['id_user'];
        if ($time) $where = " and m.created >=".$time;

        $query = $this->db->prepare("SELECT count(m.id) AS count
            FROM messages_dialogs as md
            LEFT JOIN messages as m ON m.id=md.id_message
            WHERE md.id_user=? AND md.user_read IS NULL {$where}
        ");
        $query->execute(array($id_user));
        $count = $query->fetch();
        return $count['count'];
    }

    function get_not_read_count_group_dialog()
    {
        $query = $this->db->prepare("SELECT count(id_message) AS count,id_dialog FROM messages_dialogs WHERE id_user=? AND user_read IS NULL GROUP BY id_dialog");
        $query->execute(array($_SESSION['user']['id_user']));
        $sum = 0;
        while ($row = $query->fetch()) {
            $d[$row['id_dialog']] = $row['count'];
            $sum += $row['count'];
        }
        return array('arr' => $d, 'sum' => $sum);
    }

    function get_form_invite()
    {
        if (isset($_POST['search']) && $_POST['search'] != '') {
            $search = str_replace(" ", "%", $_POST['search']);
            $s = $this->db->quote("%{$search}%");
            $where[] = "(u.first_name LIKE {$s} OR u.last_name LIKE {$s})";
        }

        $ids_from_post = count($_POST['ids']);

        if ($ids_from_post) {
            foreach ($_POST['ids'] as $v) {
                $ids[] = intval($v);
            }
        }

        if ($_POST['id_dialog']) {
            $query = $this->db->prepare("SELECT u.first_name,u.last_name,u.gender,u.id_user,u.avatar FROM dialogs_users du
              LEFT JOIN users AS u ON du.id_user=u.id_user
              WHERE du.id_dialog=?
              ");
            $query->execute(array($_POST['id_dialog']));
            while ($row = $query->fetch()) {
                $users_dialog[] = $row;
                $ids[] = $row['id_user'];
            }

        }
        else $where[] = "cu.id_user !=".$_SESSION['user']['id_user'];

        if (count($ids) > 0) $where[] = "cu.id_user NOT IN (" . implode(",", $ids) . ")";
        if (count($where) > 0) $where = "and " . implode(" AND ", $where);
        else $where = "";

        require_once(ROOT . 'libraries/paginator/paginator.php');

        $query = $this->db->prepare("select SQL_CALC_FOUND_ROWS u.first_name,u.last_name,u.gender,u.id_user,u.avatar from company_users cu
          LEFT JOIN users as u ON cu.id_user=u.id_user
          where id_company IN (select id_company from company_users where id_user=?) {$where}
          GROUP by u.id_user
          LIMIT {$this->limit}
          OFFSET " . \Paginator::get_offset($this->limit, $_POST['page']) . "
          ");
        $query->execute(array($_SESSION['user']['id_user']));
        $users = $query->fetchAll();

        $paginator = new \Paginator($this->db->found_rows(), $_POST['page'], $this->limit);

        $data = array(
            'users' => $users,
            'users_dialog' => $users_dialog,
            'paginator' => $paginator,
            'search' => $_POST['search'],
            'id_dialog' => $_POST['id_dialog'],
            'ids' => $ids,
        );

        if (!$_POST['no_search_input']) {
            $res['success'] = $this->layout_get("elements/invite_dialog.html", $data);
        } else $res['success'] = $this->layout_get("elements/users_table_invite.html", $data);

        if ($_POST) echo json_encode($res);
        else return $res['success'];
    }

    function get_call()
    {
        $query = $this->db->prepare("SELECT u.first_name,u.last_name,u.gender,u.id_user,u.avatar FROM dialogs_users du
          LEFT JOIN users AS u ON du.id_user=u.id_user
          WHERE du.id_dialog=?
          ");
        $query->execute(array($_POST['id_dialog']));
        while ($row = $query->fetch()) {
            $ids[] = $row['id_user'];
        }

        $res['success'] = $this->layout_get("elements/call.html", array('users' => $ids));
        echo json_encode($res);
    }

    function save_dialog_users()
    {
        $this->db->beginTransaction();
        $_POST['id_dialog'] = intval($_POST['id_dialog']);

        if ($_POST['id_dialog'] > 0 && !$this->user_dialog_exists($_SESSION['user']['id_user'], $_POST['id_dialog'])) $res['error'] = 'Вы не участвуете в этом диалоге';
        else if ($_POST['id_dialog'] < 1)
        {
            $_POST['users'][] = $_SESSION['user']['id_user'];
            $dialog = $this->check_dialog_exists($_POST['users']);
            if (!$dialog)
            {
                if (!$this->db->query("insert into dialogs(id) values(null)")) $res['error'] = "Ошибка базы данных";
                else
                {
                    $_POST['id_dialog'] = $this->db->lastInsertId();
                }
            }
        }

        if (!$dialog)
        {
            $dialog = $_POST['id_dialog'];
            if (!$res['error'])
            {
                $query = $this->db->prepare("INSERT INTO dialogs_users (id_user,id_dialog) VALUES(?,?) ON DUPLICATE KEY UPDATE id_user=id_user,id_dialog=id_dialog");
                foreach ($_POST['users'] as $v) {
                    if ($v != "") {
                        if (!$query->execute(array($v, $_POST['id_dialog']))) $res['error'] = "Ошибка базы данных";
                    }
                }
            }
        }

        if ($res['error']) $this->db->rollBack();
        else {
            $this->db->commit();
            $res['success'] = $dialog;
        }

        echo json_encode($res);
    }

    function check_dialog_exists($users = array())
    {
        $count = count($users);
        if ($count < 2) return false;

        foreach ($users as &$u)
        {
            $u = $this->db->quote($u);
        }

        $ids = implode(",",$users);

        $query = $this->db->prepare(" SELECT id_dialog
            FROM   dialogs_users a
            WHERE  id_user IN ({$ids}) AND
            EXISTS
            (
               SELECT  1
                 FROM    dialogs_users b
                 WHERE   a.id_dialog = b.id_dialog
                 GROUP   BY id_dialog
                 HAVING  COUNT(DISTINCT id_user) = ?
             )
             GROUP  BY id_dialog
             HAVING COUNT(*) = ?"
        );

        $query->execute(array($count,$count));
        if ($dialog = $query->fetch())
        {
            $query = $this->db->prepare("update dialogs_users set user_exit=NULL WHERE id_dialog=?");
            $query->execute(array($dialog['id_dialog']));
            return $dialog['id_dialog'];
        }
        else return false;
    }

    function user_dialog_exists($id_user, $id_dialog)
    {
        $query = $this->db->prepare("SELECT * FROM dialogs_users WHERE id_user=? AND id_dialog=? AND user_exit IS NULL");
        $query->execute(array($id_user, $id_dialog));
        if ($query->fetch()) return true;
    }

    function sort_dialogs($a, $b)
    {
        return $b['message']['created'] - $a['message']['created'];
    }

    function get_call_window()
    {
        $query = $this->db->prepare("SELECT first_name,last_name,avatar FROM users WHERE id_user=?");
        $query->execute(array($_SESSION['user']['id_user']));
        $from = $query->fetch();

        $res['success'] = $this->layout_get("elements/call_popup.html", array(
            "from" => $_SESSION['user']['id_user'],
            'name' => build_user_name($from['first_name'], $from['last_name'])
        ));
        echo json_encode($res);
    }
}