<?php
namespace pages;

class pages extends \Controller {

    function default_method($get=false)
    {
        if ($this->id)
        {
            if (is_numeric($this->id)) $query = $this->db->prepare("select * from pages as p
                LEFT JOIN pages_text as pt ON pt.id_page=p.id
                where p.id=? and pt.main IS NOT NULL");
            else $query = $this->db->prepare("select * from pages as p
                LEFT JOIN pages_text as pt ON pt.id_page=p.id
                where url=? and pt.main IS NOT NULL");
            $query->execute(array($this->id));
            if ($page = $query->fetch())
            {
                $page['text'] = htmlspecialchars_decode(preg_replace('/<pre>.*?<code.*?>(.*)?<\/code>.*?<\/pre>/siu', '$1', $page['text']));
                if ($get) return $page;
                else
                {
                    $data['page'] = $page;
                    $data = $data+$this->rules($page['template'],$page['id_page']);
                    $this->layout_show("templates/{$page['template']}.html",$data);
                }
            }
            else $this->error_page();

        }
        else $this->error_page();
    }

    function get_layout($layout)
    {
        $this->layout = false;
        return $this->layout_get($layout);
    }

    function rules($template,$id)
    {
        $data = array();
        if ($template == "with_left_sidebar")
        {
            /** @var  $pages_ctr \admin\pages\pages */
            $pages_ctr = $this->get_controller("pages","pages",true);
            $parents = $pages_ctr->parents($this->id,true);
            if (count($parents) > 1) $root_parent = end($parents);
            else $root_parent = $id;

            $query = $this->db->query("select id,path,name from pages where id IN(".implode(",",$parents).") order by field('id',".implode(",",$parents).")");
            $parents_names = $query->fetchAll();

            $data['menu'] = $pages_ctr->get_nested_pages($root_parent);
            foreach ($parents_names as $n)
            {
                crumbs($n['name'],$n['path']);
            }
        }
        return $data;
    }

    function update_cache_pages()
    {
        $query = $this->db->prepare("select id,name,path from pages");
        $query->execute();
        while ($row = $query->fetch())
        {
            $pages[$row['path']] = $row;
        }
        \Cache::connect()->set('pages',$pages,100000);
        return $pages;
    }
}

