<?php

use Phinx\Migration\AbstractMigration;

class FixDpPosition extends AbstractMigration
{
    public function up()
    {
		$this->query('UPDATE dp
			set position = "0"
            WHERE name = "планирование"
            LIMIT 1
        ');
    }

    public function down()
	{

	}
}
