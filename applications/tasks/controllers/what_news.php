<?php
namespace tasks;

require_once(__DIR__ .'../../../../index.php');

class what_news extends \Controller {

    private $id;

    function default_method($id,$key)
    {
        /** @var $task_cr \tasks\tasks */
        $task_cr = $this->get_controller("tasks");
        /** @var  $task_error_cr \tasks\error */
        $task_error_cr = $this->get_controller("tasks","error");
        if ($task = $task_cr->get_task($id))
        {
            $task_cr->get_options();
            $pid = getmypid();

            if ($key == get_setting('cron_key') && !$task_cr->is_process_running($task['pid']))
            {
                $this->id = $task['id'];
                $GLOBALS['cli_task_id'] = $this->id;
                $task_cr->set_status($id,"run",$task['period'],$pid);

                /* @var $comments_ctr \projects\comments */
                $comments_ctr = $this->get_controller("projects","comments");
                /** @var  $messages_ctr \users\messages*/
                $messages_ctr = $this->get_controller("users","messages");
                /** @var  $entity_ctr \projects\entities */
                $entity_ctr = $this->get_controller("projects","entities");
                /** @var  $task_ctr \tasks\tasks */
                $task_ctr = $this->get_controller("tasks");

                $task = $task_ctr->get_task_from_controller("what_news");
                $time = $task['completed'];
                if (!$time) $time = time();

                $from = get_setting('email');
                $query = $this->db->query("select u.id_user,u.email,u.last_open_company,company.name from users as u
                    LEFT JOIN company ON company.id=u.last_open_company
                    where mailconfirm = 1
                ");
                while ($user = $query->fetch())
                {
                    $data = array();
                    $data['domain'] = get_full_domain_name($user['name']);
                    $data_raw = $comments_ctr->get_not_read_count($user['id_user'],false,$time);
                    if ($data_raw)
                    {
                        $data_aggregate = array();
                        foreach ($data_raw as $item) {
                            $data_aggregate[$item['id_company']]['name'] = $item['company_name'];
                            $data_aggregate[$item['id_company']]['items'][] = $item;
                        }

                        $data['comments'] = $data_aggregate;
                    }

                    $data_raw = $messages_ctr->get_not_read_count($user['id_user'],$time);
                    if ($data_raw)
                    {
                        $data['messages'] = $data_raw;
                    }

                    $data_raw = $entity_ctr->get_new_entities($user['id_user'],$time);
                    if ($data_raw)
                    {
                        $data['other'] = $data_raw;
                    }

                    if (count($data) > 1)
                    {
                        $html = $this->layout_get("what_news.html",array('data' => $data));
                        if (!send_mail($from, $user['email'], "Что нового?", $html, get_setting('site_name'))) echo "error {$user['email']}\n\r";
                    }
                }

                $task_cr->set_status($id,"stand");
            }
        }
        else
        {
            $task_error_cr->set_error("Неверный ключ",$this->id);
        }
    }
}

/** @var  $run \tasks\what_news */
$run = \Controller::get_controller("tasks","what_news");
$run->default_method($argv[1],$argv[2]);
