<?php

use Phinx\Migration\AbstractMigration;

class InsertOptions extends AbstractMigration
{
    public function up()
    {
		$data = [
			'name' => 'Названия',
			'id_parent' => 0
		];

		$this->insert('group_options', $data);
    }

    public function down()
	{

	}
}
