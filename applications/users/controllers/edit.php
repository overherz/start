<?php
namespace users;
class edit extends \Controller {

    function default_method() {
        switch($_POST['act'])
        {
            case "save_profile":
                $this->save_profile();
                break;
            case "change_password":
                $this->change_password();
                break;
            default: $this->profile();
        }
    }
    
    function profile()
    {
        $id = $_SESSION['user']['id_user'];
        $user = $this->get_controller("users")->get_user($id);

        crumbs("Мои данные");

        if (!$user) $this->redirect('/users/');
        if (!$_SESSION['user'] || $_SESSION['user']['id_user'] != $user['id_user']) $this->redirect("/users/~{$user['id_user']}/");

        $query = $this->db->prepare("select pr.*, up.* FROM profile as pr LEFT JOIN (SELECT userprofiles.* from userprofiles WHERE userprofiles.iduser = ?) as up ON up.idprof=pr.id");
        $query->execute(array($id));

        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");
        $regions = $user_ctr->get_regions();
        $cities = $user_ctr->get_cities_region($user['region']);

        $this->layout_show('edit.html', array('user'=>$user,'regions' => $regions,'cities' => $cities));
    }

    function save_profile()
    {
        $u_ctr = $this->get_controller("users");
        $user = $u_ctr->get_user($_POST['id']);

        if (!$user) $res['error'][] = "Пользователь не найден";
        if (!$_SESSION['user'] || $_SESSION['user']['id_user'] != $user['id_user']) $res['error'][] = "У Вас недостаточно прав";

        $data = $this->prepare($_POST);
        if ($data['errors'])
        {
            foreach ($data['errors'] as $r) $res['error'][] = $r;
        }
        $user = $data['user'];

        $city = explode("|",$_POST['city']);

        if (!$res['error'])
        {
            $this->db->beginTransaction();
            $query = $this->db->prepare("update users set fio=?,region=?,city=?,areacode=?,locality=?,name_oo=?,position=? WHERE id_user=? LIMIT 1");
            if (!$query->execute(
                array(
                    $user['fio'],
                    $user['region'],
                    $city[0],
                    $city[1],
                    $user['locality'],
                    $user['name_oo'],
                    $user['position'],
                    $_POST['id']
                )
            )) $res['error'][] = "Ошибка базы данных";
            else
            {
                if (!$res['error'])
                {
                    $this->db->commit();
                    $query = $this->db->prepare("select u.* from users as u where id_user=?");
                    $query->execute(array($_POST['id']));
                    $u = $query->fetch();

                    $_SESSION['user'] = $u;
                    $res['success'] = $_POST['id'];
                }
                else $this->db->rollBack();
            }
        }

        echo json_encode($res);
    }

    function change_password()
    {
        if ($_POST['new_pass'] == "") $res['error'] = "Укажите новый пароль";
        if ($_POST['old_pass'] == "") $res['error'] = "Укажите старый пароль";
        if ($_POST['new_pass'] != $_POST['repeat_pass']) $res['error'] = "Пароли не совпадают";

        if (!$res['error'])
        {
            $query = $this->db->prepare("select pass,salt,email FROM users WHERE id_user=?");
            $query->execute(array($_SESSION['user']['id_user']));
            if ($user = $query->fetch())
            {
                $login_ctrl = $this->get_controller("users","login");
                if ($login_ctrl->get_hash($_POST['old_pass'],$user['salt']) == $user['pass'])
                {
                    $get_pass = get_pass($_POST['new_pass']);
                    $query = $this->db->prepare("update users set pass=?,salt=? WHERE id_user=?");
                    if ($query->execute(array($get_pass['password'], $get_pass['salt'], $_SESSION['user']['id_user'])))
                    {
                        setcookie('login', $user['email'], time()+60*60*24*90,"/",get_cookie_domain(),null,true);
                        setcookie('password', $get_pass['password'], time()+60*60*24*90,"/",get_cookie_domain(),null,true);
                        $res['success'] = true;
                    }
                    else $res['error'] = "Ошибка базы данных";
                }
                else $res['error'] = "Старый пароль неверен";
            }
            else $res['error'] = "Пользователь не найден";
        }
        echo json_encode($res);
    }

    function prepare($user)
    {
        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");

        if (is_array($user))
        {
            foreach ($user as &$u)
            {
                $u = trim($u);
            }
        }

        if ($user['fio'] == "") $errors['fio'] = "Введите ФИО";
        if ($user['name_oo'] == "") $errors['name_oo'] = "Введите название образовательного учреждения";
        if ($user['position'] == "") $errors['position'] = "Введите должность";
        if (!$user_ctr->get_region($user['region'])) $errors['region'] = 'Укажите регион';

        if ($user['city'] == "" && $user['locality'] == "")
        {
            $errors['city'] = "Укажите город или населенный пункт";
        }

        unset($user['act']);
        unset($user['id']);

        return array('user' => $user,'errors' => $errors);
    }
}

