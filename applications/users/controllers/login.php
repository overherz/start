<?php
namespace users;

use admin\black_list\black_list;
use admin\logs\logs;

class login extends \Controller {
    
    function default_method()
    {
        switch ($_POST['act'])
        {
            case "login":
                $this->get_login($_POST['email'],$_POST['password'],true);
                break;
            default:
                $this->default_show();
        }
    }

    function default_show()
    {
        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");

        if ($_SESSION['user']['id_user']) $this->redirect();
        $page = $this->id == 'start' ? 'login_for_start.html' : 'login.html';
        $this->layout_show($page, array('regions' => $user_ctr->get_regions()));
    }

    function get_login($login,$pass,$post=false)
    {
        /** @var  $ban black_list */
        $ban = $this->get_controller("black_list",false,true);
        /** @var  $log \logs\logs */
        $log = $this->get_controller("logs");

        if ($post)
        {
            if ($login == "") $res['error']['login'] = "Логин пустой";
            if ($pass == "") $res['error']['password'] = "Пароль пустой";
        }

        if ($ban->check_ban()) $res['error']['denied'] = "За многократные неудачные попытки авторизации Вы забанены на 5 минут";

        if (!$res['error'])
        {
            $query = $this->db->prepare("select u.*,g.access_site,g.name as group_name
                from users as u
                LEFT JOIN groups as g ON g.id=u.id_group
                where email=?
                group by u.id_user
                ");
            $query->execute(array($login));

            if ($u = $query->fetch())
            {
                if ($post) $hash = $this->get_hash($pass,$u['salt']);
                else $hash = $pass;

                if ($hash == $u['pass'])
                {
                    $u['access'] = (array) json_decode($u['access_site']);
                    if ($u['access']['authorization'] || $u['id_group'] == 1)
                    {
                        if ($u['mailconfirm'] == 1)
                        {
                            if (($post && $_POST['cookie']) || ($_COOKIE['login'] != "" && $_COOKIE['password'] != "" && $_SESSION['user']))
                            {
                                setcookie('login', $login, time()+60*60*24*90,"/",get_cookie_domain(),null,true);
                                setcookie('password', $u['pass'], time()+60*60*24*90,"/",get_cookie_domain(),null,true);
                            }

                            unset($u['salt']);unset($u['access_site']); unset($u['pass']);
                            $_SESSION['user'] = $u;

                            if ($_POST['redirect'] != "") $res['success']['redirect'] = $_POST['redirect'];
                            else $res['success'] = true;
                            if (!$post) return true;
                        }
                        else
                        {
                            $res['error']['mailconfirm'] = "Вам необходимо подтвердить Email. Вам была отправлена новая ссылка.";
                            /** @var  $reg_ctr \users\registration */
                            $reg_ctr = $this->get_controller("users","registration");
                            $reg_ctr->send_activate_link($u['email']);
                        }
                    }
                    else $res['error']['denied'] = "Вам вход запрещен";
                }
                else
                {
                    $res['error']['denied'] = "Данные неверны";
                    $log->save_into_log("site","Авторизация",false,false,$u['id_user']);
                    $ban->check_for_ban();
                }
            }
            else
            {
                $res['error']['denied'] = "Данные неверны";
                $log->save_into_log("site","Авторизация",false,false);
                $ban->check_for_ban();
            }
        }

        if ($post) echo json_encode($res);
    }

    function update_access()
    {
        if ($_SESSION['user'])
        {
            if ($data = $this->db->query("select g.id as id_group,g.access_site,u.timezone
                    from users as u
                    LEFT JOIN groups as g ON g.id=u.id_group
                    where u.id_user='{$_SESSION['user']['id_user']}'
                    group by u.id_user
            ")->fetch())
            {
                $data['access_site'] = (array) json_decode($data['access_site']);
                foreach ($data as $k => $v) $_SESSION['user'][$k] = $v;
                date_default_timezone_set($_SESSION['user']['timezone']);
            }
            else unset ($_SESSION['user']);
        }
    }

    function get_hash($pass,$salt)
    {
        return md5(md5($pass).md5($salt));
    }
}

