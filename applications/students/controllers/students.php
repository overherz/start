<?php
namespace students;

class students extends \Controller {

    function default_method()
    {
        switch($_POST['act'])
        {
            case "get_form_edit":
                $this->get_form_edit();
                break;
            case "save":
                $this->save();
                break;
            case "delete":
                $this->delete();
                break;
        }
    }

    function get_all()
    {
        $students = array();
        $query = $this->db->prepare("select * from students ORDER BY fio ASC");
        $query->execute();
        while ($row = $query->fetch())
        {
            $students[$row['id']] = $row;
        }

        return $students;
    }

    function get_students_from_ug($ug, $begin = null)
    {
    	$conditions = [];
    	if (!empty($begin)) {
			$conditions[] = 'begin_start <=' . $this->db->quote($begin);
			$conditions[] = '(begin_end >=' . $this->db->quote($begin) .' OR begin_end IS NULL)';
		}

		if (!empty($conditions)) {
    		$conditions = ' AND ' . implode(' AND ', $conditions);
		} else {
    		$conditions = '';
		}

        $students = array();
        $query = $this->db->prepare("select * from students where id_study_group=? {$conditions} ORDER BY fio ASC");
        $query->execute(array($ug));
        while ($row = $query->fetch()) {
            $students[$row['id']] = $row;
        }

        return $students;
    }

    function get_student($id)
    {
        $query = $this->db->prepare("select * from students where id=?");
        $query->execute(array($id));
        return $query->fetch();
    }

    function get_form_edit()
    {
        if ($_POST['id']) {
            $data['students'][0] = $this->get_student($_POST['id']);
        } else {
            $data['students'][0] = [];
        }

        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller('ug');
        $ug = $ug_ctr->get_ug($_POST['id_study_group']);

        $where[] = $ug['type'];

        if ($ug['type'] == TYPE_UUD) {
			$data['begin'] = $_POST['begin'] = $_POST['begin'] ? $_POST['begin'] : 1;

            /** @var  $dp_ctr \dp\dp */
            $dp_ctr = $this->get_controller('dp');
            $data += $dp_ctr->get_all_uud_dp();

			if (!empty($_POST['id'])) {
				/** @var  $results_ctr \results\results */
				$results_ctr = $this->get_controller("results");
				$data['results'] = $results_ctr->get_student_result($_POST['id']);
			}

			$data['results'] = [$data['results']]; // иначе фильтрация не пройдет
			$dp_ctr->filter_uud_data($data);
			$data['results'] = reset($data['results']);

            // Костыль для 5ого класса для вывода другой сортировки
			if ($data['begin'] == 5) {
				$temp_dp = [];
				foreach ($data['dp'] as $gdp => &$dp) {
					foreach ($dp as $id_dp => &$dp_data) {
						if (!empty($dp_data['extra'])) {
//								$dp_data['sort_name'] = 'яяяяя'.$dp_data['extra']['fields'][0]; // яя нужны для правильной сортировки, выводится они не будут
							$dp_data['sort_name'] = str_replace('У', 'я', $dp_data['extra']['name']) . implode(';', $dp_data['extra']['fields']);
							$temp_dp[$id_dp] = $dp_data;
						}
					}
				}
				unset($dp);
				unset($dp_data);

				uasort($temp_dp, function($a, $b) {
					return strnatcmp($a['sort_name'], $b['sort_name']);
				});

			//	pr($temp_dp);

				$data['dp'][0] = $temp_dp;
				$data['gdp'] = [0 => 'not_show'];
				unset($temp_dp);
			}

        } else {
            $query = $this->db->prepare("select dp.*,gdp.name as gdp_name
                from dp as dp
                join gdp as gdp ON dp.id_gdp=gdp.id AND gdp.type=?
                order by position ASC
            ");

            $query->execute($where);

            while ($row = $query->fetch()) {
                $data['gdp'][$row['id_gdp']] = $row['gdp_name'];
                $data['dp'][$row['id_gdp']][] = $row;
            }

			if (!empty($_POST['id'])) {
				/** @var  $results_ctr \results\results */
				$results_ctr = $this->get_controller("results");
				$data['results'] = $results_ctr->get_student_result($_POST['id']);
			}
        }

        $data['edit'] = true;
        $data['id_study_group'] = $_POST['id_study_group'];

        if ($ug['type'] == TYPE_UUD) {
            $res['success'] = $this->layout_get("uud/report_uud1.html", $data, "results");
        } else {
            $res['success'] = $this->layout_get("start/report1.html", $data, "results");
        }

        echo json_encode($res);
    }

    function save()
    {
        $res = [];
        $_POST['fio'] = trim($_POST['fio']);

        if ($_POST['fio'] == '') $res['error'][] = "ФИО не может быть пустым";
        if ($_POST['id_study_group'] == '') $res['error'][] = "Учебная группа не может быть пустой";

        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller("ug");
        $ug = $ug_ctr->get_ug($_POST['id_study_group']);
        if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $_POST['id_study_group'])) {
            $res['error'][] = "Нет доступа";
        }

        $clear_data = [];
        $query_clear_result = $query_insert_result = '';
        if ($ug['type'] == TYPE_START) {
            $query_clear_result = $this->db->prepare("delete from results where id_student=?");
            $query_insert_result = $this->db->prepare("insert into results(id_student, id_dp, value) values(?,?,?)");
            $clear_data = [
                $_POST['id'],
            ];
        } else if ($ug['type'] == TYPE_UUD) {
            $query_clear_result = $this->db->prepare("delete from results_uud where id_student=? AND class=?");
            $query_insert_result = $this->db->prepare("insert into results_uud(id_student, id_dp, class, field, value) values(?,?,?,?,?)");
            $clear_data = [
                $_POST['id'],
                $_POST['class'],
            ];
        }

        if (count($_POST['dp']) > 0 && !$_POST['force_save']) {
            foreach ($_POST['dp'] as $k => $dp) {
                if ($dp == "") {
                    $res['error']['force_save'] = true;
                    break;
                }
            }
        }

        if (empty($_POST['begin_start'])) {
			$_POST['begin_start'] = null;
		}

		if (empty($_POST['begin_end'])) {
			$_POST['begin_end'] = null;
		}

        $this->db->beginTransaction();
        if (!$res['error']) {
            $save_data = [
                $_POST['fio'],
                $_POST['contacts'],
				$_POST['begin_start'],
				$_POST['begin_end'],
            ];

            $id_student = null;
            if ($_POST['id'] != "") {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE students set fio=?,contacts=?, begin_start=?, begin_end=? where id=?");
                if (!$query->execute($save_data)) {
                    $res['error'][] = "Ошибка базы данных";
                }
                $id_student = $_POST['id'];
            }
            else {
                $save_data[] = $_POST['id_study_group'];
                $query = $this->db->prepare("INSERT INTO students(fio,contacts,begin_start, begin_end, id_study_group) VALUES(?,?,?,?,?)");
                if (!$query->execute($save_data)) {
                    $res['error'][] = "Ошибка базы данных";
                } else {
                    $id_student = $this->db->lastInsertId();
                }
            }

            if (count($_POST['dp']) > 0) {
                if ($_POST['id'] != "") {
                    if (!$query_clear_result->execute($clear_data)) {
                        $res['error'][] = "Ошибка базы данных";
                    }
                }

                if ($ug['type'] == TYPE_START) {
                    foreach ($_POST['dp'] as $k => $dp) {
                        if ($dp == "") {
                            $dp = null;
                        }

                        if ($dp != "" && ($dp < 0 || $dp > 2)) {
                            $res['error'][] = "Некорректное значение";
                        }

                        if (!empty($id_student) && !$query_insert_result->execute(array($id_student, $k, $dp))) {
                            $res['error'][] = "Ошибка базы данных";
                        }
                    }
                } elseif ($ug['type'] == TYPE_UUD) {
					/** @var  $dp_ctr \dp\dp */
					$dp_ctr = $this->get_controller('dp');
					$max_values = $dp_ctr->get_max_uug_dp_fields_values();

                    foreach ($_POST['dp'] as $id_dp => $dp) {
                        foreach ($dp as $field_name => $value) {
                            if ($value == "") {
                                $value = null;
                            } else {
                            	$value = str_replace(',', '.', $value);
								$max_value = $max_values[$id_dp][$_POST['class']][$field_name];

								if (!is_null($value)) {
									if (strstr($value, '.')) {
										$value = (float) $value;
										if (fmod($value,0.5) != 0) {
											$res['error'][] = "Некорректное значение";
										}
									}

									if ($value < 0 || $value > $max_value) {
										$res['error'][] = "Превышено максимальное значение";
									}
								}
							}

                            $query_insert_result = $this->db->prepare("insert into results_uud(id_student, id_dp, class, field, value) values(?,?,?,?,?)");
                            if (!empty($id_student) && !$query_insert_result->execute([
                                    $id_student,
                                    $id_dp,
                                    $_POST['class'],
                                    $field_name, $value
                                ]))
                            {
                                $res['error'][] = "Ошибка базы данных";
                            }
                        }
                    }

                    if (!$this->save_groups($_POST['dp'], $_POST['class'], $id_student)) {
                        $res['error'][] = "Ошибка базы данных";
                    }
                }
            }
        }

        if (!$res['error'])
        {
            $res['success']['status'] = true;
            if ($ug['type'] == TYPE_UUD) {
            	$res['success']['begin'] = $_POST['class'];
			}
            $this->db->commit();
        }
        else $this->db->rollBack();

        echo json_encode($res);
    }

    function save_groups($data, $class, $id_student)
    {
        $error = false;
        $data_groups = [];
        $total = [];
        foreach ($data as $id_dp => $dp) {
            foreach ($dp as $field_name => $value) {
                $group = &$data_groups[$id_dp];

                if ($value == '') {
                	$group = null;
                	continue;
				}

                if ($class == 1) {
                    if (in_array($field_name, ['Б', 'В'])) {
                        $field_name = 'БВ';
                    }

                    $total[$id_dp][$field_name] += $value;

                    $bv = $total[$id_dp]['БВ'];
                    $a = $total[$id_dp]['А'];

                    if ($bv > 2) {
                        if ($a == 2) {
                            $group = 1;
                        } else {
                            $group = 3;
                        }
                    } else {
                        if ($a == 2) {
                            $group = 2;
                        } else {
                            $group = 4;
                        }
                    }

                } else {
                    if (in_array($field_name, ['А', 'Б'])) {
                        $field_name = 'АБ';
                    }

                    $total[$id_dp][$field_name] += $value;

                    $ab = $total[$id_dp]['АБ'];
                    $v = $total[$id_dp]['В'];

                    if ($ab > 2) {
                        if ($v == 2) {
                            $group = 1;
                        } else {
                            $group = 2;
                        }
                    } else {
                        if ($v == 2) {
                            $group = 3;
                        } else {
                            $group = 4;
                        }
                    }
                }
                unset($group);
            }
        }

        $query_delete = $this->db->prepare("DELETE FROM results_uud_group WHERE id_student=? AND class=?");
        $query = $this->db->prepare("INSERT INTO results_uud_group (id_student, class, id_dp, value) VALUES (?,?,?,?)");

        if (!empty($data_groups)) {
            if (!$query_delete->execute([
                $id_student,
                $class,
            ])) {
                $error = true;
            }

            if (!$error) {
                foreach ($data_groups as $id_dp =>  $group) {
                    if (!$query->execute([
                        $id_student,
                        $class,
                        $id_dp,
                        $group,
                    ])) {
                        $error = true;
                    }
                }
            }
        }

        return !$error;
    }

    function delete()
    {
        if ($_POST['id'] != "")
        {
            $query = $this->db->prepare("delete from students where id=?");
            if ($query->execute(array($_POST['id'])))
            {
                $res['success'] = true;
            }
            else $res['error'] = "Ошибка удаления";
        }
        else $res['error'] = "Передан неверный id";

        echo json_encode($res);
    }
}

