<?php

use Phinx\Migration\AbstractMigration;

class RenameDoSubgroups extends AbstractMigration
{
    public function up()
    {
		$this->query("RENAME TABLE do_to_subgroup TO dp_to_subgroup");
    }
}
