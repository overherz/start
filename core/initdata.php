<?php

if (function_exists("mb_internal_encoding")) mb_internal_encoding('UTF-8');
date_default_timezone_set("Europe/Moscow");

if (php_sapi_name() != "cli") define('URI', $_SERVER['REQUEST_URI']);
define(strtoupper('admin_root'),dirname(dirname(__FILE__)).DS."admin".DS);

$sub = explode(".",$_SERVER['HTTP_HOST']);
//if (count($sub) == 3) define(strtoupper('subdomain'),array_shift($sub));

define("LANG","ru");
define("AJAX", (isset($_SERVER["HTTP_X_PJAX"]) && $_SERVER["HTTP_X_PJAX"]) || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'));
define("PJAX", (isset($_SERVER["HTTP_X_PJAX"]) && $_SERVER["HTTP_X_PJAX"]));

require_once(ROOT."langs/".LANG.".php");
require_once(ROOT.'core/functions.php');

if($INFO['secure'] && !array_key_exists('HTTPS',$_SERVER))
{
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("Location: $redirect");
}
else if (!$INFO['secure'] && array_key_exists('HTTPS',$_SERVER))
{
    $redirect = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("Location: $redirect");
}

set_start_statistic();
set_error_handler("warning_handler");
register_shutdown_function('shutdown');

foreach ($INFO as $key => $value)
{
    if (!is_array($value))
    {
        if ($key == "dev")
        {
            define(strtoupper('true_dev_mode'),$value);
            if ((array_key_exists('dev',$_COOKIE) && $_COOKIE['dev'] == "15071965") || $value) define(strtoupper('dev'),true);
            if ((php_sapi_name() == "cli" || AJAX) && !array_key_exists('get_ajax_queries',$_GET)) $value = false;
        }
        else define(strtoupper($key),$value);
    }
}

if (defined('DEV') && DEV) error_reporting(E_ALL & ~E_NOTICE);
else error_reporting(0);
ini_set('display_errors', 0);

require_once (ROOT.'vendor/autoload.php');
require_once(ROOT.'core/controller.php');
require_once(ROOT.'core/layout.php');

if ($_GET['get_ajax_queries'] && defined('DEV') && DEV) get_dev_resources(true);

require_once(ROOT.'core/database.php');
require_once(ROOT.'core/router.php');
require_once(ROOT.'core/cache.php');

if(php_sapi_name() != "cli") new Router();

set_end_statistic();

if (defined('DEV') && DEV && !AJAX && php_sapi_name() != "cli") get_dev_resources();
if (defined('AJAX') && AJAX) $_SESSION['dev'] = $GLOBALS['dev'];