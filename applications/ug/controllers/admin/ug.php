<?php
namespace admin\ug;

class ug extends \Admin {

    private $module_name = "Учебные группы";
    private $limit = 20;

    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            case "add":
                $this->add();
                break;
            case "delete":
                $this->delete();
                break;
            case "edit":
                $this->edit();
                break;
            case "get_umk_from_kug":
                $res['success'] = true;
                $html = "<option>&nbsp;</option>";
                if ($umk = $this->get_controller("umk")->get_umk_from_kug($_POST['id']));
                {
                    foreach ($umk as $u)
                    {
                        $html .= "<option value='{$u['id']}'>{$u['name']}</option>";
                    }
                    $res['success'] = $html;
                }

                echo json_encode($res);
                break;
            default: $this->default_show();
        }
    }

    function default_show()
    {
        if (isset($_POST['search']) && $_POST['search'] != '')
        {
            $s = $this->db->quote("%{$_POST['search']}%");
            $sql = "where u.name LIKE ".$s;
        }

        require_once(ROOT.'libraries/paginator/paginator.php');
        $query = $this->db->query("select SQL_CALC_FOUND_ROWS u.*,user.*
            from study_groups as u
            LEFT JOIN users as user ON u.owner=user.id_user
            {$sql}
            order by name
            LIMIT {$this->limit}
            OFFSET ".\Paginator::get_offset($this->limit,$_POST['page'])."
        ");
        while ($row = $query->fetch())
        {
            $data[] = $row;
        }

        $paginator = new \Paginator($this->db->found_rows(), $_POST['page'], $this->limit);

        if (defined('AJAX') && AJAX)
        {
            $res['success'] = $this->layout_get('admin/table.html',array('data' => $data,'paginator' => $paginator));
            echo json_encode($res);
        }
        else $this->layout_show('admin/index.html',array('data' => $data,'paginator' => $paginator));
    }

    function add()
    {
        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");
        $users = $user_ctr->get_teachers();

        /** @var  $kug_ctr \kug\kug */
        $kug_ctr = $this->get_controller("kug");
        $kug = $kug_ctr->get_all();

        $res['success'] = $this->layout_get('admin/form.html',array('users' => $users,'kug' => $kug));
        echo json_encode($res);
    }

    function edit()
    {
        if ($_POST['id'] != "")
        {
            if ($element = $this->get($_POST['id']))
            {
                /** @var  $user_ctr \users\users */
                $user_ctr = $this->get_controller("users");
                $users = $user_ctr->get_teachers();

                /** @var  $kug_ctr \kug\kug */
                $kug_ctr = $this->get_controller("kug");
                $kug = $kug_ctr->get_all();

                /** @var  $umk_ctr \umk\umk */
                $umk_ctr = $this->get_controller("umk");
                $umk = $umk_ctr->get_all_by_kug();

                $res['success'] = $this->layout_get('admin/form.html',array("element" => $element,'mode' => 'edit','users' => $users,'kug' => $kug,'umk' => $umk));
            }
        }
        else $res['error'] = "Переданые неверные данные";

        echo json_encode($res);
    }

    function save()
    {
        if ($_POST['name'] == '') $res['error'] = "Название не может быть пустым";
        if ($_POST['name_education'] == '') $res['error'] = 'Название образовательного учреждения не может быть пустым';
        foreach ($_POST['umk'] as $item) {
            if ($item) $umk[] = $item;
        }
        $umk = array_unique($umk);
        if (count($umk) < 1) $res['error'] = "Отсутствуют УМК";

        $this->db->beginTransaction();
        if (!$res['error'])
        {
            /** @var  $log \logs\logs */
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            if ($element['name'] != $_POST['name'] && $_POST['id'] != "") $message = ". Название {$element['name']} изменено на \"{$_POST['name']}\"";

            $save_data = array(
                $_POST['owner'],
                $_POST['name'],
                $_POST['name_education'],
                strtotime($_POST['date'])
            );
            if ($_POST['id'] != "")
            {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE study_groups set owner=?,name=?,name_education=?,date=? where id=?");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";

                $query = $this->db->prepare("delete from study_groups_to_umk where id_study_group=?");
                if (!$query->execute(array($_POST['id']))) $res['error'] = "Ошибка базы данных";

                $id_group = $_POST['id'];
            }
            else
            {
                $query = $this->db->prepare("INSERT INTO study_groups(owner,name,name_education,date) VALUES(?,?,?,?)");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                else $id_group = $this->db->lastInsertId();
            }

            $query_insert_umk = $this->db->prepare("insert into study_groups_to_umk(id_study_group, id_umk) VALUES (?,?)");
            foreach ($umk as $u)
            {
                if (!$query_insert_umk->execute(array($id_group,$u))) $res['error'] = "Ошибка базы данных";
            }
        }

        if (!$res['error'])
        {
            if ($_POST['id']) $log->save_into_log("admin", $this->module_name, true, "Отредактировано. \"{$element['name']}\"" . $message, $_SESSION['admin']['id_user']);
            else $log->save_into_log("admin", $this->module_name, true, "Добавлено. \"{$_POST['name']}\"", $_SESSION['admin']['id_user']);

            $res['success'] = true;
            $this->db->commit();
        }
        else $this->db->rollBack();

        echo json_encode($res);
    }

    function delete()
    {
        if ($_POST['id'] != "")
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            $query = $this->db->prepare("delete from study_groups where id=?");
            if ($query->execute(array($_POST['id'])))
            {
                $res['success'] = true;
                if ($log) $log->save_into_log("admin",$this->module_name,true,"Удалено. \"{$element['name']}\"",$_SESSION['admin']['id_user']);
            }
            else $res['error'] = "Ошибка удаления";
        }
        else $res['error'] = "Передан неверный id";

        echo json_encode($res);
    }

    function get($id)
    {
        $query = $this->db->prepare("select * from study_groups where id=?");
        $query->execute(array($id));
        if ($element = $query->fetch())
        {
            $query = $this->db->query("select u.*
                from study_groups_to_umk as gu
                LEFT JOIN umk as u ON gu.id_umk=u.id
                where gu.id_study_group={$id}
            ");

            $element['umk'] = $query->fetchAll();
        }

        return $element;
    }
}

