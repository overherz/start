<?php
namespace umk;

class umk extends \Controller {

    function default_method()
    {
        switch($_POST['act'])
        {
            default: $this->default_show();
        }
    }

    function default_show()
    {

    }

    function get_umk_from_kug($kug)
    {
        $query = $this->db->prepare("select * from umk where id_study_group_category=? ORDER BY position ASC");
        $query->execute(array($kug));
        return $query->fetchAll();
    }

    function get_kug_from_umk($umk)
    {

    }

    function get_all_by_kug()
    {
        $query = $this->db->query("select * from umk ORDER BY position ASC");
        while ($row = $query->fetch())
        {
            $umk[$row['id_study_group_category']][] = $row;
        }

        return $umk;
    }

    function get_umk_from_ug($ug)
    {
        $umk = array();
        $query = $this->db->prepare("select id_umk from study_groups_to_umk where id_study_group=?");
        $query->execute(array($ug));
        while ($row = $query->fetch())
        {
            $umk[] = $row['id_umk'];
        }

        return $umk;
    }

    function get_umk_from_ug_name($ug)
    {
        $umk = array();
        $query = $this->db->prepare("select name
            from study_groups_to_umk as l
            left join umk as u ON l.id_umk=u.id
            where id_study_group=?
        ");
        $query->execute(array($ug));
        while ($row = $query->fetch())
        {
            $umk[] = $row['name'];
        }

        return $umk;
    }
}

