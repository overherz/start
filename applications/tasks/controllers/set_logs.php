<?php
namespace tasks;

require_once(__DIR__ .'../../../../index.php');

class set_logs extends \Controller {

    private $id;

    function __construct($id,$key)
    {
        /** @var $task_cr \tasks\tasks */
        $task_cr = $this->get_controller("tasks");
        /** @var  $task_error_cr \tasks\error */
        $task_error_cr = $this->get_controller("tasks","error");
        if ($task = $task_cr->get_task($id))
        {
            $task_cr->get_options();
            $pid = getmypid();

            if ($key == get_setting('cron_key') && !$task_cr->is_process_running($task['pid']))
            {
                $this->id = $task['id'];
                $GLOBALS['cli_task_id'] = $this->id;
                $task_cr->set_status($id,"run",$task['period'],$pid);
                $this->db = \MyPDO::connect();

                $query_comment = $this->db->prepare("insert into comments(text,parent_id,id_entity,id_user,created,type_entity) values(?,?,?,?,?,?)");
                $query = $this->db->prepare("select * from projects_tasks_roles where id_user=1");
                $query->execute();
                $i = 0;
                while ($row = $query->fetch())
                {
                    $i++;
                    if ($query_comment->execute(array("test".$i,null,$row['id_task'],'132',time(),'task')));
                }
            }
        }
        else
        {
            $task_error_cr->set_error("Неверный ключ",$this->id);
        }
    }
}

new set_logs($argv[1],$argv[2]);
