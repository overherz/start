<?php

use Phinx\Migration\AbstractMigration;

class ResultMayNull extends AbstractMigration
{
	const
		TABLE_RESULTS = 'results_uud';

	public function up()
	{
		$table = $this->table(self::TABLE_RESULTS);
		$table->changeColumn('value', 'decimal', ['precision' => 2, 'scale' => 1, 'signed' => true, 'null' => true])
			->save();
	}

	public function down()
	{

	}
}
