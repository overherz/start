<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class DpNewData extends AbstractMigration
{
    const TABLE = 'dp';

    private $data = [
        'Регулятивные УУД' => [
            [
                'name' => 'планирование',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => '1,2',
                'fields4' => '1,2',
                'fields5' => '1,2',
            ],
            [
                'name' => 'оценка',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => '1,2',
                'fields4' => '1,2',
                'fields5' => '1,2',
            ],
            [
                'name' => 'контроль',
                'begin' => 2,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => '1,2',
                'fields4' => '1,2',
                'fields5' => '1,2',
            ],
            [
                'name' => 'коррекция',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => '1,2',
                'fields4' => '1,2',
                'fields5' => '1,2',
            ],
            [
                'name' => 'границы',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => '1,2',
                'fields4' => '1,2',
                'fields5' => '1,2',
            ]
        ],
        'Познавательные УУД' => [
            [
                'name' => 'анализ',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'синтез',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'сравнение',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'классификация',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'обобщение',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'причинно-следственные связи',
                'begin' => 1,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'аналогия',
                'begin' => 2,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'отнесение к понятию',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'диаграммы и таблицы',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'индуктивное умозаключение',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
        ],
        'Коммуникативные' => [
            [
                'name' => 'речевое высказывание',
                'begin' => 2,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'точка зрения',
                'begin' => 2,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'вопросы',
                'begin' => 2,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
            [
                'name' => 'объединение информации',
                'begin' => 3,
                'fields1' => 'А,Б,В',
                'fields2' => 'А,Б,В',
                'fields3' => 'А,Б,В',
                'fields4' => 'А,Б,В',
                'fields5' => 'А,Б,В',
            ],
        ]
    ];

    public function up()
    {
        $table = $this->table(self::TABLE);
        $table->addColumn('begin', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'null' => true])
            ->addIndex(['begin'])
            ->addColumn('fields1', 'string')
            ->addColumn('fields2', 'string')
            ->addColumn('fields3', 'string')
            ->addColumn('fields4', 'string')
            ->addColumn('fields5', 'string')
            ->save();

        $insert_data = [];
        $i = 0;
        foreach ($this->data as $gdp => $data) {
            $id_gdp = $this->get_gdp_id($gdp);

            foreach ($data as $value) {
                $insert_data[] = [
                    'id_gdp' => $id_gdp,
                    'name' => $value['name'],
                    'short_name' => $value['name'],
                    'position' => $i,
                    'begin' => $value['begin'],
                    'fields1' => $value['fields1'],
                    'fields2' => $value['fields2'],
                    'fields3' => $value['fields3'],
                    'fields4' => $value['fields4'],
                    'fields5' => $value['fields5'],
                ];
                $i++;
            }
        }

        $this->insert(self::TABLE, $insert_data);
    }

    private function get_gdp_id($name)
    {
        $row = $this->fetchRow("SELECT id FROM gdp WHERE name = '{$name}'");
        return $row['id'];
    }
}
