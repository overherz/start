$(document).ready(function() {
    $(document).on("click",".del-btn",function(){
        if(!confirm('Удалить?'))return false;
        var id = $(this).data('id');
        user_api({act:'delete',id:id},function(){
            $("tr[data-id='"+id+"']").remove();
            show_message("success","Успешно удалено");
        });
        return false;
    });

    $(document).on("click",".add-btn",function(){
        user_api({act:'add'},function(data){
            show_popup(data,"Добавление");
            add_popup_button("Сохранить",'save');

            $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
            $("[name='date']").datepicker({
                dateFormat: "dd.mm.yy",
                "showAnim": 'slide'
            });
        });
    });

    $(document).on("click",".edit-btn",function(){
        var id = $(this).data('id');

        user_api({act:'edit',id:id},function(data){
            show_popup(data,"Редактирование");
            add_popup_button("Сохранить",'save');

            $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
            $("[name='date']").datepicker({
                dateFormat: "dd.mm.yy",
                "showAnim": 'slide'
            });
        });
    });

    $(document).on("click","[save]",function(){
        var params = $("#edit_form").find(":input").serialize();
        user_api(params,function(res){
            user_api(false,function(data){
                $("#search_result").html(data);
            });
            show_message("success","Сохранено");
            hide_popup();
        });
    });

    $(document).on("click",".add-btn-umk",function(){
        var html = $("#for_clone").html();
        $("#has").append(html);
        $("#has select").removeClass("no_style");
        style_input();

        return false;
    });

    $(document).on("click",".del-btn-umk",function(){
        if(!confirm('Удалить?')) return false;
        var parent = $(this).closest('table');
        parent.remove();

        return false;
    });

    $(document).on("change","select.select_kug",function(){
        var optionSelected = $(this).find("option:selected");
        var id = optionSelected.val();
        var parent = $(this).closest('table');
        var element = parent.find("[name='umk[]']");

        user_api({act:'get_umk_from_kug',id:id},function(data){
            element.html(data).trigger('refresh');
        });
    });
});