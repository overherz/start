<?php

use Phinx\Migration\AbstractMigration;

class StudentFieldBegin extends AbstractMigration
{
	const TABLE = 'students';

	public function up()
	{
		$table = $this->table(self::TABLE);
		$table
			->addColumn('begin_start', 'integer', ['null' => true])
			->addColumn('begin_end', 'integer', ['null' => true])
			->addIndex(['begin_start'])
			->addIndex(['begin_end'])
			->save();

		$this->execute('UPDATE ' . self::TABLE . ' SET begin_start = 1 WHERE id_study_group IN (SELECT id FROM study_groups WHERE type="2")');
	}

	public function down()
	{
		$table = $this->table(self::TABLE);
		$table
			->removeColumn('begin_start')
			->removeColumn('begin_end')
			->save();
	}
}
