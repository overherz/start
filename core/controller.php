<?php
class Controller {

    protected $layout;
    protected $application;
    protected $controller;
    protected $db;
    static protected $redirect = false;
    protected $cache;

    function __construct($app,$controller,$id=false, $more=false)
    {
        $this->db = MyPDO::connect();
        $this->cache = Cache::connect();
        if($id) $this->id = $id;
        $this->set_layout_path("applications".DS.$app.DS."layouts".DS);
        $this->set_application($app);
        $this->set_controller($controller);

        if($more)
        {
            ksort($more);
            $more = array_values($more);
            foreach($more as $key => $value)
            {
                if($value)
                {
                    $key = '_'.$key;
                    $this->$key = $value;
                }
            }
        }
    }

    function __destruct() {
        if (self::$redirect && !AJAX) echo "<meta http-equiv='refresh' content='".self::$redirect['delay']."; url=".self::$redirect['url']."'>";
    }

    public function __toString() {
        pr(get_object_vars($this));
        return "";
    }

    function set_layout_path($path){
        $this->layout = $path;
    }

    function set_application($application){
        $this->application = $application;
    }

    function set_controller($controller){
        $this->controller = $controller;
    }

    static function get_controller($application, $controller=null, $admin_mode=false, $id=false, $more=null)
    {
        $path = ROOT."applications".DS.$application.DS;

        if (!$controller)
        {
            $controller = $application;
        }

        if($admin_mode)
        {
            $path .= "controllers".DS."admin".DS.$controller.".php";
        }
        else
        {
            $path.= "controllers".DS.$controller.".php";
        }

        $class = $application."\\".$controller;
        if ($admin_mode) $class = "admin\\".$class;

        if(file_exists($path) && is_file($path))
        {
            include_once($path);

            return new $class($application,$controller,$id,$more);
        }
        else
        {
            $error = array(
                'message' => "Class {$class} not found",
                'file' => __FILE__,
                'line' => __LINE__
            );
            debug($error,false,true);
        }
        return false;
    }

    function layout_show($layout,$values=array(),$app=false)
    {
        if (!$values) $values = array();
        if (!$app) $layout_path = $this->layout;
        else $layout_path = "applications/{$app}/layouts/";

        $values = array_merge($values,\Controller::set_layout_global_values());
        layout::layout_show($layout_path.$layout,$values);
    }

    function layout_get($layout,$values=array(),$app=false)
    {
        if (!$values) $values = array();
        if (!$app) $layout_path = $this->layout;
        else $layout_path = "applications/{$app}/layouts/";

        $values = array_merge($values,\Controller::set_layout_global_values());
        return layout::layout_get($layout_path.$layout,$values);
    }

    static function set_layout_global_values()
    {
        if ($GLOBALS['globals']) $values['globals'] = $GLOBALS['globals'];
        $values['app'] = new \app_paths();
        if ($GLOBALS['settings']) $values['settings'] = $GLOBALS['settings'];
        if ($_POST) $values['post_data'] = $_POST;
        if ($_GET) $values['get_data'] = $_GET;
        if ($_SESSION) $values['session_data'] = $_SESSION;
        if (defined('AJAX') && AJAX) $values['ajax_data'] = true;
        if ($_COOKIE) $values['cookie_data'] = $_COOKIE;
        if (SUBDOMAIN) $values['site']['subdomain'] = SUBDOMAIN;

        return $values;
    }

    static function error_page($num=404, $values=array())
    {
        switch ($num)
        {
            case '404':
                header("HTTP/1.1 404 Not Found");
                break;
        }

        $values = array_merge($values,\Controller::set_layout_global_values());

        if (defined('AJAX') && AJAX) $values['ajax_data'] = true;
        if ($_SERVER['HTTP_REFERER'] != "") $values['referer'] = $_SERVER['HTTP_REFERER'];
        $values['url'] = request_url();
        layout::layout_show("/error/{$num}.html",$values);
        if(defined('DEV') && DEV) {
            get_dev_resources($GLOBALS['start'],$GLOBALS['memstart']);
        }
        exit();
    }

    static function redirect($url='/', $delay=0)
    {
        $delay = (int) $delay;
        if(!$delay && !headers_sent()) {
            header('Location: '.$url);
            exit();
        }
        else Controller::$redirect = array('delay' => $delay,'url' => $url);
    }

    static function set_global($key, $val)
    {
        $GLOBALS['globals'][$key] = $val;
        return true;
    }

    static function get_global($key)
    {
        return $GLOBALS['globals'][$key];
    }
}

class Admin extends Controller {
    var $path;

    function set_menu()
    {
        require(ROOT."applications/config.php");
        $handle=opendir(ROOT.'applications/');
        $menu = array();
        while(false!==($mod=readdir($handle)))
        {
            $submenu = array();
            $name = "";
            $icon = false;
            $path=ROOT."applications/{$mod}/config.php";
            if (is_file($path) && include($path))
            {
                if ($submenu) {
                    foreach ($submenu as $h => $t)
                    {
                        foreach ($t as $k => $g)
                        {
                            if ($k != $mod || strstr($k, '/')) {
                                $s['url'] = $mod."/".$k;
                            } else {
                                $s['url'] = $k;
                            }
                            $s['name'] = $g;
                            $s['category'] = $name;
                            $s['count'] = count($t);
                            $s['application'] = $mod;
                            $s['sort'] = $s['count'] > 1 ? $name.$g : $g;
                            $s['icon'] = $icon;
                            $menu[$h]['submenu'][] = $s;
                            $applications[$mod] += 1;
                        }

                        if (count($menu[$h]['submenu']) > 1)
                        {
                            uasort($menu[$h]['submenu'],function($a,$b){
                                return strnatcmp(mb_strtolower($a['sort'], 'UTF-8'), mb_strtolower($b['sort'], 'UTF-8'));
                            });
                        }
                    }
                }
            }
        }

        foreach ($menu as $k => $v)
        {
            if ($menu_names[$k] != "") $menu[$k]['name'] = $menu_names[$k];
            else $menu[$k]['name'] = $k;

            if ($this->controller != $this->application) {
                $this->path = $this->application."/".$this->controller;
            } else if ($this->controller == $this->application && $this->id) {
                $this->path =  $this->application . "/" . $this->controller . "/" . $this->id;
            } else $this->path = $this->controller;

            if (count ($v['submenu']) > 0);
            foreach ($v['submenu'] as $z => $j)
            {
                if (is_array($_SESSION['admin']['access']) && in_array($j['url'], $_SESSION['admin']['access']))
                {
                    unset($menu[$k]['submenu'][$z]);
                    if (count($menu[$k]['submenu']) > 0)
                    {
                        $applications[$j['application']]--;
                        foreach ($menu[$k]['submenu'] as &$count)
                        {
                            if ($count['application'] == $j['application']) $count['count'] = $applications[$j['application']];
                        }
                    }
                    else unset($menu[$k]);
                    continue;
                }

                if (!$menu[$k]['url']) $menu[$k]['url'] = $j['url'];

                if ($this->path == $j['url'])
                {
                    parent::set_global("submenu", $this->path);
                    parent::set_global("menu", $k);
                    $menu_from_url = true;
                }
                else if ($this->application == $j['url'] && !$menu_from_url)
                {
                    parent::set_global("submenu", $this->application);
                    parent::set_global("menu", $k);
                }
            }
        }

        $menu = array_replace($menu_names, $menu);

        parent::set_global("root_menu", $menu);
    }

    function test_access()
    {
        if ($_SESSION['admin'] && ($this->application != "index" || ($this->application == "index" && $this->controller != "index" && $this->controller != "logout")))
        {
            setcookie('redirect', $_SERVER['REDIRECT_URL'], time()+60*60*24*7,"/",get_cookie_domain());
        }
        $session_name = session_name();
        if ($_GET[$session_name])
        {
            if (session_id() && $_GET[$session_name] != session_id()) session_destroy();
            $_COOKIE[$session_name] = $_GET[$session_name];
            session_id($_GET[$session_name]);
            session_set_cookie_params(0, '/', get_cookie_domain());
            session_start();                    
        }
        
        if ($_SESSION['admin']['id_group'])
        {
            $result = $this->db->query("select u.id_group,u.fio,g.name,g.access,g.access_admin,g.color from users as u LEFT JOIN groups as g ON u.id_group=g.id where u.id_user='{$_SESSION['admin']['id_user']}' LIMIT 1")->fetch();
            if (!$result['access_admin']) unset($_SESSION['admin']);
            else {
                $_SESSION['admin']['fio'] = $result['fio'];
                $_SESSION['admin']['access'] = json_decode($result['access']);
                $_SESSION['admin']['group_name'] = $result['name'];
                $_SESSION['admin']['group_color'] = $result['color'];
            }
        }
        $this->set_menu();

        if (!$_SESSION['admin'] && ($this->application != "index" || ($this->application == "index" && $this->controller != "index")))
        {
            setcookie('redirect', $_SERVER['REQUEST_URI'], time()+60*60*24*7,"/",get_cookie_domain());
            if (defined('AJAX') && AJAX)
            {
                echo json_encode("AdminloginException");
                exit();
            }
            else parent::redirect('/admin/');
        }
        else if (is_array($_SESSION['admin']['access']) && in_array($this->path,$_SESSION['admin']['access']))
        {
            if (defined('AJAX') && AJAX)
            {
                echo json_encode("AdminloginException");
                exit();
            }
            else parent::error_page("admin_denied");
        }
    }

    function get_applications()
    {
        $func = function($value) {
            return str_replace(".php","",$value);
        };

        $folder = ROOT.'applications/';
        if ($files = scandir($folder))
        {
            foreach ($files as $f)
            {
                $name = "";
                if (is_dir($folder.$f) && $f != "." && $f != "..")
                {
                    $file = "{$folder}/{$f}/config.php";
                    if (file_exists($file))
                    {
                        include($file);
                        if ($f != "page")
                        {
                            if ($name != "") $mods[$f]['name'] = $name;
                            $mods[$f]['subapp'] = glob($folder.$f."/controllers/*.php");
                            $mods[$f]['subapp'] = array_map('basename', $mods[$f]['subapp']);
                            $mods[$f]['subapp'] = array_map($func, $mods[$f]['subapp']);
                        }
                    }
                }
            }
        }
        return $mods;
    }
}

class Global_module
{
    const
        TYPE_SITE = 1,
        TYPE_ADMIN = 2,
        TYPE_BOTH = 3;

    protected $type = self::TYPE_SITE;
    protected $on_ajax_not_run = true;
    private $modules = array();

    function run($admin,$module=false)
    {
        if (!$module)
        {
            $globals_run_order = [];
            $folder = ROOT.'globals'.DS;
            include_once($folder.DS."config.php");
            foreach ($globals_run_order as $k => $v)
            {
                $this->modules[] = $v.".php";
            }
        }
        else $this->modules[] = $module.".php";
        if (count($this->modules) > 0)
        {
            foreach ($this->modules as $m)
            {
                $file = ROOT."globals/".$m;
                if (file_exists($file))
                {
                    include_once($file);
                    $class = "global_module\\".str_replace(".php","",$m);
                    $r = new $class;
                    if (AJAX && $r->on_ajax_not_run) continue;
                    if ($r->type != self::TYPE_BOTH && ((!$admin && $r->type == self::TYPE_ADMIN) || ($admin && $r->type == self::TYPE_SITE))) continue;
                    $r->run_module();
                }
            }
        }
    }
}
