<?php

use Phinx\Migration\AbstractMigration;

class InsertOptionsValues extends AbstractMigration
{
	private $data = [
		[
			'key_name' => 'test1', // Название переменной на английском маленькими буквами без пробелов
			'name' => 'название', // название на любом языке, просто как текст в админке выводится
			'type' => 'text',
			'value' => 'Значение',
		],
		[
			'key_name' => 'test2',
			'name' => 'название',
			'type' => 'text',
			'value' => 'Значение',
		]
	];

	public function up()
	{
		$idGroup = $this->getGroupId();

		if (!empty($this->data)) {
			foreach ($this->data as &$data) {
				$data['key_name'] = strtolower($data['key_name']);
				$data['id_group'] = $idGroup;
			}
		}

		$this->insert('options', $this->data);
	}

	public function down()
	{
		$idGroup = $this->getGroupId();

		$this->query('DELETE FROM options WHERE id_group = ' . $idGroup);
	}

	private function getGroupId()
	{
		$row = $this->fetchRow("SELECT id FROM group_options WHERE name = 'Названия'");
		return $row['id'];
	}
}
