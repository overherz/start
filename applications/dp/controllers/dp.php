<?php
namespace dp;

class dp extends \Controller {

    function default_method()
    {
        switch($_POST['act'])
        {
            default: $this->default_show();
        }
    }

    function get_umk_from_kug($kug)
    {
        $query = $this->db->prepare("select * from umk where id_study_group_category=?");
        $query->execute(array($kug));
        return $query->fetchAll();
    }

    function get_all_by_kug()
    {
        $query = $this->db->query("select * from umk");
        while ($row = $query->fetch())
        {
            $umk[$row['id_study_group_category']][] = $row;
        }

        return $umk;
    }

    function get_all_uud_dp($summary = false)
    {
        $data = [];

        $query = $this->db->prepare("SELECT dp.*,gdp.name AS gdp_name, dc.class, dc.name as dc_name, dc.fields, dc.float_fields, dps.id_subgroup
                FROM dp AS dp
                JOIN gdp AS gdp ON dp.id_gdp=gdp.id AND type=?
                JOIN dp_to_class as dc ON dp.id=dc.id_dp
                left join dp_to_subgroup as dps ON dp.id = dps.id_dp
                ORDER BY position ASC
            ");
        $query->execute([TYPE_UUD]);

        while ($row = $query->fetch()) {
            $data['gdp'][$row['id_gdp']] = $row['gdp_name'];
            if (empty($data['dp'][$row['id_gdp']][$row['id']])) {
                $data['dp'][$row['id_gdp']][$row['id']] = $row;
            }

            $row['fields'] = explode(',', $row['fields']);
            $row['float_fields'] = explode(', ', $row['float_fields']);

            if ($summary) {
                list($fields, $summary_fields) = $this->udd_fields_convert($row['fields'], $row['class']);
            } else {
                $fields = $row['fields'];
            }

            $data['dp'][$row['id_gdp']][$row['id']]['extra'][$row['class']] = [
                'fields' => $fields,
                'name' => $row['dc_name'],
				'float_fields' => $row['float_fields'],
            ];

            if (!empty($summary_fields)) {
                $data['dp'][$row['id_gdp']][$row['id']]['extra'][$row['class']]['summary_fields'] = $summary_fields;
            }

            if (!empty($data['dp'][$row['id_gdp']][$row['id']]['extra'])) {
                $data['dp'][$row['id_gdp']][$row['id']]['classes'] = array_keys($data['dp'][$row['id_gdp']][$row['id']]['extra']);
            }

            unset($data['dp'][$row['id_gdp']][$row['id']]['fields']);
            unset($data['dp'][$row['id_gdp']][$row['id']]['class']);
            unset($data['dp'][$row['id_gdp']][$row['id']]['dc_name']);
        }

        if (!empty($data['gdp'])) {
        	$ids = implode(',', array_keys($data['gdp']));
        	$query = $this->db->query("SELECT s.*, GROUP_CONCAT(ds.id_dp) as dp 
				FROM dp_subgroups as s
				LEFT JOIN dp_to_subgroup as ds ON s.id = ds.id_subgroup				 
				WHERE id_parent IN ({$ids})
				GROUP BY s.id
			");

        	while ($row = $query->fetch()) {
        		$row['dp'] = explode(',', $row['dp']);
        		$length = 0;
        		if (!empty($row['dp'])) {
        			foreach ($row['dp'] as $k) {
						$length += count($data['dp'][$row['id_parent']][$k]['extra'][$row['class']]['fields']);
					}
				}

        		$data['subgroups'][$row['class']][$row['id_parent']][$row['id']] = [
        			'name' => $row['sub_name'],
					'length' => $length,
					'dp_length' => count($row['dp']),
					'dp' => $row['dp'],
				];
			}
		}

        return $data;
    }

    function filter_uud_data(&$data)
	{
		if (!empty($data['dp'])) {
			foreach ($data['dp'] as $id_gdp => &$dps) {
				foreach ($dps as $id_dp => &$dp_data) {
					if (!empty($dp_data['extra'])) {
						foreach ($dp_data['extra'] as $id_class => $class_data) {
							if ($id_class != $data['begin']) {
								unset($dp_data['extra'][$id_class]);
							}
						}
					}
					if (empty($dp_data['extra'])) {
						unset($dps[$id_dp]);
					} else {
						$dp_data['extra'] = reset($dp_data['extra']);
					}
					unset($dp_data['classes']);
				}

				if (empty($dps)) {
					unset($data['dp'][$id_gdp]);
				}
			}
			unset($dps);
			unset($dp_data);
		}

		if (!empty($data['gdp'])) {
			foreach ($data['gdp'] as $id_gdp => &$gdp_data) {
				if (empty($data['dp'][$id_gdp])) {
					unset($data['gdp'][$id_gdp]);
				}
			}
			unset($gdp_data);
		}

		if (!empty($data['results'])) {
			foreach ($data['results'] as $id_student => &$v) {
				foreach ($v as $kv => $vv) {
					if ($kv != $data['begin']) {
						unset($data['results'][$id_student][$kv]);
					}
				}

				if (!empty($data['results'][$id_student])) {
					$v = reset($v);
				}
			}
		}

		$data['data_filtered'] = true;
	}

	function gdp_uud_general_summary(&$data)
	{
		if (empty($data['data_filtered'])) {
			$this->filter_uud_data($data);
		}

		$data['gdp_summary'] = [];
		foreach ($data['gdp'] as $k => $g) {
			foreach ($data['dp'] as $kk => $dd) {

			}
		}
	}

    function get_uud_dp_max_sum($class)
    {
        $max = 0;

		$max_values = $this->get_max_uug_dp_fields_values();

		if (!empty($max_values)) {
			foreach ($max_values as $data) {
				foreach ($data as $number_class => $values) {
					if ($number_class == $class) {
						$max += array_sum($values);
					}
				}
			}
		}

        return $max;
    }

    function udd_fields_convert($fields, $class)
    {
        $summary_fields = [];
        $new_fields = [];
        foreach ($fields as $field) {
            if ($class == 1) {
                if (in_array($field, ['Б', 'В'])) {
                    $new_fields[] = $summary_fields[] = 'БВ';
                } else {
                    $new_fields[] = $field;
                }
            } elseif ($class == 2) {
                if (in_array($field, ['А', 'Б'])) {
                    $new_fields[] = $summary_fields[] = 'АБ';
                } else {
                    $new_fields[] = $field;
                }
            } else {
                $new_fields[] = $field;
            }
        }
        $new_fields = array_unique($new_fields);
        $summary_fields = array_unique($summary_fields);

        return [$new_fields, $summary_fields];
    }

    function get_max_uug_dp_fields_values()
	{
		$data = [];
		$query = $this->db->query('SELECT * from dp_to_class');
		while ($row = $query->fetch()) {
			$data[$row['id_dp']][$row['class']] = unserialize($row['fields_data']);
		}

		return $data;
	}
}

