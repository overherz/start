<?php
namespace results;

class excel extends \Controller
{
	function report_general_start($data)
	{
		$xls = new \PHPExcel();

		$sheet = $xls->setActiveSheetIndex(0);
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);


		$row = 1;
		$index = 1;
		$col = 1;
		foreach ($data['dp'] as $d) {
			foreach ($d as $dd) {
				$index = $index + 1;
			}
			if ($data['show_summary']) $index = $index + 1;
		}
		$sheet->setCellValueByColumnAndRow($col, $row, '"Школьный старт". Общие результаты.');
		$sheet->mergeCellsByColumnAndRow($col, $row, $col + $index, $row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 0;
		$sheet->setCellValueByColumnAndRow($col, $row, "Класс / группа ДОУ {$data['ug']['name']}");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col = 1;
		foreach ($data['gdp'] as $k => $g) {
			$length = count($data['dp'][$k]);
			if ($data['show_summary']) {
				$length = $length + 1;
			}
			$sheet->setCellValueByColumnAndRow($col, $row, $g);
			$sheet->mergeCellsByColumnAndRow($col, $row, $col + $length - 1, $row);
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col = $col + $length;
		}

		$row++;

		$col = 0;
		$sheet->setCellValueByColumnAndRow($col, $row, "Показатели / Фамилия, Имя");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$index = 1;
		$col = 1;
		foreach ($data['dp'] as $d) {
			foreach ($d as $dd) {
				$sheet->setCellValueByColumnAndRow($col, $row, $index);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$index++;
				$col++;
			}
			if ($data['show_summary']) {
				$sheet->setCellValueByColumnAndRow($col, $row, "∑");
				$sheet->mergeCellsByColumnAndRow($col, $row, $col, $row + 1);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
			}
		}

		$col = 1;
		$row++;
		foreach ($data['dp'] as $d) {
			foreach ($d as $dd) {
				$sheet->setCellValueByColumnAndRow($col, $row, $dd['short_name']);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$index++;
				$col++;
			}
		}

		$i = 1;
		foreach ($data['students'] as $s) {
			if (($data['id'] && $s['id'] == $data['id']) || !$data['id']) {
				$col = 0;
				$row++;

				$sheet->setCellValueByColumnAndRow($col, $row, $i . ". " . $s['fio']);

				$col++;
				foreach ($data['dp'] as $d) {
					$summ = 0;
					foreach ($d as $dd) {
						$summ = $summ + $data['results'][$s['id']][$dd['id']];
						$sheet->setCellValueByColumnAndRow($col, $row, $data['results'][$s['id']][$dd['id']]);
						$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						$col++;
					}

					if ($data['show_summary']) {
						$sheet->setCellValueByColumnAndRow($col, $row, $summ);
						$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						$col++;
					}
				}
				$i++;
			}
		}
		if ($data['show_summary']) {
			$row++;
			$col = 0;
			$sheet->setCellValueByColumnAndRow($col, $row, "Значение по классу");

			$col++;
			foreach ($data['dp'] as $d) {
				foreach ($d as $dd) {
					$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getNumberFormat()->setFormatCode('#,##0.00');
					$sheet->setCellValueByColumnAndRow($col, $row, round($data['total'][$dd['id']] / count($data['students']), 2));
					$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col) . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$col++;
				}
				$col++;
			}

			$filename = "Общие результаты ";
		} else {
			$filename = "Внесение результатов ";
		}
		$filename .= $data['ug']['name'] . ".xls";

		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}

	function report_any_table($data)
	{
		$data['html'] = '<html><head><meta charset="utf-8"></head><body>' . $data['html'] . '</body></html>';
		$path = tempnam(sys_get_temp_dir(), time());
		file_put_contents($path, $data['html']);

		\PHPExcel_Shared_Font::setTrueTypeFontPath(ROOT.'source/fonts/');
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$reader = new \PHPExcel_Reader_HTML;
		$content = $reader->load($path);

		$sheet = $content->getActiveSheet();

		$default_style = $sheet->getDefaultStyle();
		$default_style
			->getAlignment()
			->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$styleArray = array(
			'font'  => array(
				'name'  => 'Arial',
			));

		$last_row = $sheet->getHighestDataRow();
		$last_col = $sheet->getHighestDataColumn();
		$max_height = 0;
		for ($row = 1; $row <= $last_row; $row++) {
			foreach ($this->excelColumnRange('A', $last_col) as $col) {
				$sheet->getStyle($col.$row)->applyFromArray($styleArray);

				if ($data['vertical']) {
					if ($row == $data['vertical']['row']) {
						$value = $sheet->getCell($col.$data['vertical']['row'])->getValue();
						$width = mb_strwidth($value);

						if ($max_height < $width) {
							$max_height = $width;
						}
					}
				}
			}
		}

		if ($data['vertical']) {
			for ($row = 1; $row <= $last_row; $row++) {
				if ($row != $data['vertical']['row']) {
					$height = 20;
				} else {
					$height = $max_height * 8;

					foreach ($this->excelColumnRange($data['vertical']['col'], $last_col) as $col) {
						$sheet->getStyle($col.$row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$sheet->getRowDimension($row)->setRowHeight($height);
			}
		}

		$filename = $data['filename'] . ' ' . $data['ug']['name'] . ".xls";

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter = new \PHPExcel_Writer_Excel5($content);
		$objWriter->save('php://output');
	}

	function report_general_uud($data)
	{
		$data['html'] = '<html><head><meta charset="utf-8"></head><body>' . $data['html'] . '</body></html>';
		$path = tempnam(sys_get_temp_dir(), time());
		file_put_contents($path, $data['html']);

		\PHPExcel_Shared_Font::setTrueTypeFontPath(ROOT.'source/fonts/');
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$reader = new \PHPExcel_Reader_HTML;
		$content = $reader->load($path);

		$sheet = $content->getActiveSheet();

		$default_style = $sheet->getDefaultStyle();
		$default_style
			->getAlignment()
			->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$styleArray = array(
			'font'  => array(
				'name'  => 'Arial',
			));

		$last_row = $sheet->getHighestDataRow();
		$last_col = $sheet->getHighestDataColumn();
		$max_height = 0;
		$dp_row = 2;
		for ($row = 1; $row <= $last_row; $row++) {
			foreach ($this->excelColumnRange('A', $last_col) as $col) {
				$sheet->getStyle($col.$row)->applyFromArray($styleArray);
				if ($row == $dp_row) {
					$value = $sheet->getCell($col.$dp_row)->getValue();
					$width = mb_strwidth($value);

					if ($max_height < $width) {
						$max_height = $width;
					}
				}
			}
		}

		if ($data['show_summary']) {
			$filename = "Общие результаты ";
		} else {
			$filename = "Внесение результатов ";
		}

		$filename .= $data['ug']['name'] . ".xls";

		for ($row = 1; $row <= $last_row; $row++) {
			if ($row != $dp_row) {
				$height = 20;
			} else {
				$height = $max_height * 8;

				foreach ($this->excelColumnRange('B', $last_col) as $col) {
					$sheet->getStyle($col.$row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			$sheet->getRowDimension($row)->setRowHeight($height);
		}

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter = new \PHPExcel_Writer_Excel5($content);
		$objWriter->save('php://output');
	}

	function report_levels_start($data)
	{
		$xls = new \PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,"Класс / группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+count($data['gdp']) * 2 + 2,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Фамилия, имя");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+1);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$col++;
		foreach ($data['gdp'] as $g)
		{
			$sheet->setCellValueByColumnAndRow($col,$row,$g['name']);
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$sheet->mergeCellsByColumnAndRow($col,$row,$col+1,$row);
			$col++; $col++;
		}
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа стартовой готовности");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+1);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 1;

		foreach ($data['gdp'] as $g)
		{
			$sheet->setCellValueByColumnAndRow($col,$row,"Сумма баллов");
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;
			$sheet->setCellValueByColumnAndRow($col,$row,"Уровень");
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;
		}

		foreach ($data['students'] as $ss)
		{
			if (($data['id'] && $ss['id'] == $data['id']) || !$data['id'])
			{
				$row++;
				$col = 0;
				$sheet->setCellValueByColumnAndRow($col,$row,$ss['fio']);
				$col++;
				foreach ($data['gdp'] as $g)
				{
					$sheet->setCellValueByColumnAndRow($col,$row,$data['total'][$ss['id']][$g['id']]);
					$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$col++;
					$sheet->setCellValueByColumnAndRow($col,$row,$ss[$g['id']]['level_gdp']['short_name']);
					$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$col++;
				}
				$sheet->setCellValueByColumnAndRow($col,$row,$ss['group']);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
		}

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename='Уровни инструментальной и личностной готовности {$data['ug']['name']}.xls'");

		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}

	function report_groups_start($data)
	{
		$xls = new \PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,'"Таблица “Группы стартовой готовности”');
		$sheet->setTitle('Группы стартовой готовности');
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getFont()->setBold(true);

		$row++; $row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Класс / группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группы");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$max = 0;

		foreach ($data['groups'] as $g)
		{
			$sheet->setCellValueByColumnAndRow($col,$row,$g);
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;
		}

		$col = 0;
		$row++;

		foreach ($data['groups'] as $g)
		{
			$proc = round(count($data['group_students'][$g])*100/count($data['students']),2);
			$sheet->setCellValueByColumnAndRow($col,$row,$g." уч. ({$proc}%)");
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;

			if ($max < count($data['group_students'][$g])) $max = count($data['group_students'][$g]);
		}

		for ($i = 0; $i <= $max-1;$i++)
		{
			$col = 0;
			$row++;
			foreach ($data['groups'] as $g)
			{
				$sheet->setCellValueByColumnAndRow($col,$row,$data['group_students'][$g][$i]);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
			}
		}

		$sheet = $this->autosize($sheet);

		$sheet = $xls->createSheet(1);
		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,'"Распределение по группам”');
		$sheet->setTitle('Распределение по группам');
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getFont()->setBold(true);

		$row++; $row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Класс / группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Личностная готовность");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+3);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$col++;
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+1);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Инструментальная готовность");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+1,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 2;
		$sheet->setCellValueByColumnAndRow($col,$row,"Базовый уровень");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Уровень ниже базового");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 1;
		$sheet->setCellValueByColumnAndRow($col,$row,"Базовый уровень");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 1. ".count($data['group_students'][1])." уч. (".round(count($data['group_students'][1])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 3. ".count($data['group_students'][3])." уч. (".round(count($data['group_students'][3])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 1;
		$sheet->setCellValueByColumnAndRow($col,$row,"Уровень ниже базового");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 2. ".count($data['group_students'][2])." уч. (".round(count($data['group_students'][2])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 4. ".count($data['group_students'][4])." уч. (".round(count($data['group_students'][4])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename='Группы стартовой готовности {$data['ug']['name']}.xls'");

		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}

	function report_groups_uud($data)
	{
		$xls = new \PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,'"Таблица “Группы стартовой готовности”');
		$sheet->setTitle('Группы стартовой готовности');
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getFont()->setBold(true);

		$row++; $row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Класс / группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группы");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$max = 0;

		foreach ($data['groups'] as $g)
		{
			$sheet->setCellValueByColumnAndRow($col,$row,$g);
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;
		}

		$col = 0;
		$row++;

		foreach ($data['groups'] as $g)
		{
			$proc = round(count($data['group_students'][$g])*100/count($data['students']),2);
			$sheet->setCellValueByColumnAndRow($col,$row,$g." уч. ({$proc}%)");
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$col++;

			if ($max < count($data['group_students'][$g])) $max = count($data['group_students'][$g]);
		}

		for ($i = 0; $i <= $max-1;$i++)
		{
			$col = 0;
			$row++;
			foreach ($data['groups'] as $g)
			{
				$sheet->setCellValueByColumnAndRow($col,$row,$data['group_students'][$g][$i]);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
			}
		}

		$sheet = $this->autosize($sheet);

		$sheet = $xls->createSheet(1);
		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,'"Распределение по группам”');
		$sheet->setTitle('Распределение по группам');
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getFont()->setBold(true);

		$row++; $row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Класс / группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Личностная готовность");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+3);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$col++;
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+1);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Инструментальная готовность");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+1,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 2;
		$sheet->setCellValueByColumnAndRow($col,$row,"Базовый уровень");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Уровень ниже базового");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 1;
		$sheet->setCellValueByColumnAndRow($col,$row,"Базовый уровень");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 1. ".count($data['group_students'][1])." уч. (".round(count($data['group_students'][1])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 3. ".count($data['group_students'][3])." уч. (".round(count($data['group_students'][3])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 1;
		$sheet->setCellValueByColumnAndRow($col,$row,"Уровень ниже базового");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 2. ".count($data['group_students'][2])." уч. (".round(count($data['group_students'][2])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа 4. ".count($data['group_students'][4])." уч. (".round(count($data['group_students'][4])*100/count($data['students']))."%)",2);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename='Группы стартовой готовности {$data['ug']['name']}.xls'");

		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}

	function report_individual_start($data)
	{
		$xls = new \PHPExcel();
		$sheet = $xls->setActiveSheetIndex(0);
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$row = 1;
		$col = 0;

		$sheet->setCellValueByColumnAndRow($col,$row,"«Школьный старт». Индивидуальные результаты");
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+3,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$sheet->setCellValueByColumnAndRow($col,$row,"ФИО ребенка: ".$data['students'][$data['id']]['fio']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+2,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col += 3;
		$sheet->setCellValueByColumnAndRow($col,$row,"Класс/группа ДОУ: ".$data['ug']['name']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+1);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 0;
		$sheet->setCellValueByColumnAndRow($col,$row,"Группа стартовой готовности: ".$data['students'][$data['id']]['group']);
		$sheet->mergeCellsByColumnAndRow($col,$row,$col+2,$row);
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		$col = 0;
		$sheet->setCellValueByColumnAndRow($col,$row,"Компоненты стартовой готовности");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Показатели");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Балл ребенка");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$col++;
		$sheet->setCellValueByColumnAndRow($col,$row,"Значение по классу/группе");
		$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$row++;
		foreach ($data['gdp'] as $k => $g)
		{
			$col = 0;
			$length = count($data['dp'][$k]);
			$sheet->setCellValueByColumnAndRow($col,$row,$g['name']);
			$sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+$length-1);
			$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$col++;
			foreach ($data['dp'][$k] as $dd)
			{
				$col = 1;
				$sheet->setCellValueByColumnAndRow($col,$row,$dd['short_name']);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
				$sheet->setCellValueByColumnAndRow($col,$row,$data['results'][$data['students'][$data['id']]['id']][$dd['id']]);
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
				$sheet->setCellValueByColumnAndRow($col,$row,round($data['total'][$dd['id']]/count($data['students']),2));
				$sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col++;
				$row++;
			}
		}

		$sheet = $this->autosize($sheet);

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename='Группы стартовой готовности {$data['ug']['name']}.xls'");

		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}

	function autosize($sheet)
	{
		$last_col = $sheet->getHighestDataColumn();
		foreach ($this->excelColumnRange('A', $last_col) as $col) {
			$sheet
				->getColumnDimension($col)
				->setAutoSize(true);
		}

		$sheet->calculateColumnWidths();

		foreach ($this->excelColumnRange('A', $last_col) as $col) {
			$sheet
				->getColumnDimension($col)
				->setAutoSize(false);
		}

		return $sheet;
	}

	function border($sheet)
	{
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$sheet->getDefaultStyle()->applyFromArray($styleArray);

		return $sheet;
	}

	function excelColumnRange($lower, $upper) {
		++$upper;
		for ($i = $lower; $i !== $upper; ++$i) {
			yield $i;
		}
	}

	function import_download($data = []) {
		/** @var  $students_ctr \students\students */
		$students_ctr = $this->get_controller('students');
		\PHPExcel_Shared_Font::setTrueTypeFontPath(ROOT.'source/fonts/');
		\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$xls = new \PHPExcel();
		$_POST['id_study_group'] = $data['ug']['id'];

		$styleArray = array(
			'font'  => array(
				'name'  => 'Arial',
			));

		$path = tempnam($GLOBALS['info']['temp_dir'], time());
		$header = '<html><head><meta charset="utf-8"></head><body>' . $data['html'] . '</body></html>';

		/** @var  $students_ctr \students\students */
		$students_ctr = $this->get_controller("students");

		/** @var  $dp_ctr \dp\dp */
		$dp_ctr = $this->get_controller('dp');

		/** @var  $results_ctr \results\results */
		$results_ctr = $this->get_controller("results");
		$data['show_stats'] = true;
		$data['download'] = true;

		$temp_data = $data;

		foreach ([1,2,3,4,5] as $class) {
			$data = $temp_data;
			$data['begin'] = $class;
			$data += $dp_ctr->get_all_uud_dp();
			list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($data['ug']['id'], false, $class);
			$dp_ctr->filter_uud_data($data);
			$data['students'] = $students_ctr->get_students_from_ug($data['ug']['id'], $class);

			$index = $class - 1;
			$html = $this->layout_get('uud/report_uud1.html', $data, 'results');
			file_put_contents($path, $header . $html);

			$xls->setActiveSheetIndex($index);
			$sheet = $xls->getActiveSheet();
			$wizard = new \PHPExcel_Reader_HTML();
			$wizard->setSheetIndex($index);
			$wizard->loadIntoExisting($path, $xls);
			$sheet->setTitle('Класс '. $class);
			if ($class < 5) {
				$xls->createSheet();
			}

			$default_style = $sheet->getDefaultStyle();
			$default_style
				->getAlignment()
				->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
				->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

			$last_row = $sheet->getHighestDataRow();
			$last_col = $sheet->getHighestDataColumn();
			$max_height = 0;

			if ($class < 5) {
				$dp_row = 2;
			} else {
				$dp_row = 3;
			}

			for ($row = 1; $row <= $last_row; $row++) {
				foreach ($this->excelColumnRange('A', $last_col) as $col) {
					$sheet->getStyle($col.$row)->applyFromArray($styleArray);
					if ($row == $dp_row) {
						$value = $sheet->getCell($col.$dp_row)->getValue();
						$width = mb_strwidth($value);

						if ($max_height < $width) {
							$max_height = $width;
						}
					}
				}
			}

			for ($row = 1; $row <= $last_row; $row++) {
				if ($row != $dp_row) {
					$height = 20;
				} else {
					$height = $max_height * 8;

					foreach ($this->excelColumnRange('B', $last_col) as $col) {
						$sheet->getStyle($col.$row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$sheet->getRowDimension($row)->setRowHeight($height);
			}

			$this->autosize($sheet);
		}

		$filename = 'Шаблон импорта ' . $data['ug']['name'] .'.xls';
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter = new \PHPExcel_Writer_Excel5($xls);
		$objWriter->save('php://output');
	}
}