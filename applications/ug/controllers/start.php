<?php
namespace ug;

class start extends \Controller {

    function default_method()
    {
		$this->set_global('type', TYPE_START);
		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller('ug');
		$data['groups'] = array();
		if($_SESSION['user']['id_group'] == 1) {
			$data['groups'] = $ug_ctr->get_all(TYPE_START);
		} else {
			$data['groups'] = $ug_ctr->get_all_owner($_SESSION['user']['id_user'], TYPE_START);
		}

        crumbs("Главная","/");
        crumbs("Школьный старт");

		$this->layout_show('start_index.html',$data);
    }
}

