<?php
namespace uploader;

class detect_image extends \Controller {

    private $types = array(
        'image/jpeg',
        'image/png',
        'image/gif'
    );

    private $proxy = "";

    function default_method()
    {
        if ($_POST['data'] != "")
        {
            $urls = array();
            $_POST['data'] = $this->link_it($_POST['data']);
            preg_match_all("/<[Aa][\s]{1}[^>]*[Hh][Rr][Ee][Ff][^=]*=['\"\s]?([^ \"'>\s#]+)[^>]*>/", $_POST['data'], $match);

            $size = sizeof($match);
            for($i = 1; $i < $size; $i++)
            {
                foreach ($match[$i] as $url)
                {
                    $urls[] = $url;
                }
            }

            if ($urls) $this->multi_curl($urls);
        }
        $res['success'] = $_POST['data'];

        echo json_encode($res);
    }

    function link_it($text)
    {
        $text = str_replace("<br>","\n ",$text);
        $text = str_replace("<br />","\n ",$text);
        $ret = $text;
        $ret = preg_replace("#(^|[\n ])([\w]+?://[^ \"\n\r\t<]*)#is", "<a href=\"\\2\">\\2</a>", $ret);
        $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r<]*)#is", "\\1<a href=\"http://\\2\">\\2</a>", $ret);
        $ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);

        return $ret;
    }

    public function set_proxy($host_port)
    {
        $this->proxy = $host_port;
    }

    private function multi_curl($urls)
    {
        $curl_handlers = array();

        foreach ($urls as $url)
        {
            $domain = $this->get_domain($url);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, false);
            if ($domain == DOMAIN_NAME)
                curl_setopt($curl, CURLOPT_COOKIE,"login={$_COOKIE['login']};password={$_COOKIE['password']}");

            if (isset($this->proxy) && !$this->proxy == '')
                curl_setopt($curl, CURLOPT_PROXY, $this->proxy);

            $curl_handlers[] = array('url' => $url,'curl' => $curl,'domain' => $domain);
        }

        $multi_curl_handler = curl_multi_init();

        foreach($curl_handlers as $key => $curl)
        {
            curl_multi_add_handle($multi_curl_handler,$curl['curl']);
        }

        do
        {
            $multi_curl = curl_multi_exec($multi_curl_handler, $active);
        }
        while ($multi_curl == CURLM_CALL_MULTI_PERFORM  || $active);

        require_once(ROOT.'libraries/imaginator/imaginator.php');
        foreach($curl_handlers as $curl)
        {
            if(curl_errno($curl['curl']) == CURLE_OK)
            {
                $type = curl_getinfo($curl['curl'], CURLINFO_CONTENT_TYPE);
                if (in_array($type,$this->types))
                {
                    $data = curl_multi_getcontent($curl['curl']);
                    $needle_url = preg_quote($curl['url'],'/');

                    $tempDir       = sys_get_temp_dir();
                    $tempExtension = '.upload';

                    $tempFile   = tempnam($tempDir, $tempExtension);
                    $tempStream = fopen($tempFile, "w");
                    fwrite($tempStream, $data);
                    fclose($tempStream);

                    $i = new \imaginator($tempFile);
                    $path_big = $i->get('message_big','message');
                    $path_small = $i->get('message_small','message');

                    if ($path_big && $path_small)
                    {
                        $_POST['data'] = preg_replace("/<a href=\"".$needle_url."\">.*<\/a>/i","<a href='{$path_big}'><img src='{$path_small}'></a>",$_POST['data']);
                    }
                }
            }
        }
        curl_multi_close($multi_curl_handler);
        return true;
    }

    function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }
}
