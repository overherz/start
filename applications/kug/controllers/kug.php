<?php
namespace kug;

class kug extends \Controller {
    
    function default_method()
    {
        $this->layout_show('index.html');
    }

    function get_all()
    {
        $query = $this->db->query("select * from kug");
        return $query->fetchAll();
    }
}

