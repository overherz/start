<?php
namespace ug;

class import extends \Controller {

	function default_method()
	{
		switch ($_POST['act'])
		{
			case "get_form":
				$this->get_form();
				break;
			case "import":
				$this->import();
				break;
		}
	}

	/*
	 * provider
	 */
	function import()
	{
		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller("ug");
		$ug = $ug_ctr->get_ug($_POST['id']);

		if ($ug['type'] == TYPE_UUD) {
			$this->import_uud();
		} else {
			$this->import_start();
		}
	}

	function get_form()
	{
		$res['success'] = $this->layout_get("upload_form.html",array('id' => $_POST['id']));
		echo json_encode($res);
	}

	function import_start()
	{
		if (count($_FILES) < 1) $res['error'] = "Файл не выбран";
		if ($_FILES[0]['type'] != "application/vnd.ms-excel") $res['error'] = "Неверный формат файла";

		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller("ug");
		if (!$ug_ctr->check_access($_SESSION['user']['id_user'],$_POST['id'])) $res['error'] = "Нет доступа";

		if ($_POST['id'] == "") $res['error'] = "Передан неверный идентификатор группы";

		if (!$res['error'])
		{
			$query_clear = $this->db->prepare("delete from students where id_study_group=?");
			$query_insert_result = $this->db->prepare("insert into results(id_student, id_dp, value) values(?,?,?)");

			$inputFileName = $_FILES[0]['tmp_name'];

			$this->db->beginTransaction();
			try {
				$objReader = \PHPExcel_IOFactory::createReader('Excel5');
				$objPHPExcel = $objReader->load($inputFileName);
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();

				if (!$query_clear->execute(array($_POST['id']))) $res['error'] = "Ошибка базы данных";

				$data = [];
				$query = $this->db->prepare("SELECT dp.*,gdp.name AS gdp_name
                    FROM dp AS dp
                    LEFT JOIN gdp AS gdp ON dp.id_gdp=gdp.id
                    WHERE gdp.type=?
                    ORDER BY position ASC
                ");
				$query->execute(array(TYPE_START));

				while ($row = $query->fetch()) {
					$data['dp'][] = $row['id'];
				}

				for ($row = 5; $row <= $highestRow; $row++) {
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

					$dp = array();

					foreach($rowData[0] as $k => $v) {
						if ($k == 0) {
							$fio = $v;
						} else {
							if ($v === "") {
								$v = null;
							}
							$dp[] = $v;
						}
					}

					if ($fio == "") {
						$res['error'] = "Не указано ФИО";
					}

					if (!$res['error']) {
						$save_data = array(
							$_POST['id'],
							$fio,
						);

						$query = $this->db->prepare("insert into students(id_study_group, fio) values(?,?)");
						if (!$query->execute($save_data)) {
							$res['error'] = "Ошибка базы данных";
						}
						$id_student = $this->db->lastInsertId();

						if (count($dp) > 0) {
							foreach ($dp as $k => $dpp) {
								if (!empty($data['dp'][$k])) {
									if ($dpp != "" && ($dpp < 0 || $dpp > 2)) {
										$res['error'] = "Некорректное значение";
									}

									if (!$query_insert_result->execute(array($id_student,$data['dp'][$k],$dpp))) {
										$res['error'] = "Ошибка базы данных";
									}
								}
							}
						}
					}
				}
			}
			catch (Exception $e) {
				$res['error'] = "Невозможно прочитать файл";
			}

			if (!$res['error']) {
				$this->db->commit();
				$res['success'] = true;
			} else {
				$this->db->rollBack();
			}
		}

		echo json_encode($res);
	}

	function import_uud()
	{
		$res = [];
		if (count($_FILES) < 1) {
			$res['error'] = "Файл не выбран";
		}

		if ($_FILES[0]['type'] != "application/vnd.ms-excel") {
			$res['error'] = "Неверный формат файла";
		}

		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller("ug");
		if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $_POST['id'])) {
			$res['error'] = "Нет доступа";
		}

		if ($_POST['id'] == "") {
			$res['error'] = "Передан неверный идентификатор группы";
		}

		/** @var  $dp_ctr \dp\dp */
		$dp_ctr = $this->get_controller('dp');
		$max_values = $dp_ctr->get_max_uug_dp_fields_values();

		if (!$res['error']) {
			$query_clear = $this->db->prepare("delete from students where id_study_group=?");
			$query_get_dp_id = $this->db->prepare('select d.id from dp as d
 				INNER JOIN dp_to_class as dc ON dc.id_dp = d.id
 				WHERE d.name = ? AND dc.class = ?
			');

			$inputFileName = $_FILES[0]['tmp_name'];

			$this->db->beginTransaction();

			try {
				$objReader = \PHPExcel_IOFactory::createReader('Excel5');
				$objPHPExcel = $objReader->load($inputFileName);

				$dp = [];
				$classes = [];

				foreach ([1,2,3,4,5] as $class) {
					$sheet_index = $class - 1;
					$objPHPExcel->setActiveSheetIndex($sheet_index);
					$sheet = $objPHPExcel->getSheet($sheet_index);

					$highestRow = $sheet->getHighestRow();
					$highestColumn = $sheet->getHighestColumn();
					$highestColumnNumber = \PHPExcel_Cell::columnIndexFromString($highestColumn);

					if ($class < 5) {
						$dp_from = 2;
						$fields_from = 4;
						$row_from = 5;
					} else {
						$dp_from = 3;
						$fields_from = 5;
						$row_from = 6;
					}

					$dp_names = [];
					$mergedCellsRange = $sheet->getMergeCells();

					for ($col = 0; $col < $highestColumnNumber; $col++ ) {
						$cell = $sheet->getCellByColumnAndRow($col, $dp_from);
						$merged = false;
						foreach($mergedCellsRange as $currMergedRange) {
							if($cell->isInRange($currMergedRange)) {
								$currMergedCellsArray = \PHPExcel_Cell::splitRange($currMergedRange);
								$cell = $sheet->getCell($currMergedCellsArray[0][0]);
								$dp_names[] = $cell->getValue();
								$merged = true;
							}
						}

						if (!$merged) {
							$dp_names[] = $cell->getValue();
						}
					}

					$fields = reset($sheet->rangeToArray('A' . $fields_from . ':' . $highestColumn . $fields_from, NULL, TRUE, FALSE));

					for ($row = $row_from; $row <= $highestRow; $row++) {
						$raw_data = reset($sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE));

						$fio = null;
						foreach($raw_data as $k => $v) {
							if ($k == 0) {
								$fio = $v;
							} else {
								if ($v === "") {
									$v = null;
								}
								$dp[$class][$fio][$dp_names[$k]][$fields[$k]] = $v;
							}
						}

						if (empty($fio)) {
							$res['error'] = "Не указано ФИО в " . $class .' классе';
							break;
						} else {
							$classes[$fio][] = $class;
						}
					}
				}

				if (!empty($dp) && !$res['error']) {
					$dp_ids = [];
					$students = [];
					$update_groups = [];

					$query_clear->execute([$_POST['id']]);
					foreach ($dp as $number_class => $student) {
						foreach ($student as $fio => $dps) {
							$save_data = [
								$_POST['id'],
								$fio,
								reset($classes[$fio]),
								end($classes[$fio]),
							];

							if (empty($students[$fio])) {
								$query = $this->db->prepare("insert into students(id_study_group, fio, begin_start, begin_end) values(?,?,?,?)");
								if (!$query->execute($save_data)) {
									$res['error'] = "Ошибка базы данных";
									goto end;
								} else {
									$id_student = $this->db->lastInsertId();
									$students[$fio] = $id_student;
								}
							} else {
								$id_student = $students[$fio];
							}

							$query_str = "insert into results_uud(id_student, class, id_dp, field, value) values";
							$values_prepare = [];
							$insert_data = [];
							foreach ($dps as $dp_name => $fields) {
								foreach ($fields as $field => $value) {
									if (empty($dp_ids[$number_class][$dp_name])) {
										$query_get_dp_id->execute([$dp_name, $number_class]);
										$dp_from_db = $query_get_dp_id->fetch();
										$id_dp = $dp_from_db['id'];
										$dp_ids[$number_class][$dp_name] = $id_dp;
									}

									if (empty($dp_ids[$number_class][$dp_name])) {
										$res['error'] = 'Не найден показатель ' . $dp_name . ' в базе. Загрузите новый шаблон с сайта';
										goto end;
									} else {
										$id_dp = $dp_ids[$number_class][$dp_name];

										if (!is_null($value)) {
											$value = str_replace(',', '.', $value);
											$max_value = $max_values[$id_dp][$number_class][$field];

											if (strstr($value, '.')) {
												$value = (float)$value;
												if (fmod($value, 0.5) != 0) {
													$res['error'][] = "Некорректное значение в {$number_class} классе. Параметр {$dp_name}. Поле {$field}. Значение {$value}";
													goto end;
												}
											}

											if ($value < 0 || $value > $max_value) {
												$res['error'][] = "Превышено максимальное значение в {$number_class} классе. Параметр {$dp_name}. Поле {$field}. Значение {$value}";
												goto end;
											}
										}

										$values_prepare[] = '(?,?,?,?,?)';
										$insert_data = array_merge($insert_data, [
											$id_student,
											$number_class,
											$id_dp,
											$field,
											$value,
										]);

										$update_groups[$number_class][$id_student][$id_dp][$field] = (string)$value;
									}
								}
							}

							$query_insert_result = $this->db->prepare($query_str . implode(', ', $values_prepare));
							if (!$query_insert_result->execute($insert_data)) {
								$res['error'] = 'Ошибка базы данных';
								goto end;
							}
						}
					}
				}
			} catch (\Exception $e) {
				$res['error'] = "Невозможно прочитать файл";
			}

			/** @var  $student_ctr \students\students */
			$student_ctr = $this->get_controller('students');

			if (!empty($update_groups)) {
				foreach ($update_groups as $number_class => $update_classes) {
					foreach ($update_classes as $id_student => $data_student) {
						if (!$student_ctr->save_groups($data_student, $number_class, $id_student)) {
							$res['error'] = 'Не удалось обновить данные';
							goto end;
						}
					}
				}
			}

			end:
			if (!$res['error']) {
				$this->db->commit();
				$res['success'] = true;
			} else {
				$this->db->rollBack();
			}
		}

		echo json_encode($res);
	}
}

