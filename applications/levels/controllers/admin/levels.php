<?php
namespace admin\levels;

class levels extends \Admin {

    private $module_name = "Уровни ГДП";
    private $limit = 20;

    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            case "edit":
                $this->edit();
                break;
            default: $this->default_show();
        }
    }

    function default_show()
    {
        require_once(ROOT.'libraries/paginator/paginator.php');
        $data = $this->db->query("select SQL_CALC_FOUND_ROWS s.*
            from levels_gdp as s
            LIMIT {$this->limit}
            OFFSET ".\Paginator::get_offset($this->limit,$_POST['page'])."
        ")->fetchAll();

        $paginator = new \Paginator($this->db->found_rows(), $_POST['page'], $this->limit);

        if (defined('AJAX') && AJAX)
        {
            $res['success'] = $this->layout_get('admin/table.html',array('data' => $data,'paginator' => $paginator));
            echo json_encode($res);
        }
        else $this->layout_show('admin/index.html',array('data' => $data,'paginator' => $paginator));
    }

    function edit()
    {
        if ($_POST['id'] != "")
        {
            if ($element = $this->get($_POST['id']))
            {
                $res['success'] = $this->layout_get('admin/form.html',array("element" => $element,'mode' => 'edit'));
            }
        }
        else $res['error'] = "Переданые неверные данные";

        echo json_encode($res);
    }

    function save()
    {
        if ($_POST['min_value'] == "") $res['error'] = "Укажите минимум";
        if ($_POST['max_value'] == "") $res['error'] = "Укажите максимум";
        if ($_POST['min_value'] > $_POST['max_value']) $res['error'] = "Минимум не может быть больше максимума";

        if (!$res['error'])
        {
            $save_data = array(
                $_POST['min_value'],
                $_POST['max_value']
            );
            if ($_POST['id'] != "")
            {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE levels_gdp set min_value=?,max_value=? where id=?");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
            }

            if (!$res['error']) $res['success'] = true;
        }

        echo json_encode($res);
    }

    function get($id)
    {
        $query = $this->db->prepare("select * from levels_gdp where id=?");
        $query->execute(array($id));
        $element = $query->fetch();

        return $element;
    }
}

