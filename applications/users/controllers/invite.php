<?php
namespace users;

class invite extends \Controller {
    
    function default_method()
    {
        switch ($_POST['act'])
        {
            case "send_invite":
                $this->send_invite();
                break;
            case "get_invite_form":
                $this->get_invite_form();
                break;
        }
    }

    function get_invite_form()
    {
        $res['success'] = $this->layout_get("invite_form.html");
        echo json_encode($res);
    }

    function send_invite()
    {
        if ($GLOBALS['globals']['role_company'] != "admin") $res['error'] = "Приглашать могут только администраторы";
        if (trim($_POST['name']) == "") $res['error'] = "Укажите временное имя";
        if (!check_mail($_POST['email'])) $res['error'] = "Email неверен";
        else
        {
            $query = $this->db->prepare("select u.id_user,cu.id_company,cu.invite from users as u
              LEFT JOIN company_users as cu ON u.id_user=cu.id_user
              where u.email=? and cu.id_company=?
            ");
            $query->execute(array($_POST['email'],$GLOBALS['globals']['current_company']));
            $user = $query->fetch();
            if ($user['id_company'] && !$user['invite']) $res['error'] = "Данный адрес уже является участником";
        }

        if (!$res['error'])
        {
            $this->db->beginTransaction();

            $query = $this->db->prepare("select id_user from users where email=?");
            $query->execute(array($_POST['email']));
            if ($user = $query->fetch())
            {
                $id_user = $user['id_user'];
            }
            else
            {
                $query = $this->db->prepare("insert into users (email,first_name,id_group,created,mailconfirm) values(?,?,?,?,?)");
                if (!$query->execute(array($_POST['email'],$_POST['name'],DEFAULT_USER_GROUP_ID,time(),false))) $res['error'] = 'Ошибка при создании пользователя';
                else $id_user = $this->db->lastInsertId();
            }

            $query = $this->db->prepare("insert into company_users(id_company,id_user,role,invite) values(?,?,?,?) ON DUPLICATE KEY UPDATE role=?");
            if (!$query->execute(array($GLOBALS['globals']['current_company'],$id_user,"user",true,"user"))) $res['error'] = 'Ошибка при создании пользователя';

            $hash = md5(md5(time()).md5($_POST['email']));
            $query = $this->db->prepare("insert into invites(email,hash,date,id_company) values(?,?,?,?) ON DUPLICATE KEY UPDATE hash=?");
            $company = $this->get_controller('company')->get_company($GLOBALS['globals']['current_company']);
            if ($query->execute(array($_POST['email'],$hash,time(),$GLOBALS['globals']['current_company'],$hash)))
            {
                $subject = "Приглашение";
                $message = $this->layout_get("elements/invite_mail.html",array('user' => $user,'company' => $company,'hash' => $hash,'domain' => get_full_domain_name(SUBDOMAIN),'site_name' => get_setting('site_name')));
                if (!send_mail(get_setting('email'), $_POST['email'], $subject, $message,get_setting('site_name')))
                {
                    $res['error'] = "Ошибка при отправке письма";
                    $this->db->rollBack();
                }
                else {
                    $res['success'] = true;
                    $this->db->commit();
                }
            }
            else $res['error'] = "Ошибка базы данных";
        }

        echo json_encode($res);
    }

    function get_invite($hash)
    {
        $query = $this->db->prepare("select i.* from invites as i
          LEFT JOIN company_users as cu ON i.id_company=cu.id_company
          where hash=? and cu.id_company IS NOT NULL
          group by cu.id_company
        ");
        $query->execute(array($hash));
        return $query->fetch();
    }

    function get_invite_email_company($email,$id_company)
    {
        $query = $this->db->prepare("select * from invites as i
          LEFT JOIN company_users as cu ON i.id_company=cu.id_company
          where i.email=? and i.id_company=?
          group by cu.id_company
        ");
        $query->execute(array($email,$id_company));
        return $query->fetch();
    }

    function delete_invite($hash)
    {
        $query = $this->db->prepare("delete from invites where hash=?");
        if ($query->execute(array($hash))) return true;
    }
}

