$(document).ready(function($) {
    if ($("#date").length > 0 || $('#date-result').length > 0)
    {
        $("#date, #date-result").datepicker({
            language: 'ru',
            format: 'dd.mm.yyyy',
            autoclose: true,
            orientation: 'bottom'
        }).on('hide', function(event) {
            event.preventDefault();
            event.stopPropagation();
        });
    }

    $(document).off('click', '#date-result-save').on('click', '#date-result-save', function () {
        user_api(
            {
                act: 'date_result_save',
                date: $("#date-result").val(),
                begin: $("[name='begin']").val(),
                id_study_group: $("[name='id_study_group']").val()
            },
            function(res){
                show_message('success', 'Дата сохранена');
            },false,'/ug/edit');

        return false;
    });

    $(document).off('click','save').on("click","#save",function(){
        var params = $("#edit_form").find(":input").serialize();
        var type = $("#add_type").val();
        user_api(params,function() {
            show_message('success','Сохранено');
            redirect('/ug/' + type, 2);
        }, false, '/ug/add/' + type);

        return false;
    });
/*TODO Здесь должно быть 2 разных формы - для старта и для ууд */
    $(document).off("click",".edit_ug").on("click",".edit_ug",function(){
        user_api({id:$(this).data('id')},function(res){
            var title = 'Редактирование данных учебной группы';
            if (location.href.indexOf("uud") > 0)
                title = 'Редактирование данных класса';

            show_modal(res,title);
            add_modal_button("Сохранить","save",false,function(){
                var params = $("#edit_form").find(":input").serialize();
                user_api(params,function(res){
                    show_message('success','Группа сохранена');
                    redirect(false,2);
                },false,'/ug/add');
            });
        },false,'/ug/edit');

        return false;
    });

    $(document).off('change',"[name='kug']").on("change","[name='kug']",function(){
        var id = $(this).val();
        $(".umk_ul").hide();
        $(".umk_ul[data-id='"+id+"']").show();
    });

    $(document).off("click",".edit_student").on("click",".edit_student",function(){
        user_api(
            {
                act: 'get_form_edit',
                id: $(this).data('id'),
                id_study_group: $("[name='id_study_group']").val(),
                begin: $(this).data('begin')
            },
            function(res){
                show_modal(res,"Редактирование данных ребенка");
                save_buttons();
        },false,'/students');

        return false;
    });

    function save_buttons()
    {
        add_modal_button("Сохранить","save",false,function(){
            var params = $("#edit_student").serialize();
            user_api(params,function(data){
                show_message('success','Ученик сохранен');
                if (data.begin) {
                    redirect(window.location.href.split('?')[0] + '?begin=' + data.begin);
                } else {
                    redirect(false,2);
                }
            },function(res){
                if (res.force_save)
                {
                    $("#force_save_alert").show();
                    add_modal_button("Заполнить позже","save",false,function(){
                        var params = $("#edit_student").serialize();
                        params = params + '&'+$.param({ 'force_save': true });
                        user_api(params,function(data) {
                            show_message('success', 'Ученик сохранен');
                            if (data.begin) {
                                redirect(window.location.href.split('?')[0] + '?begin=' + data.begin);
                            } else {
                                redirect(false,2);
                            }
                        },false,'/students');
                    });

                    add_modal_button("Внести недостающие данные сейчас","enter",false,function(){
                        $("#force_save_alert").hide();
                        $(".enter_value").each(function(){
                            if ($(this).val() == "") $(this).parent().addClass('bg-danger');
                        });
                        remove_modal_button("enter");
                        save_buttons();
                    });
                }
                else show_message('error',res);
            },'/students');
        });
    }

    $(document).off("click","#delete_student_confirm").on("click","#delete_student_confirm",function(){
        $("#delete_student_form").show();
        return false;
    });

    $(document).off("click","#delete_student_cancel").on("click","#delete_student_cancel",function(){
        $("#delete_student_form").hide();
        return false;
    });

    $(document).off("click","#delete_student").on("click","#delete_student",function(){
        user_api({act:'delete',id:$(this).data('id')},function(res){
            show_message('success','Ученик удален');
            redirect(false,2);
        },false,'/students');
        return false;
    });

    $(document).off("click",".individual_report").on("click",".individual_report",function(){
        var begin = $(this).data('begin');
        if (typeof begin === 'undefined') {
            begin = '';
        }
        redirect($(".students" + begin).val());
        return false;
    });

    var files = [];
    $(document).off("change","#upload_excel input[type=file]").on("change","#upload_excel input[type=file]",function(){
        files = this.files;
        $('#upload-file-info').html($(this).val());
    });

    $(document).off("click",".get_upload_form").on("click",".get_upload_form",function(){
        user_api({act:'get_form',id:$(this).data('id')},function(res){
            show_modal(res,"Загрузка результатов диагностики из Excel");
            add_modal_button("Загрузить","upload",false,function(){
                var data = new FormData();
                $.each( files, function( key, value ){
                    data.append( key, value );
                });

                data.append('act', 'import');
                data.append('id', $("#upload_excel [name='id']").val());

                $.ajax({
                    url: '/ug/import',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(res)
                    {
                        hide_modal();
                        if(res.error) show_message("error",res.error);
                        else if (res.success)
                        {
                            show_message('success','Файл успешно загружен и обработан');
                            redirect(false,2);
                        }
                    },
                    error: function(res)
                    {
                        hide_modal();
                        show_message("error",res.error);
                    }
                });
            });
        },false, '/ug/import');
        return false;
    });
});
