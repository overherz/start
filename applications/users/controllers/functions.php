<?php
namespace users;

class functions extends \Controller {

    public $regions = array(
        //'Africa' => \DateTimeZone::AFRICA,
        //'America' => \DateTimeZone::AMERICA,
        //'Antarctica' => \DateTimeZone::ANTARCTICA,
        //'Asia' => \DateTimeZone::ASIA,
        //'Atlantic' => \DateTimeZone::ATLANTIC,
        //'Australia' => \DateTimeZone::AUSTRALIA,
        'Europe' => \DateTimeZone::EUROPE,
        //'Indian' => \DateTimeZone::INDIAN,
        //'Pacific' => \DateTimeZone::PACIFIC,
    );

    function default_method()
    {
        switch($_POST['act'])
        {
            case "get_cities_region":
                $this->get_cities_region($_POST['region']);
                break;
            case "get_timezones":
                $this->get_timezones();
                break;
        }
    }

    function get_cities_region($region)
    {
        $this->get_controller("users")->get_cities_region($region);
    }

    function generate_timezone_list($region)
    {
        $timezones = array();
        $timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );

        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new \DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
        }

        // sort timezone by offset
        asort($timezone_offsets);

        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }

        return $timezone_list;
    }

    function get_timezones()
    {
        $res['success'] = true;
        $list = "";
        if ($timezones = $this->generate_timezone_list($this->regions[$_POST['timezone_region']]))
        {
            foreach ($timezones as $k => $timezone)
            {
                $list .= "<option title='{$timezone}' value='{$k}'>{$timezone}</option>";
            }
        }

        if ($list != "") $res['success'] = $list;

        echo json_encode($res);
    }

    function get_region_from_timezone($zone)
    {
        if (!$zone) return false;
        $timezones = array();
        foreach($this->regions as $k => $region)
        {
            $timezones[$k] = \DateTimeZone::listIdentifiers($region);
        }

        foreach($timezones as $k => $timezone )
        {
            foreach ($timezone as $kk => $vv)
            {
                if ($vv == $zone) return $k;
            }
        }
    }
}

