<?php

use Phinx\Migration\AbstractMigration;

class DpToDp extends AbstractMigration
{
	const
		TABLE_DP_TO_DP = 'dp_to_dp',
		TABLE_DP_SUBGROUPS = 'dp_subgroups',
		TABLE_DP_TO_SUBGROUPS = 'do_to_subgroup';

	private $data = [
		'границы' => ['постановка учебной цели', 'гипотеза'],
		'планирование' => ['планирование учебных действий', 'эффективный способ'],
		'коррекция' => ['текущий контроль'],
		'контроль' => ['контроль результата'],
		'оценка' => ['оценивание', 'рефлексия'],
		'анализ' => ['сравнение'],
		'синтез' => ['сравнение'],
		'сравнение' => ['обобщение'],
		'обобщение' => ['обобщение'],
		'причинно-следственные связи' => ['обобщение'],
		'классификация' => ['аналогия'],
		'аналогия' => ['индукция'],
		'индуктивное умозаключение' => ['индукция'],
		'отнесение к понятию' => ['определение понятия', 'подведение под понятие'],
		'диаграммы и таблицы' => ['построение схемы', 'использование схемы'],
		'вопросы' => ['неявная информация', 'главная мысль'],
		'объединение информации' => ['объединение информации', 'противоречия'],
		'речевое высказывание' => ['аргументация'],
		'точка зрения' => ['передача информации'],
	];

	private $groups = [
		'целеполагание' => ['постановка учебной цели', 'гипотеза'],
		'планирование' => ['планирование учебных действий', 'эффективный способ'],
		'контроль' => ['текущий контроль', 'контроль результата'],
		'оценивание и рефлексия' => ['оценивание', 'рефлексия'],
		'мыслительные операции' => ['сравнение', 'обобщение'],
		'умозаключения' => ['аналогия', 'индукция'],
		'понятия' => ['определение понятия', 'подведение под понятие'],
		'модели и схемы' => ['построение схемы', 'использование схемы'],
		'понимание информации' => ['неявная информация', 'главная мысль'],
		'объединение информации' => ['объединение информации', 'противоречия'],
		'изложение информации' => ['аргументация', 'передача информации']
	];

	public function up()
	{
		$this->to_capitalize();
		$this->rename();

		try {
			$table = $this->table(self::TABLE_DP_TO_DP);
			$table
				->addColumn('class_from', 'integer')
				->addColumn('class_to', 'integer')
				->addColumn('dp_from', 'integer')
				->addColumn('dp_to', 'integer')
				->addIndex(['class_from', 'class_to', 'dp_from', 'dp_to'], ['unique' => true])
				->addForeignKey('dp_from', 'dp', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
				->addForeignKey('dp_to', 'dp', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
				->save();
		} catch (\Exception $e) {
			echo $e . PHP_EOL;
		}

		$this->query('TRUNCATE ' . self::TABLE_DP_TO_DP);

		foreach ($this->data as $k => $data) {
			foreach ($data as $datum) {
				$from = $this->get_dp_id($this->capitalize($k), 4);
				$to = $this->get_dp_id($this->capitalize($datum), 5);

				$insert = [
					'class_from' => 4,
					'class_to' => 5,
					'dp_from' => $from,
					'dp_to' => $to,
				];

				$this->insert(self::TABLE_DP_TO_DP, $insert);
			}
		}

		$table = $this->table(self::TABLE_DP_SUBGROUPS);
		$table
			->addColumn('sub_name', 'string')
			->save();

		foreach ($this->groups as $k => $data) {
			$insert = [
				'sub_name' => $this->capitalize($k),
			];

			$this->insert(self::TABLE_DP_SUBGROUPS, $insert);
		}

		$table = $this->table(self::TABLE_DP_TO_SUBGROUPS);
		$table
			->addColumn('id_subgroup', 'integer')
			->addColumn('id_dp', 'integer')
			->addForeignKey('id_subgroup', self::TABLE_DP_SUBGROUPS, 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
			->addForeignKey('id_dp', 'dp', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
			->addIndex(['id_subgroup', 'id_dp'], ['unique' => true])
			->save();

		foreach ($this->groups as $k => $data) {
			foreach ($data as $datum) {
				$insert = [
					'id_subgroup' => $this->get_subgroup_id($this->capitalize($k)),
					'id_dp' => $this->get_dp_id($this->capitalize($datum), 5),
				];

				$this->insert(self::TABLE_DP_TO_SUBGROUPS, $insert);
			}
		}
	}

	public function down()
	{
		$this->dropTable(self::TABLE_DP_TO_DP);
		$this->dropTable(self::TABLE_DP_TO_SUBGROUPS);
		$this->dropTable(self::TABLE_DP_SUBGROUPS);
	}

	private function get_dp_id($name, $class)
	{
		$row = $this->fetchRow("SELECT dp.id FROM dp as dp
			JOIN dp_to_class as c ON dp.id = c.id_dp AND class = {$class}
 			WHERE dp.name = '{$name}'");
		return $row['id'];
	}

	private function get_subgroup_id($name)
	{
		$row = $this->fetchRow("SELECT id FROM " . self::TABLE_DP_SUBGROUPS . " WHERE sub_name = '{$name}'");
		return $row['id'];
	}

	private function to_capitalize()
	{
		$rows = $this->fetchAll("SELECT id, name, short_name FROM dp");
		foreach ($rows as $row) {
			$name = $this->capitalize($row['name']);
			$short_name = $this->capitalize($row['short_name']);

			$this->query("UPDATE dp set name = '{$name}', short_name = '{$short_name}' WHERE id = {$row['id']} LIMIT 1");
		}
	}

	private function capitalize($str) {
		$str = mb_strtoupper(mb_substr($str, 0,1)) . mb_substr($str, 1);
		return $str;
	}

	private function rename()
	{
		$this->query("UPDATE dp
			set name = 'Неявная информация',
				short_name = 'Неявная информация'
            WHERE name = 'Неявная инфомарция' 
            LIMIT 1
        ");
	}
}
