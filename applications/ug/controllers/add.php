<?php
namespace ug;

class add extends \Controller {
    
    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            default:
                $this->default_show();
        }
    }

    function default_show()
    {
    	$data = [];

        if ($this->id == 'start') {
            crumbs("Главная","/");
            crumbs("Школьный старт","/ug/" . $this->id);
            crumbs("Новая учебная группа");
			$this->set_global('type', TYPE_START);
            $data['type'] = 'start';

            /** @var  $kug_ctr \kug\kug */
            $kug_ctr = $this->get_controller("kug");
            $data['kug'] = $kug_ctr->get_all();

            /** @var  $umk_ctr \umk\umk */
            $umk_ctr = $this->get_controller("umk");
            $data['umk'] = $umk_ctr->get_all_by_kug();
        } else {
            crumbs("Главная","/");
            crumbs("Учимся Учиться и Действовать","/ug/uud");
            crumbs("Новый класс");
			$this->set_global('type', TYPE_UUD);
            $data['type'] = 'uud';
        }

        $this->layout_show('add.html', $data);
    }

    function save()
    {
        foreach ($_POST as &$d) {
            if (!is_array($d)) {
                $d = trim($d);
            }
        }
        unset($d);

        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller("ug");
        $check_for_start = false;
        if (!empty($_POST['id'])) {
            $ug = $ug_ctr->get_ug($_POST['id']);
            if ($ug['type'] == TYPE_START) {
                $check_for_start = true;
            }
        } else if ($this->id == 'start') {
            $check_for_start = true;
        }

        if (count($_POST['umk']) < 1 && $check_for_start) {
            $res['error'] = "Выберите учебно‐методические комплекты";
        }

        if ($_POST['name'] == "") {
            $res['error'] = "Введите название группы";
        }

        if ($_POST['date'] == "" && $check_for_start) {
            $res['error'] = "Укажите дату";
        }

        if ($_POST['name_education'] == "") {
            $res['error'] = "Введите название образовательной организации";
        }

        $this->db->beginTransaction();
        if (!$res['error'])
        {
            $save_data = array(
                $_SESSION['user']['id_user'],
                $_POST['name'],
                $_POST['name_education'],
                strtotime($_POST['date'])
            );

            if (!empty($_POST['id']))
            {
                if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $_POST['id'])) {
                    $res['error'] = "Нет доступа";
                }

                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE study_groups set owner=?,name=?,name_education=?,date=? where id=?");
                if (!$query->execute($save_data)) {
                    $res['error'] = "Ошибка базы данных";
                }

                $query = $this->db->prepare("delete from study_groups_to_umk where id_study_group=?");
                if (!$query->execute(array($_POST['id']))) {
                    $res['error'] = "Ошибка базы данных";
                }

                $id_group = $_POST['id'];
            }
            else
            {
                $types = \Controller::get_global("projects_map");
                $save_data[] = $types[$this->id];

                $query = $this->db->prepare("INSERT INTO study_groups(owner,name,name_education,date,type) VALUES(?,?,?,?,?)");
                if (!$query->execute($save_data)) {
                    $res['error'] = "Ошибка базы данных";
                } else {
                    $id_group = $this->db->lastInsertId();
                }
            }

            $query_insert_umk = $this->db->prepare("insert into study_groups_to_umk(id_study_group, id_umk) VALUES (?,?)");

            foreach ($_POST['umk'] as $u) {
                if (!$query_insert_umk->execute(array($id_group,$u))) {
                    $res['error'] = "Ошибка базы данных";
                }
            }
        }

        if (!$res['error']) {
            $res['success'] = true;
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }

        echo json_encode($res);
    }
}

