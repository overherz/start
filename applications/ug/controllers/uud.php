<?php
namespace ug;

class uud extends \Controller {
    
    function default_method()
    {
		$this->set_global('type', TYPE_UUD);
        $data['type'] = TYPE_UUD;
		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller('ug');
		$data['groups'] = array();
		if($_SESSION['user']['id_group'] == 1) {
			$data['groups'] = $ug_ctr->get_all(TYPE_UUD);
		} else {
			$data['groups'] = $ug_ctr->get_all_owner($_SESSION['user']['id_user'], TYPE_UUD);
		}

        crumbs("Главная","/");
        crumbs("Учимся Учиться и Действовать");

		$this->layout_show('uud_index.html',$data);
    }
}

