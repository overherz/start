<?php
namespace admin\server;

class database extends \Admin {

    function default_method()
    {
        $message = null;
        $data = array();

        $vars = array();
        $rs = $this->db->query("SHOW GLOBAL VARIABLES");
        while ($ar = $rs->fetch())
            $vars[$ar["Variable_name"]] = $ar["Value"];

        $stat = array();
        $rs = $this->db->query("SHOW GLOBAL STATUS");
        if (!$rs)
            $rs = $this->db->query("SHOW STATUS");
        while ($ar = $rs->fetch())
            $stat[$ar["Variable_name"]] = $ar["Value"];

        $arVersion = array();
        if (preg_match("/^(\\d+)\\.(\\d+)/", $vars["version"], $arVersion))
        {
            if ($arVersion[1] < 5)
                $rec = lang("PERFMON_KPI_REC_VERSION_OLD");
            elseif ($arVersion[1] == 5)
                $rec = lang("PERFMON_KPI_REC_VERSION_OK");
            else
                $rec = lang("PERFMON_KPI_REC_VERSION_NEW");
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_VERSION"),
                "IS_OK" => $arVersion[1] == 5,
                "KPI_VALUE" => $vars["version"],
                "KPI_RECOMMENDATION" => $rec,
            );
        }
        $uptime = array(
            "#DAYS#" => intval($stat['Uptime'] / (86400)),
            "#HOURS#" => intval(($stat['Uptime'] % 86400) / (3600)),
            "#MINUTES#" => intval(($stat['Uptime'] % 3600) / 60),
            "#SECONDS#" => $stat['Uptime'] % 60,
        );

        if ($stat['Uptime'] >= 86400)
            $rec = lang("PERFMON_KPI_REC_UPTIME_OK");
        else
            $rec = lang("PERFMON_KPI_REC_UPTIME_TOO_SHORT");
        $data[] = array(
            "KPI_NAME" => lang("PERFMON_KPI_NAME_UPTIME"),
            "IS_OK" => $stat['Uptime'] >= 86400,
            "KPI_VALUE" => lang("PERFMON_KPI_VAL_UPTIME", $uptime),
            "KPI_RECOMMENDATION" => $rec,
        );

        if ($stat["Questions"] < 1)
        {
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_QUERIES"),
                "IS_OK" => false,
                "KPI_VALUE" => $stat["Questions"],
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_NO_QUERIES"),
            );
        }
        else
        {
            // Server-wide memory
            $calc['server_buffers'] = $vars['key_buffer_size'];
            $server_buffers = 'key_buffer_size';
            if ($vars['tmp_table_size'] > $vars['max_heap_table_size'])
            {
                $calc['server_buffers'] += $vars['max_heap_table_size'];
                $server_buffers .= ' + max_heap_table_size';
            }
            else
            {
                $calc['server_buffers'] += $vars['tmp_table_size'];
                $server_buffers .= ' + tmp_table_size';
            }

            if (isset($vars['innodb_buffer_pool_size']))
            {
                $calc['server_buffers'] += $vars['innodb_buffer_pool_size'];
                $server_buffers .= ' + innodb_buffer_pool_size';
            }

            if (isset($vars['innodb_additional_mem_pool_size']))
            {
                $calc['server_buffers'] += $vars['innodb_additional_mem_pool_size'];
                $server_buffers .= ' + innodb_additional_mem_pool_size';
            }

            if (isset($vars['innodb_log_buffer_size']))
            {
                $calc['server_buffers'] += $vars['innodb_log_buffer_size'];
                $server_buffers .= ' + innodb_log_buffer_size';
            }

            if (isset($vars['query_cache_size']))
            {
                $calc['server_buffers'] += $vars['query_cache_size'];
                $server_buffers .= ' + query_cache_size';
            }
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_GBUFFERS"),
                "KPI_VALUE" => $this->format_file_size($calc['server_buffers']),
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_GBUFFERS", array("#VALUE#" => $server_buffers)),
            );

            // Per thread
            $calc['per_thread_buffers'] = $vars['read_buffer_size'] + $vars['read_rnd_buffer_size'] + $vars['sort_buffer_size'] + $vars['thread_stack'] + $vars['join_buffer_size'];
            $per_thread_buffers = 'read_buffer_size + read_rnd_buffer_size + sort_buffer_size + thread_stack + join_buffer_size';
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_CBUFFERS"),
                "KPI_VALUE" => $this->format_file_size($calc['per_thread_buffers']),
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_CBUFFERS", array("#VALUE#" => $per_thread_buffers)),
            );

            $max_connections = 'max_connections';
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_CONNECTIONS"),
                "KPI_VALUE" => $vars['max_connections'],
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_CONNECTIONS", array("#VALUE#" => $max_connections)),
            );

            // Global memory
            $calc['total_possible_used_memory'] = $calc['server_buffers'] + ($calc['per_thread_buffers'] * $vars['max_connections']);
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_MEMORY"),
                "KPI_VALUE" => $this->format_file_size($calc['total_possible_used_memory']),
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_MEMORY"),
            );

            // Query cache
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_CACHE_TYPE"),
                "KPI_VALUE" => $vars['query_cache_type']
            );

            if ($vars['query_cache_size'] < 1)
                $rec = lang("PERFMON_KPI_REC_QCACHE_ZERO_SIZE", array(
                    "#PARAM_NAME#" => "query_cache_size",
                    "#PARAM_VALUE_LOW#" => "8M",
                    "#PARAM_VALUE_HIGH#" => "128M",
                ));
            elseif ($vars['query_cache_size'] > 128 * 1024 * 1024)
                $rec = lang("PERFMON_KPI_REC_QCACHE_TOOLARGE_SIZE", array(
                    "#PARAM_NAME#" => "query_cache_size",
                    "#PARAM_VALUE_HIGH#" => "128M",
                ));
            else
                $rec = lang("PERFMON_KPI_REC_QCACHE_OK_SIZE", array(
                    "#PARAM_NAME#" => "query_cache_size",
                ));
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_QCACHE_SIZE"),
                "IS_OK" => $vars['query_cache_size'] > 0 && $vars['query_cache_size'] <= 128 * 1024 * 1024,
                "KPI_VALUE" => $this->format_file_size($vars['query_cache_size']),
                "KPI_RECOMMENDATION" => $rec,
            );

            if ($vars['query_cache_size'] > 0)
            {
                if ($stat['Com_select'] == 0)
                {
                    $value = "&nbsp;";
                    $rec = lang("PERFMON_KPI_REC_QCACHE_NO");
                }
                else
                {
                    $calc['query_cache_efficiency'] = @round($stat['Qcache_hits'] / (($stat['Com_select'] - $stat['Qcache_not_cached']) + $stat['Qcache_hits']) * 100, 2);
                    if (is_nan($calc['query_cache_efficiency'])) $calc['query_cache_efficiency'] = 0;
                    $value = $calc['query_cache_efficiency']."%";
                    $rec = lang("PERFMON_KPI_REC_QCACHE", array(
                        "#GOOD_VALUE#" => "20%",
                        "#PARAM_NAME#" => "query_cache_limit",
                        "#PARAM_VALUE#" => $this->format_file_size($vars['query_cache_limit']),
                    ));
                }
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_QCACHE"),
                    "IS_OK" => $stat['Com_select'] > 0 && $calc['query_cache_efficiency'] >= 20,
                    "KPI_VALUE" => $value,
                    "KPI_RECOMMENDATION" => $rec,
                );

                if ($stat['Com_select'] > 0)
                {
                    $data[] = array(
                        "KPI_NAME" => lang("PERFMON_KPI_NAME_QCACHE_PRUNES"),
                        "KPI_VALUE" => $stat['Qcache_lowmem_prunes'],
                        "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_QCACHE_PRUNES", array(
                            "#STAT_NAME#" => "Qcache_lowmem_prunes",
                            "#PARAM_NAME#" => "query_cache_size",
                            "#PARAM_VALUE#" => $this->format_file_size($vars['query_cache_size']),
                            "#PARAM_VALUE_HIGH#" => "128M",
                        )),
                    );
                }
            }
            // Sorting
            $calc['total_sorts'] = $stat['Sort_scan'] + $stat['Sort_range'];
            $total_sorts = 'Sort_scan + Sort_range';
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_SORTS"),
                "KPI_VALUE" => $calc['total_sorts'],
                "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_SORTS", array(
                    "#STAT_NAME#" => $total_sorts,
                )),
            );

            if ($calc['total_sorts'] > 0)
            {
                $calc['pct_temp_sort_table'] = round(($stat['Sort_merge_passes'] / $calc['total_sorts']) * 100, 2);
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_SORTS_DISK"),
                    "IS_OK" => $calc['pct_temp_sort_table'] <= 10,
                    "KPI_VALUE" => $calc['pct_temp_sort_table']."%",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_SORTS_DISK", array(
                        "#STAT_NAME#" => "Sort_merge_passes / (Sort_scan + Sort_range)",
                        "#GOOD_VALUE#" => "10",
                        "#PARAM1_NAME#" => "sort_buffer_size",
                        "#PARAM1_VALUE#" => $this->format_file_size($vars['sort_buffer_size']),
                        "#PARAM2_NAME#" => "read_rnd_buffer_size",
                        "#PARAM2_VALUE#" => $this->format_file_size($vars['read_rnd_buffer_size']),
                    )),
                );
            }

            // Joins
            $calc['joins_without_indexes'] = $stat['Select_range_check'] + $stat['Select_full_join'];
            $calc['joins_without_indexes_per_day'] = intval($calc['joins_without_indexes'] / ($stat['Uptime'] / 86400));
            if ($calc['joins_without_indexes_per_day'] > 250)
            {
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_JOINS"),
                    "KPI_VALUE" => $calc['joins_without_indexes'],
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_JOINS", array(
                        "#STAT_NAME#" => "Select_range_check + Select_full_join",
                        "#PARAM_NAME#" => "join_buffer_size",
                        "#PARAM_VALUE#" => $this->format_file_size($vars['join_buffer_size']),
                    )),
                );
            }

            // Temporary tables
            if ($stat['Created_tmp_tables'] > 0)
            {
                $calc['tmp_table_size'] = ($vars['tmp_table_size'] > $vars['max_heap_table_size'])? $vars['max_heap_table_size']: $vars['tmp_table_size'];
                if ($stat['Created_tmp_disk_tables'] > 0)
                    $calc['pct_temp_disk'] = round(($stat['Created_tmp_disk_tables'] / ($stat['Created_tmp_tables'] + $stat['Created_tmp_disk_tables'])) * 100, 2);
                else
                    $calc['pct_temp_disk'] = 0;
                $pct_temp_disk = 30;

                if ($calc['pct_temp_disk'] > $pct_temp_disk && $calc['max_tmp_table_size'] < 256 * 1024 * 1024)
                {
                    $is_ok = false;
                    $value = $calc['pct_temp_disk']."%";
                    $rec = lang("PERFMON_KPI_REC_TMP_DISK_1", array(
                        "#STAT_NAME#" => "Created_tmp_disk_tables / (Created_tmp_tables + Created_tmp_disk_tables)",
                        "#STAT_VALUE#" => $pct_temp_disk."%",
                        "#PARAM1_NAME#" => "tmp_table_size",
                        "#PARAM1_VALUE#" => $this->format_file_size($vars['tmp_table_size']),
                        "#PARAM2_NAME#" => "max_heap_table_size",
                        "#PARAM2_VALUE#" => $this->format_file_size($vars['max_heap_table_size']),
                    ));
                }
                elseif ($calc['pct_temp_disk'] > $pct_temp_disk && $calc['max_tmp_table_size'] >= 256)
                {
                    $is_ok = false;
                    $value = $calc['pct_temp_disk']."%";
                    $rec = lang("PERFMON_KPI_REC_TMP_DISK_2", array(
                        "#STAT_NAME#" => "Created_tmp_disk_tables / (Created_tmp_tables + Created_tmp_disk_tables)",
                        "#STAT_VALUE#" => $pct_temp_disk."%",
                    ));
                }
                else
                {
                    $is_ok = true;
                    $value = $calc['pct_temp_disk']."%";
                    $rec = lang("PERFMON_KPI_REC_TMP_DISK_3", array(
                        "#STAT_NAME#" => "Created_tmp_disk_tables / (Created_tmp_tables + Created_tmp_disk_tables)",
                        "#STAT_VALUE#" => $pct_temp_disk."%",
                    ));
                }
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_TMP_DISK"),
                    "IS_OK" => $is_ok,
                    "KPI_VALUE" => $value,
                    "KPI_RECOMMENDATION" => $rec,
                );
            }

            // Thread cache
            if ($vars['thread_cache_size'] == 0)
            {
                $is_ok = false;
                $value = $vars['thread_cache_size'];
                $rec = lang("PERFMON_KPI_REC_THREAD_NO_CACHE", array(
                    "#PARAM_VALUE#" => 4,
                    "#PARAM_NAME#" => "thread_cache_size",
                ));
            }
            else
            {
                $calc['thread_cache_hit_rate'] = round(100 - (($stat['Threads_created'] / $stat['Connections']) * 100), 2);
                $is_ok = $calc['thread_cache_hit_rate'] > 50;
                $value = $calc['thread_cache_hit_rate']."%";
                $rec = lang("PERFMON_KPI_REC_THREAD_CACHE", array(
                    "#STAT_NAME#" => "1 - Threads_created / Connections",
                    "#GOOD_VALUE#" => "50%",
                    "#PARAM_NAME#" => "thread_cache_size",
                    "#PARAM_VALUE#" => $vars['thread_cache_size'],
                ));
            }
            $data[] = array(
                "KPI_NAME" => lang("PERFMON_KPI_NAME_THREAD_CACHE"),
                "IS_OK" => $is_ok,
                "KPI_VALUE" => $value,
                "KPI_RECOMMENDATION" => $rec,
            );

            // Table cache
            if ($stat['Open_tables'] > 0)
            {
                if ($stat['Opened_tables'] > 0)
                    $calc['table_cache_hit_rate'] = round($stat['Open_tables'] / $stat['Opened_tables'] * 100, 2);
                else
                    $calc['table_cache_hit_rate'] = 100;
                if (array_key_exists('table_cache', $vars))
                    $table_cache = 'table_cache';
                else
                    $table_cache = 'table_open_cache';
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_TABLE_CACHE"),
                    "IS_OK" => $calc['table_cache_hit_rate'] >= 20,
                    "KPI_VALUE" => $calc['table_cache_hit_rate']."%",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_TABLE_CACHE", array(
                        "#STAT_NAME#" => "Open_tables / Opened_tables",
                        "#GOOD_VALUE#" => "20%",
                        "#PARAM_NAME#" => $table_cache,
                        "#PARAM_VALUE#" => $vars[$table_cache],
                    )),
                );
            }

            // Open files
            if ($vars['open_files_limit'] > 0)
            {
                $calc['pct_files_open'] = round($stat['Open_files'] / $vars['open_files_limit'] * 100, 2);
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_OPEN_FILES"),
                    "IS_OK" => $calc['pct_files_open'] <= 85,
                    "KPI_VALUE" => $calc['pct_files_open']."%",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_OPEN_FILES", array(
                        "#STAT_NAME#" => "Open_files / open_files_limit",
                        "#GOOD_VALUE#" => "85%",
                        "#PARAM_NAME#" => "open_files_limit",
                        "#PARAM_VALUE#" => $vars['open_files_limit'],
                    )),
                );
            }

            // Table locks
            if ($stat['Table_locks_immediate'] > 0)
            {
                if ($stat['Table_locks_waited'] == 0)
                    $calc['pct_table_locks_immediate'] = 100;
                else
                    $calc['pct_table_locks_immediate'] = round($stat['Table_locks_immediate'] / ($stat['Table_locks_waited'] + $stat['Table_locks_immediate']) * 100, 2);
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_LOCKS"),
                    "KPI_VALUE" => (
                    $calc['pct_table_locks_immediate'] >= 95?
                        $calc['pct_table_locks_immediate']."%":
                        '<span class="errortext">'.$calc['pct_table_locks_immediate'].'%</span>'
                    ),
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_LOCKS", array(
                        "#STAT_NAME#" => "Table_locks_immediate / (Table_locks_waited + Table_locks_immediate)",
                        "#GOOD_VALUE#" => "95%",
                    )),
                );
            }

            // Performance options
            if ($vars['concurrent_insert'] == "OFF")
            {
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_INSERTS"),
                    "KPI_VALUE" => "OFF",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INSERTS", array(
                        "#PARAM_NAME#" => "concurrent_insert",
                        "#REC_VALUE#" => "'ON'",
                    )),
                );
            }
            elseif ($vars['concurrent_insert'] == "0")
            {
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_INSERTS"),
                    "KPI_VALUE" => "0",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INSERTS", array(
                        "#PARAM_NAME#" => "concurrent_insert",
                        "#REC_VALUE#" => "1",
                    )),
                );
            }

            // Aborted connections
            if ($stat['Connections'] > 0)
            {
                $calc['pct_aborted_connections'] = round(($stat['Aborted_connects'] / $stat['Connections']) * 100, 2);
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_CONN_ABORTS"),
                    "IS_OK" => $calc['pct_aborted_connections'] <= 5,
                    "KPI_VALUE" => $calc['pct_aborted_connections']."%",
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_CONN_ABORTS"),
                );
            }

            // InnoDB
            if ($vars['storage_engine'] == "InnoDB")
            {
                if ($stat['Innodb_buffer_pool_reads'] > 0 && $stat['Innodb_buffer_pool_read_requests'] > 0)
                {
                    $calc['innodb_buffer_hit_rate'] = round((1 - $stat['Innodb_buffer_pool_reads'] / $stat['Innodb_buffer_pool_read_requests']) * 100, 2);
                    $data[] = array(
                        "KPI_NAME" => lang("PERFMON_KPI_NAME_INNODB_BUFFER"),
                        "IS_OK" => $calc['innodb_buffer_hit_rate'] > 95,
                        "KPI_VALUE" => $calc['innodb_buffer_hit_rate']."%",
                        "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INNODB_BUFFER", array(
                            "#STAT_NAME#" => "1 - Innodb_buffer_pool_reads / Innodb_buffer_pool_read_requests>",
                            "#GOOD_VALUE#" => 95,
                            "#PARAM_NAME#" => "innodb_buffer_pool_size",
                            "#PARAM_VALUE#" => $this->format_file_size($vars['innodb_buffer_pool_size']),
                        )),
                    );
                }
                $data[] = array(
                    "KPI_NAME" => "innodb_flush_log_at_trx_commit",
                    "IS_OK" => $vars['innodb_flush_log_at_trx_commit'] == 2 || $vars['innodb_flush_log_at_trx_commit'] == 0,
                    "KPI_VALUE" => strlen($vars['innodb_flush_log_at_trx_commit'])? $vars['innodb_flush_log_at_trx_commit']: lang("PERFMON_KPI_EMPTY"),
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INNODB_FLUSH_LOG", array(
                        "#PARAM_NAME#" => "innodb_flush_log_at_trx_commit",
                        "#GOOD_VALUE#" => 2,
                    )),
                );
                $data[] = array(
                    "KPI_NAME" => "sync_binlog",
                    "IS_OK" => $vars['sync_binlog'] == 0 || $vars['sync_binlog'] >= 1000,
                    "KPI_VALUE" => intval($vars['sync_binlog']),
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_SYNC_BINLOG", array(
                        "#PARAM_NAME#" => "sync_binlog",
                        "#GOOD_VALUE_1#" => 0,
                        "#GOOD_VALUE_2#" => 1000,
                    )),
                );
                $data[] = array(
                    "KPI_NAME" => "innodb_flush_method",
                    "IS_OK" => $vars['innodb_flush_method'] == "O_DIRECT",
                    "KPI_VALUE" => strlen($vars['innodb_flush_method'])? $vars['innodb_flush_method']: lang("PERFMON_KPI_EMPTY"),
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INNODB_FLUSH_METHOD", array(
                        "#PARAM_NAME#" => "innodb_flush_method",
                        "#GOOD_VALUE#" => "O_DIRECT",
                    )),
                );
                $data[] = array(
                    "KPI_NAME" => "transaction-isolation",
                    "KPI_VALUE" => strlen($vars['tx_isolation'])? $vars['tx_isolation']: lang("PERFMON_KPI_EMPTY"),
                );
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_INNODB_LOG_WAITS"),
                    "KPI_VALUE" => $stat["Innodb_log_waits"],
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_INNODB_LOG_WAITS", array($this->format_file_size($vars["innodb_log_file_size"]))),
                );
                $data[] = array(
                    "KPI_NAME" => lang("PERFMON_KPI_NAME_BINLOG"),
                    "KPI_VALUE" => $stat["Binlog_cache_disk_use"],
                    "KPI_RECOMMENDATION" => lang("PERFMON_KPI_REC_BINLOG", array($this->format_file_size($vars["binlog_cache_size"]))),
                );
            }
        }
        $this->layout_show("admin/database.html",array('data' => $data));
    }

    function format_file_size($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}

