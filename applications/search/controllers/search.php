<?php
namespace search;

class search extends \Controller {

    function default_method()
    {
        crumbs("Глобальный поиск");
        $result = array();

        if ($_POST['search'] != "")
        {
            /** @var  $entity_cr \projects\entities */
            $entity_cr = $this->get_controller("projects","entities");
            $result = $entity_cr->get_entities_search($_SESSION['user']['id_user'],$GLOBALS['globals']['current_company'],$_POST['search']);

            /** @var  $u_cr \projects\user_tasks */
            $u_cr = $this->get_controller("projects","user_tasks");
            $u_cr->filter = $_POST;
            $u_cr->dashboard = true;
            $u_cr->global_search = true;
            $result['tasks'] = $u_cr->default_method();

            /** @var  $users_cr \users\users */
            $users_cr = $this->get_controller("users");
            $users_cr->limit = 50;
            $result['users'] = $users_cr->users_search()['users'];

            /** @var  $comments_cr \projects\comments */
            $comments_cr = $this->get_controller("projects","comments");
            $result['comments'] = $comments_cr->comments_search($_POST['search']);
        }

        $has_result = false;
        foreach ($result as $r)
        {
            if ($r || (is_array($r) && count($r))) $has_result = true;
        }

        $res['success'] = $this->layout_get("result.html",array('search' => $result,'dashboard' => true,'has_result' => $has_result));

        echo json_encode($res);

    }
}

