<?php
namespace admin\kug;

class kug extends \Admin {

    private $module_name = "КУГ";

    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            case "add":
                $this->add();
                break;
            case "delete":
                $this->delete();
                break;
            case "edit":
                $this->edit();
                break;
            default: $this->default_show();
        }
    }

    function default_show()
    {
        $data = $this->db->query("select * from kug")->fetchAll();

        if (defined('AJAX') && AJAX)
        {
            $res['success'] = $this->layout_get('admin/table.html',array('data' => $data));
            echo json_encode($res);
        }
        else $this->layout_show('admin/index.html',array('data' => $data));
    }

    function add()
    {
        $res['success'] = $this->layout_get('admin/form.html');
        echo json_encode($res);
    }

    function edit()
    {
        if ($_POST['id'] != "")
        {
            if ($element = $this->get($_POST['id']))
            {
                $res['success'] = $this->layout_get('admin/form.html',array("element" => $element,'mode' => 'edit'));
            }
        }
        else $res['error'] = "Переданые неверные данные";

        echo json_encode($res);
    }

    function save()
    {
        if ($_POST['name_category_students'] == '') $res['error'] = "Категория отдельных детей не может быть пустой";
        if ($_POST['name_group'] == '') $res['error'] = "Категория группы не может быть пустой";

        if (!$res['error'])
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            if ($element['name'] != $_POST['name'] && $_POST['id'] != "") $message = ". Название {$element['name']} изменено на \"{$_POST['name']}\"";

            $save_data = array(
                $_POST['name_category_students'],
                $_POST['name_group']
            );

            if ($_POST['id'] != "")
            {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE kug set name_category_students=?,name_group=? where id=?");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Отредактировано. \"{$element['name']}\"" . $message, $_SESSION['admin']['id_user']);
            }
            else
            {
                $query = $this->db->prepare("INSERT INTO kug(name_category_students,name_group) VALUES(?,?)");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Добавлено. \"{$_POST['name']}\"", $_SESSION['admin']['id_user']);
            }

            if (!$res['error']) $res['success'] = true;
        }

        echo json_encode($res);
    }

    function delete()
    {
        if ($_POST['id'] != "")
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            $query = $this->db->prepare("delete from kug where id=?");
            if ($query->execute(array($_POST['id'])))
            {
                $res['success'] = true;
                if ($log) $log->save_into_log("admin",$this->module_name,true,"Удалено. \"{$element['name']}\"",$_SESSION['admin']['id_user']);
            }
            else $res['error'] = "Ошибка удаления";
        }
        else $res['error'] = "Передан неверный id";

        echo json_encode($res);
    }

    function get($id)
    {
        $query = $this->db->prepare("select * from kug where id=?");
        $query->execute(array($id));
        $element = $query->fetch();

        return $element;
    }
}

