<?php
namespace ug;

class edit extends \Controller {

	function default_method()
	{
		switch ($_POST['act']) {
			case 'date_result_save':
				$this->date_result_save();
				break;
			default:
				$this->default_show();
		}
	}

	function default_show()
	{
		if (defined('AJAX') && AJAX) {
			$this->id = $_POST['id'];
		}

		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller("ug");
		if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $this->id)) {
			$this->error_page('denied');
		}

		$data['ug'] = $ug_ctr->get_ug($this->id);

		crumbs("Мои группы","/ug/start");
		crumbs("Редактирование учебной группы");

		if ($data['ug']['type'] == TYPE_START) {
			/** @var  $kug_ctr \kug\kug */
			$kug_ctr = $this->get_controller("kug");
			$data['kug'] = $kug_ctr->get_all();
            $data['type'] = TYPE_START;
            $data['type'] = "start";

			/** @var  $umk_ctr \umk\umk */
			$umk_ctr = $this->get_controller("umk");
			$data['umk'] = $umk_ctr->get_all_by_kug();
		}

		if (defined('AJAX') && AJAX) {
			$res['success'] = $this->layout_get("add.html",$data);
			echo json_encode($res);
		} else {
			$this->layout_show('add.html',$data);
		}
	}

	function date_result_save()
	{
		/** @var  $ug_ctr \ug\ug */
		$ug_ctr = $this->get_controller("ug");

		if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $_POST['id_study_group'])) {
			$res['error'] = "Нет доступа";
		}

		$ug = $ug_ctr->get_ug($_POST['id_study_group']);

		if (!$ug) {
			$res['error'] = 'Класс не найден';
		} else {
			$dates = $ug['dates'];
			$_POST['begin'] = (int) $_POST['begin'];
			$dates[$_POST['begin']] = $_POST['date'];
		}

		if (empty($res['error'])) {
			$query = $this->db->prepare("UPDATE study_groups set dates=? where id=?");
			if (!$query->execute([serialize($dates), $_POST['id_study_group']])) {
				$res['error'] = "Ошибка базы данных";
			}
		}

		if (empty($res['error'])) {
			$res['success'] = true;
		}

		echo json_encode($res);
	}
}
