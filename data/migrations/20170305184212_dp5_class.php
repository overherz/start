<?php

use Phinx\Migration\AbstractMigration;

class Dp5Class extends AbstractMigration
{
    const
        TABLE = 'dp',
        TABLE_GDP = 'gdp',
        TABLE_DC = 'dp_to_class';

    private $data = [
        'Регулятивные УУД' => [
            'Постановка учебной цели||У||4',
            'Гипотеза||з15||А,Б',
            'Планирование учебных действий||У||7',
            'Эффективный способ||У||8;10',
            'Текущий контроль||У||8',
            'Контроль результата||У||10;12',
            'Оценивание||У||13',
            'Рефлексия||У||13'
        ],
        'Познавательные УУД' => [
            'Сравнение||У||8;9',
            'Обобщение||з5||А,Б',
            'Аналогия||з14||А,Б',
            'Индукция||з4||А,Б',
            'Определение понятия||з2||А,Б',
            'Подведение под понятие||з11||А,Б',
            'Построение схемы||з9||А,Б',
            'Использование схемы||з7||А,Б',
        ],
        'Информационно-коммуникативные УУД' => [
            'Неявная инфомарция||з6||А,Б',
            'Главная мысль||з3||А,Б',
            'Объединение информации||з8||А,Б',
            'Противоречия||з10||А,Б',
            'Аргументация||з12||А,Б',
            'Передача информации||з13||А,Б',
        ]
    ];

    public function up()
    {
        $gdp_data = [
            'name' => 'Информационно-коммуникативные УУД',
            'type' => 2
        ];

        $this->insert(self::TABLE_GDP, $gdp_data);

        $i = 0;
        foreach ($this->data as $gdp => $data) {
            $id_gdp = $this->get_gdp_id($gdp);

            foreach ($data as $value) {
                $value = explode('||', $value);

                $insert_data = [
                    'id_gdp' => $id_gdp,
                    'name' => $value[0],
                    'short_name' => $value[0],
                    'position' => $i,
                ];

                $this->insert(self::TABLE, $insert_data);
                $last_id = $this->get_last_dp_id();

                $insert_data2 = [
                    'id_dp' => $last_id,
                    'class' => 5,
                    'name' => $value[1],
                    'fields' => $value[2]
                ];

                $this->insert(self::TABLE_DC, $insert_data2);

                $i++;
            }
        }
    }

    public function down()
    {

    }

    private function get_gdp_id($name)
    {
        $row = $this->fetchRow("SELECT id FROM gdp WHERE name = '{$name}'");
        return $row['id'];
    }

    private function get_last_dp_id()
    {
        $row = $this->fetchRow('SELECT id
            FROM dp
            ORDER BY id DESC
        ');

        return $row['id'];
    }
}
