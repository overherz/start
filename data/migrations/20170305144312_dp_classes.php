<?php

use Phinx\Migration\AbstractMigration;

class DpClasses extends AbstractMigration
{
    const
        TABLE_CLASSES = 'dp_to_class',
        TABLE_DP = 'dp';

    public function up()
    {
        $table = $this->table(self::TABLE_CLASSES, array('id' => false, 'primary_key' => array('id_dp', 'class')));
        $table
            ->addColumn('id_dp', 'integer')
            ->addColumn('class', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('fields', 'string')
            ->addForeignKey('id_dp', 'dp', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
            ->save();

        $rows = $this->fetchAll('SELECT d.*
            FROM dp as d
            JOIN gdp as g ON d.id_gdp=g.id
            WHERE g.type = 2
        ');

        if (!empty($rows)) {
            $insert_data = [];

            foreach ($rows as $row) {
                $classes = range($row['begin'], 4);

                foreach ($classes as $class) {
                    if (stristr($row['fields' . $class], '1,2')) {
                        $name = 'У';
                    } else {
                        $name = 'З';
                    }

                    $insert_data[] = [
                        'id_dp' => $row['id'],
                        'class' => $class,
                        'fields' => $row['fields' . $class],
                        'name' => $name,
                    ];
                }
            }

            $this->insert(self::TABLE_CLASSES, $insert_data);

            $table = $this->table(self::TABLE_DP);
            $table
                ->removeColumn('begin')
                ->removeColumn('fields1')
                ->removeColumn('fields2')
                ->removeColumn('fields3')
                ->removeColumn('fields4')
                ->removeColumn('fields5')
                ->save();
        }
    }

    public function down()
    {
        return false;
    }
}
