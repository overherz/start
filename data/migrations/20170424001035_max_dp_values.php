<?php

use Phinx\Migration\AbstractMigration;

class MaxDpValues extends AbstractMigration
{
	const
		TABLE_DP_TO_CLASS = 'dp_to_class';

	public function up()
	{
		$table = $this->table(self::TABLE_DP_TO_CLASS);
		$table->addColumn('fields_data', 'text')
			->save();

		$rows = $this->fetchAll("SELECT id_dp, class, fields FROM dp_to_class");
		foreach ($rows as $row) {
			$fields = explode(',', $row['fields']);
			$fields_data = [];

			if ($row['class'] == 1 || $row['class'] == 2) {
				foreach ($fields as $field) {
					$fields_data[$field] = 2;
				}
			} elseif ($row['class'] == 3 || $row['class'] == 4) {
				foreach ($fields as $field) {
					if ($field == '1' || $field == '2') {
						$fields_data[$field] = 3;
					} else {
						$fields_data[$field] = 2;
					}
				}
			} elseif ($row['class'] == 5) {
				foreach ($fields as $field) {
					if ($field == 'А' || $field == 'Б' || $field == '9' || $field == '11') {
						$fields_data[$field] = 1;
					} else {
						$fields_data[$field] = 2;
					}
				}
			}

			$fields_data = serialize($fields_data);
			$this->query("UPDATE dp_to_class set fields_data = '{$fields_data}' WHERE id_dp = {$row['id_dp']} AND class = {$row['class']} LIMIT 1");
		}
	}

	public function down()
	{
		$table = $this->table(self::TABLE_DP_TO_CLASS);
		$table->removeColumn('fields_data')
			->save();
	}
}
