<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class GroupsTypes extends AbstractMigration
{
    const TABLE = 'study_groups';

    public function up()
    {
        $table = $this->table(self::TABLE);
        $table->addColumn('type', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addIndex(['type'])
            ->save();

        $this->execute('UPDATE ' . self::TABLE . ' SET type = 1');
    }

    public function down()
    {
        $table = $this->table(self::TABLE);
        $table->removeColumn('type')
            ->save();
    }
}
