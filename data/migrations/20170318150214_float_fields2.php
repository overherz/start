<?php

use Phinx\Migration\AbstractMigration;

class FloatFields2 extends AbstractMigration
{
	const
		TABLE_RESULTS = 'results_uud',
		TABLE_DP_TO_CLASS = 'dp_to_class';

	public function up()
	{
		$this->query('UPDATE dp_to_class
			set float_fields = "В"
            WHERE class = 4 AND fields = "А,Б,В"
        ');
	}

	public function down()
	{

	}
}
