<?php
namespace admin\black_list;

class black_list extends \Admin {

    public $time = 300;
    public $max_count = 5;

    function default_method()
    {
        $this->layout_show('admin/index.html');
    }

    function check_ban($ip=false)
    {
        if (!$ip) $ip = $_SERVER['REMOTE_ADDR'];
        $query = $this->db->prepare("select * from black_list where ip=? LIMIT 1");
        $query->execute(array($ip));
        return $query->fetch();
    }

    function add_ban($ip=false)
    {
        $error = false;
        if (!$ip) $ip = $_SERVER['REMOTE_ADDR'];
        if (!$this->check_ban($ip))
        {
            $this->db->beginTransaction();

            $query = $this->db->prepare("insert into black_list(ip,time) values(?,?)");
            if (!$query->execute(array($ip,time()+$this->time))) $error = true;

            $query = $this->db->prepare("delete from logs where title='Авторизация' and ip=? and status='0'");
            if (!$query->execute(array($ip))) $error = true;

            if ($error) $this->db->rollBack();
            else
            {
                $this->db->commit();
                return true;
            }
        }
    }

    function delete_ban($ip=false)
    {
        if (!$ip)
        {
            $time = time();
            $query = $this->db->prepare("delete from black_list where time <= ?");
            return $query->execute(array($time));
        }
        else
        {
            $query = $this->db->prepare("delete from black_list where ip=? LIMIT 1");
            return $query->execute(array($ip));
        }
    }

    function check_for_ban()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $query = $this->db->prepare("select id from logs where title='Авторизация' and ip=? and date >= ? and status='0'");
        $query->execute(array($ip,time()-$this->time));
        $count = count($query->fetchAll());
        if ($count >= $this->max_count) return $this->add_ban($ip);
    }
}

