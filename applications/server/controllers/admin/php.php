<?php
namespace admin\server;

class php extends \Admin {

    function default_method()
    {
        ob_start();
        phpinfo();
        $info = ob_get_contents();
        ob_get_clean();

        $info = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$info);
        $info = preg_replace("/<table.*?>/","<table class='table table-bordered table-striped table-condensed'>",$info);
        $this->layout_show("admin/php.html",array('info' => $info));
    }
}

