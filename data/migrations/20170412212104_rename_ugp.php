<?php

use Phinx\Migration\AbstractMigration;

class RenameUgp extends AbstractMigration
{
    public function up()
    {
		$this->query('UPDATE gdp
			set name = "Коммуникативные УУД"
            WHERE name = "Коммуникативные"
            LIMIT 1
        ');
    }
}
