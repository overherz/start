<?php

use Phinx\Migration\AbstractMigration;

class SubgroupsClass extends AbstractMigration
{
    public function up()
    {
		$table = $this->table('dp_subgroups');
		$table
			->addColumn('class', 'integer')
			->save();

		$this->query('UPDATE dp_subgroups SET class=5');
    }

    public function down()
	{
		$table = $this->table('dp_subgroups');
		$table
			->removeColumn('class')
			->save();
	}
}
