<?php

use Phinx\Migration\AbstractMigration;

class UgDates extends AbstractMigration
{
	const TABLE = 'study_groups';

	public function up()
	{
		$table = $this->table(self::TABLE);
		$table->addColumn('dates', 'text')
			->save();
	}

	public function down()
	{
		$table = $this->table(self::TABLE);
		$table->removeColumn('dates')
			->save();
	}
}
