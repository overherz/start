<?php
namespace tasks;

require_once(__DIR__ .'../../../../index.php');

class sitemap_builder extends \Controller {

    private $id;

    function __construct($id,$key)
    {
        /** @var $task_cr \tasks\tasks */
        $task_cr = $this->get_controller("tasks");
        /** @var  $task_error_cr \tasks\error */
        $task_error_cr = $this->get_controller("tasks","error");
        if ($task = $task_cr->get_task($id))
        {
            $task_cr->get_options();
            $pid = getmypid();

            if ($key == get_setting('cron_key') && !$task_cr->is_process_running($task['pid']))
            {
                $this->id = $task['id'];
                $GLOBALS['cli_task_id'] = $this->id;
                $task_cr->set_status($id,"run",$task['period'],$pid);
                $this->get_controller("sitemap_builder")->create_sitemap();
                $task_cr->set_status($id,"stand");
            }
        }
        else
        {
            $task_error_cr->set_error("Неверный ключ",$this->id);
        }
    }
}

new sitemap_builder($argv[1],$argv[2]);
