<?php

use Phinx\Migration\AbstractMigration;

class SubgroupsParent extends AbstractMigration
{
	CONST
		TABLE_DP_SUBGROUPS = 'dp_subgroups';

	private $groups = [
		'Целеполагание' => 'Регулятивные УУД',
		'Планирование' => 'Регулятивные УУД',
		'Контроль' => 'Регулятивные УУД',
		'Оценивание и рефлексия' => 'Регулятивные УУД',
		'Мыслительные операции' => 'Познавательные УУД',
		'Умозаключения' => 'Познавательные УУД',
		'Понятия' => 'Познавательные УУД',
		'Модели и схемы' => 'Познавательные УУД',
		'Понимание информации' => 'Информационно-коммуникативные УУД',
		'Объединение информации' => 'Информационно-коммуникативные УУД',
		'Изложение информации' => 'Информационно-коммуникативные УУД'
	];

	public function up()
	{
		$table = $this->table(self::TABLE_DP_SUBGROUPS);
		$table
			->addColumn('id_parent', 'integer')
			->save();

		foreach ($this->groups as $k => $data) {
			$id_parent = $this->get_group_id($data);
			$id = $this->get_subgroup_id($k);
			$this->query("UPDATE " . self::TABLE_DP_SUBGROUPS . " set id_parent = '{$id_parent}' WHERE id = {$id} LIMIT 1");
		}

		$table
			->addForeignKey('id_parent', 'gdp', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
			->save();
	}

	public function down()
	{
		$table = $this->table(self::TABLE_DP_SUBGROUPS);
		$table->removeColumn('id_parent')
			->save();
	}

	private function get_subgroup_id($name)
	{
		$row = $this->fetchRow("SELECT id FROM " . self::TABLE_DP_SUBGROUPS . " WHERE sub_name = '{$name}'");
		return $row['id'];
	}

	private function get_group_id($name)
	{
		$row = $this->fetchRow("SELECT id FROM gdp WHERE name = '{$name}'");
		return $row['id'];
	}
}
