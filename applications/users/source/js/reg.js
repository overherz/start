$(document).ready(function($)
{
    $(".registration").on("click", function(){
        user_api($("#registration_form").serialize(),function(res){
            show_message("success","Регистрация выполнена");
            redirect('/users/registration?act=success',2,true);
        },false,'/users/registration');
        return false;
    });

    $(document).on("change","#region",function(){
        user_api({act:'get_cities_region',region:$(this).val()},function(data){
            var citiesBlock = $("#city");
            if (data.length>0){
                citiesBlock.html(data);
                var city = $($("option",citiesBlock)[1]);
                city.attr('selected', 'true');
            }
            
        },false,'/users/functions');
    });
});