<?php
require_once(ROOT.'libraries/predis-1.0/autoload.php');
Predis\Autoloader::register();

class RedisDB
{
    private static $Instance;

    public function __construct() {

    }

    public static function connect() {

        if (!self::$Instance)
        {
            try {
                self::$Instance = new Predis\Client('tcp://127.0.0.1:6379');
            }
            catch (Exception $e) {
                die($e->getMessage());
            }
        }
        return self::$Instance;
    }
}
