<?php

namespace global_module;

class get_options extends \Global_module
{
    protected $type = \Global_module::TYPE_BOTH;
    protected $on_ajax_not_run = false;

    function run_module()
    {
        if (!\Cache::connect()->get('options'))
        {
            if ($result = \MyPDO::connect()->query("select * from options"))
                while ($row = $result->fetch()) $GLOBALS['settings'][$row['key_name']] = $row;
            \Cache::connect()->set('options',$GLOBALS['settings'], 600);
        }
        else $GLOBALS['settings'] = \Cache::connect()->get('options');

        if (!\Router::admin())
        {
            if ($GLOBALS['settings']['site_close']['value'])
            {
                echo $GLOBALS['settings']['site_close_message']['value'];
                exit();
            }
        }
    }
}
