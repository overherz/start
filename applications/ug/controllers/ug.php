<?php
namespace ug;

class ug extends \Controller {

    function default_method()
    {
        crumbs("Выбор диагностического комплекта");
		$this->layout_show('index.html');
    }

	function get_all($type)
	{
		$query = $this->db->prepare("select *, (select fio from users where id_user=study_groups.owner limit 1) as owner_name
			from study_groups where type = ?");
		$query->execute([$type]);
		return $query->fetchAll();
	}

	function get_all_owner($owner, $type)
	{
		$query = $this->db->prepare("select * from study_groups where owner=? and type=?");
		$query->execute(array($owner, $type));
		return $query->fetchAll();
	}

	function check_access($user, $ug)
	{
		$query = $this->db->prepare("select * from study_groups where id=? and owner=?");
		$query->execute(array($ug, $user));
		$ug = $query->fetch();

		/** @var  $user_ctr \users\users */
		$user_ctr = $this->get_controller("users");
		$user = $user_ctr->get_user($user);

		if ($ug || $user['id_group'] == "1") return true;
	}

	function get_ug($id)
	{
		$query = $this->db->prepare("select ug.*,u.fio as owner_fio
            from study_groups as ug
            left join users as u ON ug.owner=u.id_user
            where id=?
        ");
		$query->execute(array($id));
		if ($ug = $query->fetch())
		{
			/** @var  $umk_ctr \umk\umk */
			$umk_ctr = $this->get_controller("umk");
			$ug['umk'] = $umk_ctr->get_umk_from_ug($ug['id']);
			$ug['umk_name'] = $umk_ctr->get_umk_from_ug_name($ug['id']);

			if ($ug['type'] == TYPE_UUD) {
				$ug['dates'] = unserialize($ug['dates']);
			}
		}

		$this->set_global('type', $ug['type']);

		return $ug;
	}
}

