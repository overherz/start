$(document).ready(function($)
{
    $(document).on("click","#save_profile",function(){
        var request = $("#edit_profile").serialize();
        user_api(request,function(data){
            show_message("success","Профиль сохранен");
            redirect(false,1);
        },false,'/users/edit');
        return false;
    });

    $(document).on("click","#change_password",function(){
        var request = $("#change_password_form").serialize();
        user_api(request,function(data){
            show_message("success", "Пароль изменен");
            redirect(false,1);
        },false,'/users/edit');
        return false;
    });

    $(document).on("change","#region",function(){
        user_api({act:'get_cities_region',region:$(this).val()},function(data){
            $("#city").html(data);
        },false,'/users/functions');
    });

    $(document).on("click","#btn_change_pass",function(){
        $("#change_password_form").show();
        return false;
    });

    $(document).on("click","#btn_change_pass_cancel",function(){
        $("#change_password_form").hide();
        return false;
    });
});