<?php
namespace results;

class report extends \Controller
{
    function default_method()
    {
        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller("ug");
        if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $this->_0)) {
            $this->error_page('denied');
        }

        $data['hide_sidebar'] = true;
        $data['ug'] = $ug_ctr->get_ug($this->_0);

        crumbs("Главная","/");

        if ($data['ug']['type'] == TYPE_START) {
            crumbs("Школьный старт", '/ug/start');
            crumbs($data['ug']['name'], "/ug/start_show/{$data['ug']['id']}");
        } elseif ($data['ug']['type'] == TYPE_UUD) {
            crumbs("Учимся Учиться и Действовать","/ug/uud");
			$begin = (int) $_GET['begin'];
			$data['begin'] = $begin;
            $data['labels'] = $this->getLabelsByClass($data['begin']);
            crumbs($data['ug']['name'] . ". " . $begin . " класс", "/ug/uud_show/{$data['ug']['id']}?begin=" . $begin);
        }

        switch ($this->id) {
            case "general":
                if ($data['ug']['type'] == TYPE_START) {
                    $this->report_general_start($data);
                } elseif ($data['ug']['type'] == TYPE_UUD) {
                    $this->report_general_uud($data);
                }
                break;
            case "levels":
                $this->report_levels_start($data);
                break;
            case "groups":
                if ($data['ug']['type'] == TYPE_START) {
                    $this->report_groups_start($data);
                } elseif ($data['ug']['type'] == TYPE_UUD) {
                    $this->report_groups_uud($data);
                }
                break;
            case "individual":
                if ($data['ug']['type'] == TYPE_START) {
                    $this->report_individual_start($data);
                } elseif ($data['ug']['type'] == TYPE_UUD) {
                    $this->report_individual_uud($data);
                }
                break;
			case 'progress':
				$this->progress($data);
				break;
			case 'reflex':
				$this->reflex($data);
				break;
			case 'import_download':
				$this->import_download($data);
				break;
        }
    }

    /***
     * @return array $labels;
     * @param int $begin;
     * @param string $fio;
     */
    function getLabelsByClass($begin)
	{
        $labels = array();
        $labels[1]['general']['name'] = "Общие результаты";
        $labels[1]['general']['tables'][0] = "Индивидуальные оценки";
        $labels[1]['groups']['name'] = "Распределение по группам";
        $labels[1]['groups']['tables'][0] = "Распределение по группам";
        $labels[1]['rating']['name'] = "Рейтинг УУД";
        $labels[1]['rating']['tables'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[1]['rating']['tables'][1] = "Рейтинг УУД по отдельным умениям в $begin&nbsp;классе";
        $labels[1]['rating']['diagrams'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[1]['rating']['diagrams'][1] = "Рейтинг УУД по отдельным умениям в $begin&nbsp;классе";
        $labels[1]['individual']['name'] = "Пофамильные карты";
        $labels[1]['individual']['tables'][0] = "Индивидуальные результаты мониторинга метапредметных УУД. $begin&nbsp;класс";
        $labels[1]['individual']['tables'][1] = "Сводные показатели";
        $labels[1]['individual']['diagrams'][0] = "Индивидуальные результаты мониторинга метапредметных УУД. $begin&nbsp;класс";


        $labels[2]['general']['name'] = "Общие результаты";
        $labels[2]['general']['tables'][0] = "Индивидуальные оценки";
        $labels[2]['rating']['name'] = "Рейтинг УУД";
        $labels[2]['rating']['tables'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[2]['rating']['tables'][1] = "Рейтинг УУД по отдельным умениям в $begin&nbsp;классе";
        $labels[2]['rating']['diagrams'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[2]['rating']['diagrams'][1] = "Рейтинг УУД по отдельным умениям в $begin&nbsp;классе";
        $labels[2]['progress']['name'] = "Прогресс";
        $labels[2]['progress']['tables'][0] = "Прогресс в развитии УУД у каждого учащегося";
        $labels[2]['progress']['tables'][1] = "Обобщенный прогресс в развитии УУД в целом по классу";
        $labels[2]['progress']['tables'][2] = "Прогресс в развитии УУД по отдельным умениям в целом по классу";
        $labels[2]['progress']['diagrams'][0] = "Обобщенный прогресс в развитии УУД в целом по классу";
        $labels[2]['progress']['diagrams'][1] = "Прогресс в развитии УУД по отдельным умениям в целом по классу";
        $labels[2]['individual']['name'] = "Пофамильные карты";
        $labels[2]['individual']['tables'][0] = "Индивидуальные результаты мониторинга метапредметных УУД. $begin&nbsp;класс";
        $labels[2]['individual']['tables'][1] = "Сводные показатели";
        $labels[2]['individual']['diagrams'][0] = "Индивидуальные результаты мониторинга метапредметных УУД";


        $labels[3]['general']['name'] = "Общие результаты";
        $labels[3]['general']['tables'][0] = "Индивидуальные оценки";
        $labels[3]['rating']['name'] = "Рейтинг УУД";
        $labels[3]['rating']['tables'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[3]['rating']['tables'][1] = "Детализированный рейтинг развития УУД в $begin&nbsp;классе";
        $labels[3]['rating']['diagrams'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[3]['rating']['diagrams'][1] = "Детализированный рейтинг УУД в $begin&nbsp;классе";
        $labels[3]['progress']['name'] = "Прогресс";
        $labels[3]['progress']['tables'][0] = "Прогресс в развитии УУД у каждого учащегося";
        $labels[3]['progress']['tables'][1] = "Обобщенный прогресс в развитии УУД в целом по классу";
        $labels[3]['progress']['tables'][2] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[3]['progress']['diagrams'][0] = "Обобщенный прогресс в развитии УУД в целом по $begin&nbsp;классу";
        $labels[3]['progress']['diagrams'][1] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[3]['individual']['name'] = "Пофамильные карты";
        $labels[3]['individual']['tables'][0] = "Индивидуальные результаты мониторинга УУД &laquo;Учимся учиться и действовать&raquo;. $begin&nbsp;класс";
        $labels[3]['individual']['tables'][1] = "Сводные показатели";
        $labels[3]['individual']['diagrams'][0] = "Индивидуальные результаты мониторинга УУД";


        $labels[4]['general']['name'] = "Общие результаты";
        $labels[4]['general']['tables'][0] = "Результаты в уровнях (базовый, ниже базового)";
        $labels[4]['rating']['name'] = "Рейтинг УУД";
        $labels[4]['rating']['tables'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[4]['rating']['tables'][1] = "Детализированный рейтинг развития УУД в $begin&nbsp;классе";
        $labels[4]['rating']['diagrams'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[4]['rating']['diagrams'][1] = "Детализированный рейтинг УУД в $begin&nbsp;классе";
        $labels[4]['progress']['name'] = "Прогресс";
        $labels[4]['progress']['tables'][0] = "Прогресс в развитии УУД у каждого учащегося";
        $labels[4]['progress']['tables'][1] = "Обобщенный прогресс в развитии УУД в целом по классу";
        $labels[4]['progress']['tables'][2] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[4]['progress']['diagrams'][0] = "Обобщенный прогресс в развитии УУД в целом по $begin&nbsp;классу";
        $labels[4]['progress']['diagrams'][1] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[4]['individual']['name'] = "Пофамильные карты";
        $labels[4]['individual']['tables'][0] = "Индивидуальные результаты мониторинга УУД &laquo;Учимся учиться и&nbsp;действовать&raquo;. $begin&nbsp;класс";
        $labels[4]['individual']['tables'][1] = "Сводные показатели";
        $labels[4]['individual']['diagrams'][0] = "Индивидуальные результаты мониторинга УУД";

        $labels[5]['general']['name'] = "Общие результаты";
        $labels[5]['general']['tables'][0] = "Общий отчет";
        $labels[5]['rating']['name'] = "Рейтинг УУД";
        $labels[5]['rating']['tables'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[5]['rating']['tables'][1] = "Детализированный рейтинг развития УУД в $begin&nbsp;классе";
        $labels[5]['rating']['diagrams'][0] = "Обобщенный рейтинг УУД в $begin&nbsp;классе";
        $labels[5]['rating']['diagrams'][1] = "Детализированный рейтинг УУД в $begin&nbsp;классе";
        $labels[5]['progress']['name'] = "Прогресс";
        $labels[5]['progress']['tables'][0] = "Прогресс в развитии УУД у каждого учащегося";
        $labels[5]['progress']['tables'][1] = "Обобщенный прогресс в развитии УУД в целом по классу";
        $labels[5]['progress']['tables'][2] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[5]['progress']['diagrams'][0] = "Обобщенный прогресс в развитии УУД в целом по $begin&nbsp;классу";
        $labels[5]['progress']['diagrams'][1] = "Прогресс в развитии УУД по отдельным умениям в целом по $begin&nbsp;классу";
        $labels[5]['individual']['name'] = "Пофамильные карты";
        $labels[5]['individual']['tables'][0] = "Индивидуальные результаты мониторинга УУД &laquo;Учимся учиться и&nbsp;действовать&raquo;. $begin&nbsp;класс";
        $labels[5]['individual']['tables'][1] = "Сводные показатели";
        $labels[5]['individual']['diagrams'][0] = "Индивидуальные результаты мониторинга УУД";
        $labels[5]['reflex']['name'] = "Индекс рефлексивности";
        $labels[5]['reflex']['tables'][0] = "Индекс рефлексивности";
        $labels[5]['reflex']['diagrams'][0] = "Индекс рефлексивности в $begin классе";

        return $labels[$begin];
    }

    function report_general_start($data = [])
    {
        crumbs("Общие результаты");

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0);

        $query = $this->db->prepare("SELECT dp.*,gdp.name AS gdp_name
                FROM dp AS dp
                JOIN gdp AS gdp ON dp.id_gdp=gdp.id AND type=?
                ORDER BY position ASC
            ");
        $query->execute([TYPE_START]);
        while ($row = $query->fetch()) {
            $data['gdp'][$row['id_gdp']] = $row['gdp_name'];
            $data['dp'][$row['id_gdp']][] = $row;
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results'] = $results_ctr->get_ug_result($this->_0)) {
            $data['total'] = array();
            foreach ($data['results'] as $result) {
                foreach ($result as $k => $r) {
                    $data['total'][$k] += $r;
                }
            }
        }

        $data['id'] = $this->_1;
        $data['show_summary'] = true;
        if ($_GET['summary'] == "hide") $data['show_summary'] = false;

        if ($_GET['mode'] == "excel") {
            /** @var  $excel \results\excel */
            $excel = $this->get_controller('results', 'excel');
            $excel->report_general_start($data);
        } else {
            $this->layout_show("start/general.html", $data);
        }
    }

    function report_general_uud($data = [])
    {
        crumbs("Отчет «Общие результаты»");

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0, $data['begin']);

        /** @var  $dp_ctr \dp\dp */
        $dp_ctr = $this->get_controller('dp');
        $data += $dp_ctr->get_all_uud_dp(true);

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($this->_0, true);
		$dp_ctr->filter_uud_data($data);

		$temp_dp = [];
		foreach ($data['dp'] as $t_dp) {
			foreach ($t_dp as $tt_dp) {
				$temp_dp[$tt_dp['id']] = $tt_dp['id_gdp'];
			}
		}

        if (!empty($data['results'])) {
            $data['total'] = array();
            foreach ($data['results'] as $id_student => $dp) {
				foreach ($dp as $id_dp => $fields) {
					if (isset($fields['total'])) {
						$data['total'][$id_dp] += $fields['total'];
						$data['gdp_total'][$id_student][$temp_dp[$id_dp]] += $fields['total'];
					}
				}
            }
        }
        unset($temp_dp);

        $data['id'] = $this->_1;
        $data['show_summary'] = true;
        if ($_GET['summary'] == "hide") {
            $data['show_summary'] = false;
        }

        $data['show_stats'] = true;
        if ($_GET['stats'] == "hide") {
            $data['show_stats'] = false;
        }

		$data['colors'] = [
			'#ffc000',
			'#993366',
			'#00b050',
		];

		$data['classes'] = [
			'uud-reg-bg',
			'uud-poznavat-bg',
			'uud-communicate-bg',
		];

		if ($_GET['mode'] == "excel") {
			$data['html'] = $this->layout_get("uud/report_uud1.html", $data);
			$data['html'] .= $this->layout_get("uud/report_rating_uud.html", $data);

			/** @var  $excel \results\excel */
			$excel = $this->get_controller('results', 'excel');
			$excel->report_general_uud($data);
		} else {
			$this->layout_show("uud/general_uud.html", $data);
		}
    }

    function report_levels_start($data = [])
    {
        crumbs("Уровни инструментальной и личностной готовности");

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0);

        $query = $this->db->prepare("SELECT * FROM gdp where type=?");
        $query->execute([TYPE_START]);
        while ($row = $query->fetch()) {
            $data['gdp'][$row['id']] = array('name' => $row['name'], 'id' => $row['id']);
        }

        if (count($data['gdp']) > 0) {
            $query = $this->db->query("SELECT * FROM levels_gdp");
            while ($row = $query->fetch()) {
                $data['gdp'][$row['id_gdp']]['levels'][] = $row;
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results'] = $results_ctr->get_ug_result($this->_0, true)) {
            $data['total'] = array();
            foreach ($data['results'] as $k => $result) {
                foreach ($result as $rk => $r) {
                    foreach ($r as $v) {
                        $data['total'][$k][$rk] += $v;
                    }
                }
            }
        }

        foreach ($data['students'] as &$s) {
            foreach ($data['gdp'] as $gdp) {
                foreach ($gdp['levels'] as $level) {
                    $value = $data['total'][$s['id']][$gdp['id']];
                    if ($value >= $level['min_value'] && $value <= $level['max_value']) {
                        $s[$gdp['id']]['level_gdp'] = $level;
                        $s['levels'][] = $level['id'];
                    }
                }
            }

            $s['group'] = $this->detect_group($s['levels']);
        }

        $data['id'] = $this->_1;

        if ($_GET['mode'] == "excel") {
            /** @var  $excel \results\excel */
            $excel = $this->get_controller('results', 'excel');
            $excel->report_levels_start($data);
        } else {
            $this->layout_show("start/levels.html", $data);
        }
    }

    function report_groups_start($data = [])
    {
        crumbs("Группы стартовой готовности");

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0);

        $query = $this->db->prepare("SELECT * FROM gdp where type=?");
        $query->execute([TYPE_START]);
        while ($row = $query->fetch()) {
            $data['gdp'][$row['id']] = array('name' => $row['name'], 'id' => $row['id']);
        }

        if (count($data['gdp']) > 0) {
            $query = $this->db->query("SELECT * FROM levels_gdp");
            while ($row = $query->fetch()) {
                $data['gdp'][$row['id_gdp']]['levels'][] = $row;
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results'] = $results_ctr->get_ug_result($this->_0, true)) {
            $data['total'] = array();
            foreach ($data['results'] as $k => $result) {
                foreach ($result as $rk => $r) {
                    foreach ($r as $v) {
                        $data['total'][$k][$rk] += $v;
                    }
                }
            }
        }

        $data['groups'] = array(1, 2, 3, 4);

        foreach ($data['students'] as &$s) {
            foreach ($data['gdp'] as $gdp) {
                foreach ($gdp['levels'] as $level) {
                    $value = $data['total'][$s['id']][$gdp['id']];
                    if ($value >= $level['min_value'] && $value <= $level['max_value']) {
                        $s[$gdp['id']]['level_gdp'] = $level;
                        $s['levels'][] = $level['id'];
                    }
                }
            }

            $data['group_students'][$this->detect_group($s['levels'])][] = $s['fio'];
        }

        if ($_GET['mode'] == "excel") {
            /** @var  $excel \results\excel */
            $excel = $this->get_controller('results', 'excel');
            $excel->report_groups_start($data);
        } else {
            $this->layout_show("start/groups.html", $data);
        }
    }

    function report_groups_uud($data = [])
    {
        crumbs("Отчёт «Распределение по группам»");
        $data['begin'] = (int) $_GET['begin'];

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0, $data['begin']);

        /** @var  $dp_ctr \dp\dp */
        $dp_ctr = $this->get_controller('dp');
        $data += $dp_ctr->get_all_uud_dp();

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        list($data['dp_groups'], $data['dp_groups_total']) = $results_ctr->get_dp_groups($this->_0, $data['begin']);

        if ($_GET['mode'] == "excel") {
			$data['html'] = $this->layout_get("uud/report_group_uud.html", $data);

			/** @var  $excel \results\excel */
			$excel = $this->get_controller('results', 'excel');
			$data['filename'] = 'Распределение по группам ' . $data['begin'] . ' класс';
			$excel->report_any_table($data);
        }
        else {
            $this->layout_show("uud/groups_uud.html", $data);
        }
    }

    function detect_group($levels)
    {
        $group = null;

        if (in_array(1, $levels) && in_array(3, $levels)) $group = 1;
        else if (in_array(1, $levels) && in_array(4, $levels)) $group = 2;
        else if (in_array(2, $levels) && in_array(3, $levels)) $group = 3;
        else if (in_array(2, $levels) && in_array(4, $levels)) $group = 4;

        return $group;
    }

    function report_individual_start($data = [])
    {
        crumbs("Индивидуальные результаты");

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->_0);

        $query = $this->db->prepare("SELECT dp.*,gdp.name AS gdp_name
                FROM dp AS dp
                LEFT JOIN gdp AS gdp ON dp.id_gdp=gdp.id
                WHERE gdp.type=?
                ORDER BY position ASC
            ");

        $query->execute(array(TYPE_START));
        while ($row = $query->fetch()) {
            $data['gdp'][$row['id_gdp']] = array('name' => $row['gdp_name'], 'id' => $row['id_gdp']);
            $data['dp'][$row['id_gdp']][] = $row;
        }

        if (count($data['gdp']) > 0) {
            $query = $this->db->query("SELECT * FROM levels_gdp");
            while ($row = $query->fetch()) {
                $data['gdp'][$row['id_gdp']]['levels'][] = $row;
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results'] = $results_ctr->get_ug_result($this->_0)) {
            $data['total'] = array();
            foreach ($data['results'] as $result) {
                foreach ($result as $k => $r) {
                    $data['total'][$k] += $r;
                }
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results1'] = $results_ctr->get_ug_result($this->_0, true)) {
            $data['total1'] = array();
            foreach ($data['results1'] as $k => $result) {
                foreach ($result as $rk => $r) {
                    foreach ($r as $v) {
                        $data['total1'][$k][$rk] += $v;
                    }
                }
            }
        }

        foreach ($data['students'] as &$s) {
            foreach ($data['gdp'] as $gdp) {
                foreach ($gdp['levels'] as $level) {
                    $value = $data['total1'][$s['id']][$gdp['id']];
                    if ($value >= $level['min_value'] && $value <= $level['max_value']) {
                        $s[$gdp['id']]['level_gdp'] = $level;
                        $s['levels'][] = $level['id'];
                    }
                }
            }
            $s['group'] = $this->detect_group($s['levels']);
        }

        $data['id'] = $this->_1;

        if ($_GET['mode'] == "excel") {
            /** @var  $excel \results\excel */
            $excel = $this->get_controller('results', 'excel');
            $excel->report_individual_start($data);
        } else {
            $this->layout_show("start/individual.html", $data);
        }
    }

    function report_individual_uud($data = [])
    {
        crumbs('Индивидуальные результаты мониторинга метапредметных УУД');
        $data['begin'] = (int) $_GET['begin'];

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
		$data['students'] = $students_ctr->get_students_from_ug($this->_0, $data['begin']);
		$data['student'] = $data['students'][$this->_1];

        /** @var  $dp_ctr \dp\dp */
        $dp_ctr = $this->get_controller('dp');
        $data += $dp_ctr->get_all_uud_dp();

        $data['max'] = $dp_ctr->get_uud_dp_max_sum($data['begin']);

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        list($data['dp_groups'], $data['dp_groups_total']) = $results_ctr->get_dp_groups($data['student']['id_study_group'], false, $this->_1);
        list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($this->_0);

		$this->uud_set_progress($data);
		$this->set_reflex($data);

		if ($data['begin'] == 5) {
			$temp_dp = [];
			foreach ($data['dp'] as $t_dp) {
				foreach ($t_dp as $tt_dp) {
					$temp_dp[$tt_dp['id']] = $tt_dp['id_gdp'];
				}
			}

			if (!empty($data['results'])) {
				foreach ($data['results'] as $id_student => $classes) {
					foreach ($classes as $number_class => $dp) {
						if ($number_class == $data['begin']) {
							foreach ($dp as $id_dp => $fields) {
								if (!empty($fields['total'])) {
									$data['gdp_total'][$id_student][$temp_dp[$id_dp]] += $fields['total'];
								}
							}
						}
					}
				}
			}
			unset($temp_dp);
		}

		if ($_GET['mode'] == 'excel') {
			$data['html'] = $this->layout_get("uud/report_individual_uud.html", $data);

			/** @var  $excel \results\excel */
			$excel = $this->get_controller('results', 'excel');
			$data['filename'] = 'Индивидуальные результаты мониторинга метапредметных УУД ' . $data['begin'] . ' класс';
			$data['vertical'] = [
				'col' => 'A',
				'row' => 3
			];
			$excel->report_any_table($data);
		} else {
			$this->layout_show('uud/individual_uud.html', $data);
		}
    }

    function uud_set_progress(&$data)
	{
		$dp = [];
		$query = $this->db->prepare("SELECT dp.id, dp.id_gdp, dc.class
                FROM dp AS dp
                JOIN gdp AS gdp ON dp.id_gdp=gdp.id AND type=?
                JOIN dp_to_class as dc ON dp.id=dc.id_dp
            ");
		$query->execute([TYPE_UUD]);

		while ($row = $query->fetch()) {
			$dp[$row['id']]['id_gdp'] = $row['id_gdp'];
			$dp[$row['id']]['classes'][] = $row['class'];
		}

		$progresses = [];
		$data['progress'] = [];
		$data['dp_progress'] = [];

		$begin = $data['begin'];
		if ($begin < 5) {
			foreach ($data['dp_groups'] as $id_student => $students) {
				foreach ($students as $number_class => $dps) {
					if ($number_class == $begin) {
						foreach ($dps as $id_dp => $group) {
							if ($id_dp != 'total') {
								if (in_array($begin - 1, $dp[$id_dp]['classes'])) {

									$dp_progress = &$data['dp_progress'][$id_student][$id_dp];
									$gdp_progress = &$data['progress'][$id_student][$dp[$id_dp]['id_gdp']];

									if ($begin == 2) {
										$progress = &$progresses[$id_student][$dp[$id_dp]['id_gdp']];
										$progress_total = &$progresses[$id_student]['total'];
										if ($group == 4) {
											$progress[0]++;
											$progress_total[0]++;
											$dp_progress = 0;
										} elseif ($group == 1) {
											$progress[1]++;
											$progress_total[1]++;
											$dp_progress = 1;
										} elseif ($group - $data['dp_groups'][$id_student][$number_class-1][$id_dp] < 0) {
											$progress[1]++;
											$progress_total[1]++;
											$dp_progress = 1;
										} else {
											$progress[0]++;
											$progress_total[0]++;
											$dp_progress = 0;
										}

										if ($progress[1] == $progress[0] && $progress[1] == 1) {
											$gdp_progress = 1;
										} else if ($progress[1] > $progress[0]) {
											$gdp_progress = 1;
										} else {
											$gdp_progress = 0;
										}

										if ($progress_total[1] > $progress_total[0]) {
											$data['progress'][$id_student]['total'] = 1;
										} else {
											$data['progress'][$id_student]['total'] = 0;
										}
									} elseif ($begin == 3) {
										if (isset($data['dp_groups'][$id_student][$number_class-1][$id_dp])) {
											if ($data['levels'][$id_student][$number_class][$dp[$id_dp]['id_gdp']][$id_dp] == 0) {
												$data['dp_progress'][$id_student][$id_dp] = $data['dp_groups'][$id_student][$number_class-1][$id_dp] == 1 ? 'C' : 'D';
											} else {
												if ($data['dp_groups'][$id_student][$number_class-1][$id_dp] == 1) {
													$dp_progress = 'A';
												} elseif ($data['dp_groups'][$id_student][$number_class-1][$id_dp] == 4) {
													$dp_progress = 'C';
												} else {
													$dp_progress = 'B';
												}
											}
										}
									} elseif ($begin == 4) {
										$level = $data['levels'][$id_student][$number_class][$dp[$id_dp]['id_gdp']][$id_dp];
										$prev_level = $data['levels'][$id_student][$number_class - 1][$dp[$id_dp]['id_gdp']][$id_dp];
										$gdp_level = $data['levels'][$id_student][$number_class][$dp[$id_dp]['id_gdp']]['total'];
										$gdp_prev_level = $data['levels'][$id_student][$number_class - 1][$dp[$id_dp]['id_gdp']]['total'];
										$total_level = $data['levels'][$id_student][$number_class]['total'];
										$total_prev_level = $data['levels'][$id_student][$number_class - 1]['total'];

										if (isset($prev_level)) {
											if ($level == 0) {
												$dp_progress = $prev_level == 0 ? 'D' : 'C';
											} else {
												$dp_progress = $prev_level == 1 ? 'A' : 'B';
											}

											if ($gdp_level == 0) {
												$gdp_progress = $gdp_prev_level == 0 ? 'D' : 'C';
											} else {
												$gdp_progress = $gdp_prev_level == 1 ? 'A' : 'B';
											}

											if ($total_level == 0) {
												$data['progress'][$id_student]['total'] = $total_prev_level == 0 ? 'D' : 'C';
											} else {
												$data['progress'][$id_student]['total'] = $total_prev_level == 1 ? 'A' : 'B';
											}
										}
									}
								}
							}
							unset($progress);
							unset($progress_total);
							unset($dp_progress);
							unset($gdp_progress);
						}
					}
				}
			}
		} elseif ($begin == 5) {
			// Соотнесение прогрессов для 5 класса
			$progress_transform5 = [
				// есть прогресс
				'нс' => 1,
				'сс' => 1,
				'св' => 1,
				'вв' => 1,
				// резкий прогресс
				'нв' => '1*',
				// нет прогресса
				'сн' => 0,
				'нн' => 0,
				'вс' => 0,
				// резкий регресс
				'вн' => '0*',
			];

			$progress_to_name_transform = [
				0 => 'н',
				1 => 'н',
				2 => 'с',
				3 => 'с',
				4 => 'с',
				5 => 'в',
				6 => 'в'
			];

			$progress_to_name_transform_new = [
				0 => 'н',
				1 => 'н',
				2 => 'с',
				3 => 'в',
				4 => 'в',
			];

			$subgroup_gdp = [];
			foreach ($data['subgroups'][$data['begin']] as $k => $v) {
				foreach ($v as $kk => $vv) {
					$subgroup_gdp[$kk] = $k;
				}
			}

			$old_dp = [];
			$new_dp = [];
			$old_dp_subgroups = [];
			$new_dp_subgroups = [];
			$old_gdp_dp_subgroups = [];
			$new_gdp_dp_subgroups = [];

			$query = $this->db->prepare('SELECT d.*, dg.id_subgroup
				FROM dp_to_dp as d 
				JOIN dp_to_subgroup as dg ON d.dp_to = dg.id_dp
				WHERE d.class_from = 4 and d.class_to = 5
			');
			$query->execute();

			while ($row = $query->fetch()) {
				$data['dp_to_subgroups'][$row['dp_to']] = $row['id_subgroup'];
				$new_dp_subgroups[$row['id_subgroup']][] = $row['dp_to'];
				$new_dp_subgroups[$row['id_subgroup']] = array_unique($new_dp_subgroups[$row['id_subgroup']]);
				$new_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]][] = $row['dp_to'];
				$new_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]] = array_unique($new_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]]);
				$new_dp[] = $row['dp_to'];

				$old_dp_subgroups[$row['id_subgroup']][] = $row['dp_from'];
				$old_dp_subgroups[$row['id_subgroup']] = array_unique($old_dp_subgroups[$row['id_subgroup']]);
				$old_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]][] = $row['dp_from'];
				$old_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]] = array_unique($old_gdp_dp_subgroups[$subgroup_gdp[$row['id_subgroup']]]);
				$old_dp[] = $row['dp_from'];
			}

			$old_dp = array_unique($old_dp);
			$new_dp = array_unique($new_dp);

			$subgroup_old_sum = [];
			$subgroup_old_gdp_sum = [];
			$subgroup_old_total_sum = [];
			foreach ($old_dp_subgroups as $id_subgroup => $old) {
				foreach ($old as $old_id) {
					foreach ($data['results'] as $id_student => $classes) {
						if (isset($classes[$begin-1][$old_id]['total'])) {
							$subgroup_old_sum[$id_student][$id_subgroup] += $classes[$begin-1][$old_id]['total'];
							$subgroup_old_gdp_sum[$id_student][$subgroup_gdp[$id_subgroup]] += $classes[$begin-1][$old_id]['total'];
							$subgroup_old_total_sum[$id_student] += $classes[$begin-1][$old_id]['total'];
						}
					}
				}
			}

			if (!empty($subgroup_old_sum)) {
				foreach ($subgroup_old_sum as $id_student => &$d) {
					foreach ($d as $id_subgroup => &$sub) {
						$value = (int) floor($sub / count($old_dp_subgroups[$id_subgroup]));
						$sub = $progress_to_name_transform[$value];
					}
				}
				unset($d);
				unset($sub);
			}

			if (!empty($subgroup_old_gdp_sum)) {
				foreach ($subgroup_old_gdp_sum as $id_student => &$d) {
					foreach ($d as $id_gdp => &$sub) {
						$value = (int) floor($sub / count($old_gdp_dp_subgroups[$id_gdp]));
						$sub = $progress_to_name_transform[$value];
					}
				}
				unset($d);
				unset($sub);
			}

			if (!empty($subgroup_old_total_sum)) {
				foreach ($subgroup_old_total_sum as $id_student => &$sub) {
					$value = (int) floor($sub / count($old_dp));
					$sub = $progress_to_name_transform[$value];
				}
				unset($sub);
			}

			$subgroup_new_sum = [];
			$subgroup_new_gdp_sum = [];
			$subgroup_new_total_sum = [];
			foreach ($new_dp_subgroups as $id_subgroup => $new) {
				foreach ($new as $new_id) {
					foreach ($data['results'] as $id_student => $classes) {
						if (isset($classes[$begin][$new_id]['total'])) {
							$subgroup_new_sum[$id_student][$id_subgroup] += $classes[$begin][$new_id]['total'];
							$subgroup_new_gdp_sum[$id_student][$subgroup_gdp[$id_subgroup]] += $classes[$begin][$new_id]['total'];
							$subgroup_new_total_sum[$id_student] += $classes[$begin][$new_id]['total'];
						}
					}
				}
			}

			if (!empty($subgroup_new_sum)) {
				foreach ($subgroup_new_sum as $id_student => &$d) {
					foreach ($d as $id_subgroup => &$sub) {
						$sub = $progress_to_name_transform_new[$sub];
					}
				}
				unset($d);
				unset($sub);
			}

			if (!empty($subgroup_new_gdp_sum)) {
				foreach ($subgroup_new_gdp_sum as $id_student => &$d) {
					foreach ($d as $id_gdp => &$sub) {
						$value = (int) floor($sub / count($data['subgroups'][$data['begin']][$id_gdp]));
						$sub = $progress_to_name_transform_new[$value];
					}
				}
				unset($d);
				unset($sub);
			}

			if (!empty($subgroup_new_total_sum)) {
				foreach ($subgroup_new_total_sum as $id_student => &$sub) {
					$value = (int) floor($sub / count($subgroup_gdp));
					$sub = $progress_to_name_transform_new[$value];
				}
				unset($sub);
			}

			$data['subgroups_progress'] = [];
			foreach ($subgroup_old_sum as $id_student => $d) {
				foreach ($d as $id_subgroup => $sub) {
					$data['subgroups_progress'][$id_student][$id_subgroup] = $progress_transform5[$sub.$subgroup_new_sum[$id_student][$id_subgroup]];
				}
			}

			foreach ($subgroup_old_gdp_sum as $id_student => $d) {
				foreach ($d as $id_gdp => $sub) {
					$data['progress'][$id_student][$id_gdp] = $progress_transform5[$sub.$subgroup_new_gdp_sum[$id_student][$id_gdp]];
				}
			}

			foreach ($subgroup_old_total_sum as $id_student => $sub) {
				$data['progress'][$id_student]['total'] = $progress_transform5[$sub.$subgroup_new_total_sum[$id_student]];
			}

			$gdp_keys = array_keys($data['subgroups'][$data['begin']]);
			foreach ($data['gdp'] as $k => $v) {
				if (!in_array($k, $gdp_keys)) {
					unset($data['gdp'][$k]);
				}
			}
		}
	}

	function progress($data = [])
	{
		/** @var  $students_ctr \students\students */
		$students_ctr = $this->get_controller("students");
		$data['students'] = $students_ctr->get_students_from_ug($this->_0, $data['begin']);

		crumbs('Прогресс');

		/** @var  $dp_ctr \dp\dp */
		$dp_ctr = $this->get_controller('dp');
		$data += $dp_ctr->get_all_uud_dp();

		/** @var  $results_ctr \results\results */
		$results_ctr = $this->get_controller("results");
		list($data['dp_groups'], $data['dp_groups_total']) = $results_ctr->get_dp_groups($this->_0);
		list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($this->_0);

		$this->uud_set_progress($data);

		$progress_sum = [];
		$results_sum = [];
		$data['percent_progress_total'] = [];

		if ($data['begin'] < 5) {
			$data['progress_levels3'] = ['A', 'B', 'C', 'D'];
			$data['progress_levels3_colors'] = ['4f81bd','c0504d','9bbb59','8064a2'];

			// подсчитаем сумму прогрессов по всем ученикам
			foreach ($data['dp_progress'] as $student) {
				foreach ($student as $id_dp => $progress) {
					if (!is_null($progress)) {
						if ($data['begin'] == 2) {
							$progress_sum[$id_dp] += $progress;
						} elseif ($data['begin'] == 3 || $data['begin'] == 4) {
							$progress_sum[$id_dp][$progress]++;
						}

						$results_sum[$id_dp]++;
					}
				}
			}

			// разделим сумму прогрессов на количество учеников, получим процент по показателям
			if (!empty($results_sum)) {
				foreach ($results_sum as $id_dp => $sum) {
					if ($data['begin'] == 2) {
						$data['percent_progress'][$id_dp] = $progress_sum[$id_dp] / $sum * 100;
					} elseif ($data['begin'] == 3 || $data['begin'] == 4) {
						foreach ($data['progress_levels3'] as $l) {
							$data['percent_progress'][$id_dp][$l] = round($progress_sum[$id_dp][$l] / $sum * 100);
						}
					}
				}
			}

			// теперь соберем проценты по группам показателей
			foreach ($data['dp'] as $dp) {
				foreach ($dp as $id_dp => $dp_data) {
					if (!empty($data['percent_progress'][$id_dp])) {
						if ($data['begin'] == 2) {
							$data['percent_progress_total'][$dp_data['id_gdp']] += $data['percent_progress'][$id_dp];
						} elseif ($data['begin'] == 3 || $data['begin'] == 4) {
							foreach ($data['progress_levels3'] as $l) {
								$data['percent_progress_total'][$dp_data['id_gdp']][$l] += $data['percent_progress'][$id_dp][$l];
							}
						}
					}
				}
			}

			// вычислим проценты в группах показателей
			foreach ($data['gdp'] as $k => $g) {
				$length = 0;
				foreach ($data['dp'][$k] as $dd) {
					if (in_array($data['begin']-1, $dd['classes'])) {
						$length += 1;
					}
				}

				if ($length) {
					if ($data['begin'] == 2) {
						$data['percent_progress_total'][$k] = round($data['percent_progress_total'][$k] / $length);
					} elseif ($data['begin'] == 3 || $data['begin'] == 4) {
						foreach ($data['progress_levels3'] as $l) {
							$data['percent_progress_total'][$k][$l] = round($data['percent_progress_total'][$k][$l] / $length);
						}
					}
				}
			}
		} elseif ($data['begin'] == 5) {
			//$data['progress_levels5'] = ['1', '1*'];
			$data['progress_levels5'] = ['s1', 's0'];
			$data['progress_levels5_labels'] = ['s1'=>'1+1* (наличие прогресса)', 's0' => '0+0* (отсутствие прогресса)'];
			$data['progress_levels5_colors'] = ['CCFFCC','FFCC99'];

			// подсчитаем сумму прогрессов по всем ученикам
			foreach ($data['subgroups_progress'] as $student) {
				foreach ($student as $id_sub => $progress) {
					if (!is_null($progress)) {
						$progress_sum[$id_sub][$progress]++;
						$results_sum[$id_sub]++;
					}
				}
			}

			foreach ($progress_sum as $idGroup => $sGroup){
                $progress_sum_s[$idGroup]['s1'] = $sGroup['1'] + $sGroup['1*'];
                $progress_sum_s[$idGroup]['s0'] = $sGroup['0'] + $sGroup['0*'];
            }

			// разделим сумму прогрессов на количество учеников, получим процент по показателям
			if (!empty($results_sum)) {
				foreach ($results_sum as $id_sub => $sum) {
					foreach ($data['progress_levels5'] as $l) {
						$data['percent_progress'][$id_sub][$l] = round($progress_sum_s[$id_sub][$l] / $sum * 100);
					}
				}
			}

			// теперь соберем проценты по группам показателей
			foreach ($data['subgroups'][$data['begin']] as $k => $sub) {
				foreach ($sub as $id_sub => $sub_data) {
					if (!empty($data['percent_progress'][$id_sub])) {
						foreach ($data['progress_levels5'] as $l) {
							$data['percent_progress_total'][$k][$l] += $data['percent_progress'][$id_sub][$l];

						}
					}
				}
			}

			// вычислим проценты в группах показателей
			foreach ($data['gdp'] as $k => $g) {
				foreach ($data['progress_levels5'] as $l) {
					$data['percent_progress_total'][$k][$l] = round($data['percent_progress_total'][$k][$l] / count($data['subgroups'][$data['begin']][$k]));
				}
			}
		}

		if ($_GET['mode'] == 'excel') {
			$data['html'] = $this->layout_get("uud/report_progress_uud.html", $data);

			/** @var  $excel \results\excel */
			$excel = $this->get_controller('results', 'excel');
			$data['filename'] = 'Прогресс ' . $data['begin'] . ' класс';
			$excel->report_any_table($data);
		} else {
			$this->layout_show('uud/progress.html', $data);
		}
	}

	function set_reflex(&$data)
	{
		$ignore = 74;
		$plus = 81;
		$b_summ = [];

		/** @var  $dp_ctr \dp\dp */
		$dp_ctr = $this->get_controller('dp');
		$max_values = $dp_ctr->get_max_uug_dp_fields_values();

		foreach ($data['results'] as $id_student => $s_data) {
			foreach ($s_data as $number_class => $ss_data) {
				if ($number_class == $data['begin']) {
					foreach ($ss_data as $id_dp => $sss_data) {
						if ($id_dp == 'total' || $id_dp == $ignore) {
							continue;
						}

						if (isset($max_values[$id_dp][$data['begin']]['Б']) && $max_values[$id_dp][$data['begin']]['Б'] == $sss_data['Б']) {
							$b_summ[$id_student]++;
						}

						if ($id_dp == $plus) {
							if (isset($max_values[$id_dp][$data['begin']]['9']) && $max_values[$id_dp][$data['begin']]['9'] == $sss_data[9]) {
								$b_summ[$id_student]++;
							}
						}
					}
				}
			}
		}

		$data['reflex'] = [];
		foreach ($data['students'] as $student) {
			$data['reflex'][$student['id']] = round($b_summ[$student['id']] / 14, 2);
		}
	}

	function reflex($data = [])
	{
		crumbs('Рефлексивность');

		/** @var  $students_ctr \students\students */
		$students_ctr = $this->get_controller("students");
		$data['students'] = $students_ctr->get_students_from_ug($this->_0, $data['begin']);

		/** @var  $results_ctr \results\results */
		$results_ctr = $this->get_controller("results");
		list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($this->_0, true, $data['begin']);

		$this->set_reflex($data);

		if ($_GET['mode'] == 'excel') {
			$data['html'] = $this->layout_get("uud/report_reflex.html", $data);

			/** @var  $excel \results\excel */
			$excel = $this->get_controller('results', 'excel');
			$data['filename'] = 'Рефлексивность ' . $data['begin'] . ' класс';
			$excel->report_any_table($data);
		} else {
			$this->layout_show('uud/reflex.html', $data);
		}
	}

	function import_download($data = []) {
		/** @var  $excel \results\excel */
		$excel = $this->get_controller('results', 'excel');
		$excel->import_download($data);
	}
}
