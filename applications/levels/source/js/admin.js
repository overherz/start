$(document).ready(function() {
    $(document).on("click",".del-btn",function(){
        if(!confirm('Удалить?'))return false;
        var id = $(this).data('id');
        user_api({act:'delete',id:id},function(){
            $("tr[data-id='"+id+"']").remove();
            show_message("success","Успешно удалено");
        });
        return false;
    });

    $(document).on("click",".add-btn",function(){
        user_api({act:'add'},function(data){
            show_popup(data,"Добавление");
            add_popup_button("Сохранить",'save');
        });
    });

    $(document).on("click",".edit-btn",function(){
        var id = $(this).data('id');

        user_api({act:'edit',id:id},function(data){
            show_popup(data,"Редактирование");
            add_popup_button("Сохранить",'save');
        });
    });

    $(document).on("click","[save]",function(){
        var params = $("#edit_form").find(":input").serialize();
        user_api(params,function(res){
            user_api(false,function(data){
                $("#search_result").html(data);
            });
            show_message("success","Сохранено");
            hide_popup();
        });
    });
});