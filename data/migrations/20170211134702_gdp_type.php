<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class GdpType extends AbstractMigration
{
    const TABLE = 'gdp';

    private $data = [
        [
            'name' => 'Регулятивные УУД',
            'type' => 2,
        ],
        [
            'name' => 'Познавательные УУД',
            'type' => 2,
        ],
        [
            'name' => 'Коммуникативные',
            'type' => 2,
        ],
    ];

    public function up()
    {
        $table = $this->table(self::TABLE);
        $table->addColumn('type', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addIndex(['type'])
            ->save();

        $this->execute('UPDATE ' . self::TABLE . ' SET type = 1');

        $this->insert(self::TABLE, $this->data);
    }

    public function down()
    {
        $table = $this->table(self::TABLE);
        $table->removeColumn('type')
            ->save();
    }
}
