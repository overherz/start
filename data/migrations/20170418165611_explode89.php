<?php

use Phinx\Migration\AbstractMigration;

class Explode89 extends AbstractMigration
{
	public function up()
	{
		$this->query('UPDATE dp_to_class
			set `fields` = "9,11"
            WHERE  class= 5 AND `fields` = "8;9"
            LIMIT 1
        ');
	}
}
