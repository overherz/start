<?php
namespace admin\students;

class export extends \Admin {

    function default_method()
    {
        /*
        $sheet->setCellValueByColumnAndRow($col,$row,"Личностная готовность");
        $sheet->mergeCellsByColumnAndRow($col,$row,$col,$row+3);
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setTextRotation(90)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
         */

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_all();

        $query = $this->db->query("SELECT dp.*,gdp.name AS gdp_name
                FROM dp AS dp
                LEFT JOIN gdp AS gdp ON dp.id_gdp=gdp.id
                ORDER BY position ASC
            ");
        while ($row = $query->fetch()) {
            $data['gdp'][$row['id_gdp']] = array('name' => $row['gdp_name'], 'id' => $row['id_gdp']);
            $data['dp'][$row['id_gdp']][] = $row;
        }

        if (count($data['gdp']) > 0) {
            $query = $this->db->query("SELECT * FROM levels_gdp");
            while ($row = $query->fetch()) {
                $data['gdp'][$row['id_gdp']]['levels'][] = $row;
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results'] = $results_ctr->get_all_result()) {
            $data['total'] = array();
            foreach ($data['results'] as $result) {
                foreach ($result as $k => $r) {
                    $data['total'][$k] += $r;
                }
            }
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        if ($data['results1'] = $results_ctr->get_all_result(true)) {
            $data['total1'] = array();
            foreach ($data['results1'] as $k => $result) {
                foreach ($result as $rk => $r) {
                    foreach ($r as $v) {
                        $data['total1'][$k][$rk] += $v;
                    }
                }
            }
        }

        /** @var  $report_ctr \results\report */
        $report_ctr = $this->get_controller("results","report");

        foreach ($data['students'] as &$s) {
            foreach ($data['gdp'] as $gdp) {
                foreach ($gdp['levels'] as $level) {
                    $value = $data['total1'][$s['id']][$gdp['id']];
                    if ($value >= $level['min_value'] && $value <= $level['max_value']) {
                        $s[$gdp['id']]['level_gdp'] = $level;
                        $s['levels'][] = $level['id'];
                    }
                }
            }
            $s['group'] = $report_ctr->detect_group($s['levels']);
        }

        $teachers = array();
        $groups = array();

        /** @var  $ug_ctr \admin\ug\ug */
        $ug_ctr = $this->get_controller("ug","ug",true);

        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");

        $query = $this->db->query("select * from kug");
        while ($row = $query->fetch()) $kug[$row['id']] = $row['name_group'];

        $xls = new \PHPExcel();
        $sheet = $xls->setActiveSheetIndex(0);
        \PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

        $row = 1;
        $col = 0;

        $sheet->setCellValueByColumnAndRow($col,$row,"Id учащегося");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"ФИО учащегося");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Группа стартовой готовности");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;

        foreach ($data['gdp'] as $k => $g)
        {
            foreach ($data['dp'][$k] as $dd)
            {
                $sheet->setCellValueByColumnAndRow($col,$row,$dd['short_name']);
                $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $col++;
            }
        }

        $sheet->setCellValueByColumnAndRow($col,$row,"Педагог (ФИО)");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Регион");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Город");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Населенный пункт");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Образовательная организация");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Дата проведения диагностики");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Учебная группа");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"Категория УГ");
        $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($col).$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $col++;
        $sheet->setCellValueByColumnAndRow($col,$row,"УМК");

        $row++;

        foreach ($data['students'] as $ss)
        {
            $col = 0;
            $sheet->setCellValueByColumnAndRow($col,$row,$ss['id']);
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,$ss['fio']);
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,$ss['group']);
            $col++;

            foreach ($data['gdp'] as $k => $g)
            {
                foreach ($data['dp'][$k] as $dd)
                {
                    $sheet->setCellValueByColumnAndRow($col,$row,$data['results'][$data['students'][$ss['id']]['id']][$dd['id']]);
                    $col++;
                }
            }

            if (!$groups[$ss['id_study_group']]) $groups[$ss['id_study_group']] = $ug_ctr->get($ss['id_study_group']);
            if (!$teachers[$groups[$ss['id_study_group']]['owner']]) $teachers[$groups[$ss['id_study_group']]['owner']] = $user_ctr->get_user($groups[$ss['id_study_group']]['owner']);

            $sheet->setCellValueByColumnAndRow($col,$row,$teachers[$groups[$ss['id_study_group']]['owner']]['fio']);
            $col++;
            $region = $user_ctr->get_region($teachers[$groups[$ss['id_study_group']]['owner']]['region']);
            $sheet->setCellValueByColumnAndRow($col,$row,$region['formalname']);
            $col++;

            $city = $user_ctr->get_city($teachers[$groups[$ss['id_study_group']]['owner']]['city'],$teachers[$groups[$ss['id_study_group']]['owner']]['region'],$teachers[$groups[$ss['id_study_group']]['owner']]['areacode']);
            $sheet->setCellValueByColumnAndRow($col,$row,$city['formalname']);
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,$teachers[$groups[$ss['id_study_group']]['owner']]['locality']);
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,$groups[$ss['id_study_group']]['name_education']);
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,date("d.m.Y",$groups[$ss['id_study_group']]['date']));
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,$groups[$ss['id_study_group']]['name']);
            $col++;

            $umk = array();
            $kugs = array();
            foreach ($groups[$ss['id_study_group']]['umk'] as $item) {
                $umk[] = $item['name'];
                $kugs[] = $kug[$item['id_study_group_category']];
            }

            $kugs = array_unique($kugs);

            $sheet->setCellValueByColumnAndRow($col,$row,implode("; ",$kugs));
            $col++;
            $sheet->setCellValueByColumnAndRow($col,$row,implode("; ",$umk));

            $row++;
        }

      //  pr($teachers);

        //$sheet = $report_ctr->autosize($sheet);

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename='Группы стартовой готовности {$data['ug']['name']}.xls'");

        $objWriter = new \PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');

    }
}

