<?php

use Phinx\Migration\AbstractMigration;

class DpGroups extends AbstractMigration
{
    const TABLE = 'results_uud_group';

    public function up()
    {
        $table = $this->table(self::TABLE);
        $table
            ->addColumn('id_student', 'integer')
            ->addColumn('class', 'integer')
            ->addColumn('id_dp', 'integer')
            ->addColumn('value', 'integer')
            ->addIndex(array('id_student', 'class', 'id_dp'))
            ->addForeignKey('id_student', 'students', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
            ->addForeignKey('id_dp', 'dp', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
            ->save();
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
