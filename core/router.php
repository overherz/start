<?php

class Router {

    static private $application;
    static private $controller;
    static private $id;
    static private $application_before_rules;
    static private $controller_before_rules;
    static private $admin;
    static private $url;
    private $second;
    static private $route = array();

    function __construct()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = self::url_parse($url);
        Router::$url = $url;
		\Controller::set_global('router_path', $url);

        $routeArray = explode('/', $url);
        self::$route = array();
        foreach ($routeArray as &$value) if (!empty($value)) self::$route[] = trim($value);

        if(defined('SUBDOMAINS') && SUBDOMAINS && self::$route[0] != 'admin')
        {
            $excluded_app = explode(",",SUBDOMAIN_APP_EXCLUDED);
            if (self::$route[0] == "") Controller::redirect("/".SUBDOMAIN_DEFAULT);
            if (!in_array(self::$route[0],$excluded_app))
            {
                $subdomain = array_shift(self::$route);
                define('SUBDOMAIN', $subdomain);
                define('WEBROOT',$_SERVER['HTTP_HOST']."/".$subdomain."/");
            }
            else define(strtoupper('webroot'),$_SERVER['HTTP_HOST']."/");
        }
        else define(strtoupper('webroot'),$_SERVER['HTTP_HOST']."/");

        if (self::$route[0] == 'admin') {
            self::$admin = true;
            array_shift(self::$route);
            define('ADMIN',true);
        }

        if (self::$route[0])
        {
            if (substr(self::$route[0],0,1) == "~")
            {
                self::$id = substr(self::$route[0], 1);
                self::$application = "index";
            }
            else self::$application = self::$route[0];

            if (self::$route[1] && self::$route[2])
            {
                if (substr(self::$route[1],0,1) == "~") self::$id = substr(self::$route[1], 1);
                elseif (preg_match("/^[0-9]+$/",self::$route[1])) self::$id = self::$route[1];
                else {
                    self::$controller = self::$route[1];
                    self::$id = self::$route[2];
                    $this->second = self::$route[2];
                    unset(self::$route[2]);
                }
            }
            else if (self::$route[1])
            {
                if (substr(self::$route[1],0,1) == "~") self::$id = substr(self::$route[1], 1);
                elseif (preg_match("/^[0-9]+$/",self::$route[1])) self::$id = self::$route[1];
                else self::$controller = self::$route[1];
            }
        }
        else self::$application = "index";

        if (!self::$controller) self::$controller = self::$application;

        if (substr(self::$id,0,1) == "~") self::$id = substr(self::$id, 1);

        unset(self::$route[0]);
        unset(self::$route[1]);
        $this->rules();

        $g = new \Global_module();
        $g->run(self::$admin);

        $this->global_rules();
        $this->get_application();
    }

    static function url_parse($url,$with_get=false)
    {
        $new_url = $url;
        if(strpos($url,'?')) $question = true;
        if ($question) $new_url = substr($url,0,strpos($url,'?'));
        if (substr($new_url, -1) != "/") $new_url .= "/";
        if ($with_get && $question) $new_url .= substr($url,strpos($url,'?'),mb_strlen($url));
        return $new_url;
    }

    private function global_rules()
    {
        $path = ROOT."applications".DS;
        if(file_exists("{$path}/config.php") && is_file("{$path}/config.php")) require_once("{$path}/config.php");
        if (count($router_rules) > 0)
        {
            if (array_key_exists(self::$application."_".self::$controller,$router_rules))
            {
                $rules = explode("_",$router_rules[self::$application."_".self::$controller]);
                if (count($rules) > 1)
                {
                    self::$application_before_rules = self::$application;
                    self::$controller_before_rules = self::$controller;
                    self::$application = $rules[0];
                    self::$controller = $rules[1];
                }
            }
        }
    }

    private function rules()
    {
        if (!self::$admin)
        {
            if (self::$url == "/") self::$url = "/index/";

            $result = \Cache::connect()->get('pages');
            if (!$result)
            {
                $result = \Controller::get_controller("pages")->update_cache_pages();
            }

            if ($result[self::$url])
            {
                self::$id = $result[self::$url]['id'];
                self::$application = "pages";
                self::$controller = "pages";
                \Controller::set_global('page_name',$result[self::$url]['name']);
            }
        }
    }

    function get_application()
    {
        if ($cr = Controller::get_controller(self::$application, self::$controller, self::$admin, self::$id, self::$route))
        {
            if (self::$admin) $cr->test_access();
            $cr->default_method();
        }
    }

    static function application()
    {
        return Router::$application;
    }

    static function set_route($application, $controller = false, $id = false) {
        self::$application = $application;
        if (!empty($controller)) {
            self::$controller = $controller;
        } else {
            self::$controller = $application;
        }
        if (!empty($id)) {
            self::$id = $id;
        }
    }

    static function controller()
    {
        return Router::$controller;
    }

    static function application_before_rules()
    {
        return Router::$application_before_rules;
    }

    static function controller_before_rules()
    {
        return Router::$controller_before_rules;
    }

    static function app_cont()
    {
        return Router::$application."_".Router::$controller;
    }

    static function id()
    {
        return Router::$id;
    }

    static function admin()
    {
        return Router::$admin;
    }

    static function url()
    {
        return Router::$url;
    }

    static function more($index=false)
    {
        if ($index) return Router::$route[$index];
        else return Router::$route;
    }

    static function url_array()
    {
        if ($ar = explode("/",\Router::$url))
        {
            foreach ($ar as &$arr)
            {
                if ($arr != "") $return_arr[] = $arr;
            }
            return $return_arr;
        }
    }

    static function get_info()
    {
        pr("app - ".self::$application);
        pr("contr - ".self::$controller);
        pr("id - ".self::$id);
    }
}