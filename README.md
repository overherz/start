# Разворачивание проекта #

скачать файлы или сделать git clone https://overherz@bitbucket.org/overherz/start.git . Если проект клонирован, то в корне выполнить команду 
```
#!php

git config core.filemode false
```

в корне проекта запустить 
```
#!php

composer update
```

если composer не установлен, то предварительно его поставить


```
#!php

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```


Накатить миграции

_Миграции надо накатывать только если база не импортирована_

```
#!php

vendor/bin/phinx migrate
```
Скопировать конфиг config.php_default в config.php

Скопировать конфиг phinx.yml_default в phinx.yml

