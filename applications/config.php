<?php

$menu_names = array(
    'options' => 'Управление сайтом',
    'content' => 'Управление контентом',
    'users' => 'Управление пользователями',
    'server' => 'Сервер',
	'start' => 'Старт',
	'uud' => 'УУД',
);

$router_rules = array(
    'projects_news' => 'projects_entities',
    'projects_wiki' => 'projects_entities',
    'projects_discus' => 'projects_entities',
);

