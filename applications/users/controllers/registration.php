<?php
namespace users;

class registration extends \Controller {

    function default_method()
    {
        if ($_POST['act'] == "check_for_activate_send") $this->check_for_activate_send();
        else if ($_GET['act'] == 'confirm') $this->mailconfirm();
        else if ($_GET['act'] == 'success') $this->success_registration();
        else $this->init_registration();
    }

    function success_registration()
    {
        $this->layout_show("success_registration.html");
    }

    function mailconfirm()
    {
        $this->db->beginTransaction();

        $query = $this->db->prepare("select * from recovery where hash=? and type='confirm_mail' LIMIT 1");
        $query->execute(array($_GET['confirmcode']));
        if ($user = $query->fetch())
        {
            $query = $this->db->prepare("UPDATE users SET mailconfirm = ? where email = ?");
            if ($query->execute(array(true,$user['email'])))
            {
                if (!$this->db->query("delete from recovery where hash=".$this->db->quote($_GET['confirmcode'])." and type='confirm_mail'")) $error = true;
                /** @var  $user_ctr \users\users */
                $user_ctr = $this->get_controller("users");
                $_SESSION['user'] = $user_ctr->get_user_from_email($user['email']);

                /** @var  $user_login_ctr \users\login */
                $user_login_ctr = \Controller::get_controller("users","login");
                $user_login_ctr->update_access();

                if (!$error) $this->db->commit();
                $this->redirect('/ug');
            }
            else $error = true;
        }
        else $error = true;

        if ($error) $this->db->rollBack();

        $this->layout_show('elements/mailconfirm.html', array('error' => $error));
    }

    function init_registration()
    {
        foreach ($_POST as $k => &$v)
        {
            if (!in_array($k,array('password1','password2'))) $v = trim($v);
        }

        $res = $this->validate_registration();

        if (!$res['error'])
        {
            $this->db->beginTransaction();

            $get_pass = get_pass($_POST['password1']);
            $pass = $get_pass['password'];
            $salt = $get_pass['salt'];

            $mailconfirm = false;

            $city = explode("|",$_POST['city']);

            $created = time();
            $data = array(
                $_POST['fio'],
                $_POST['email'],
                $pass,
                $salt,
                DEFAULT_USER_GROUP_ID,
                $mailconfirm,
                $created,
                $_POST['region'],
                $city[0],
                $city[1],
                $_POST['locality'],
                $_POST['name_oo']
            );

            $p = $this->db->prepare("insert into users(fio,email,pass,salt,id_group,mailconfirm,created,region,city,areacode,locality,name_oo) values(?,?,?,?,?,?,?,?,?,?,?,?)");
            if (!$p->execute($data)) $res['error']['global'] = "Ошибка базы данных";

            if (!$res['error'] && !$mailconfirm)
                if (!$this->send_activate_link($_POST['email'],$_POST['password1'])) $res['error'] = "Ошибка при отправке письма";

            if ($res['error']) $this->db->rollBack();
            else
            {
                $res['success'] = true;
                $this->db->commit();
            }
        }
        echo json_encode($res);
    }

    function validate_registration()
    {
        // Проверка паролей
        if ($_POST['password1'] == "") $res['error']['password1'] = "Укажите пароль";
        else if ($_POST['password2'] == "") $res['error']['password2'] = "Подтвердите пароль";
        else if ($_POST['password1'] != $_POST['password2'])
        {
            $res['error']['password1'] = $res['error']['password2'] = "Пароли не совпадают";
        }
        elseif (mb_strlen($_POST['password1']) < 6)
        {
            $res['error']['password1'] = "Пароль не должен быть короче 6 символов";
        }

        /** @var  $user_ctr  \users\users */
        $user_ctr = $this->get_controller('users');
        // Проверка адреса почты
        if ($_POST['email'] == "") $res['error']['email'] = "Укажите Email";
        else if (!preg_match(iconv("utf-8","windows-1251",'/^[а-яa-z0-9]{1}[а-яa-z0-9_\-\.]{1,30}@([а-яa-z0-9\-]{1,30}\.{0,1}[а-яa-z0-9\-]{1,5}){1,3}\.[а-яa-z]{2,5}$/i'),mb_strtolower(iconv("utf-8","windows-1251",$_POST['email'])))) $res['error']['email'] = "Адрес почты неверен";
        else if ($user_ctr->get_user_from_email($_POST['email'])) $res['error']['email'] = 'Такой пользователь уже существует';

        $query = $this->db->prepare("select * from users where email=? LIMIT 1");
        $query->execute(array($_POST['email']));
        $result  = $query->fetch();
        if ($result && $result['mailconfirm']) $res['error']['email'] = "адрес занят";

        // Проверка имени
        if ($_POST['fio'] == "") $res['error']['fio'] = "Введите ФИО";

        /** @var  $user_ctr \users\users */
        $user_ctr = $this->get_controller("users");

        if ($_POST['name_oo'] == "") $res['error']['name_oo'] = "Введите название образовательного учреждения";
        if (!$user_ctr->get_region($_POST['region'])) $res['error']['region'] = 'Укажите регион';

        if ($_POST['city'] == "" && $_POST['locality'] == "")
        {
            $res['error']['city'] = "Укажите город или населенный пункт";
        }

        if (!$_POST['agree']) $res['error']['agree'] = "Подтвердите согласие с условиями";
        return $res;
    }

    function check_for_activate_send()
    {
        $query = $this->db->prepare("select email from users where email=? and mailconfirm='0'");
        $query->execute(array($_POST['email']));
        if ($query->fetch())
        {
            if (!$this->send_activate_link($_POST['email'])) $res['error'] = "При отправке письма возникла ошибка";
            else $res['success'] = true;
        }
        echo json_encode($res);
    }

    function send_activate_link($email,$password=false,$subdomain=false)
    {
        $get_pass = get_pass(uniqid());
        $salt = $get_pass['salt'];
        $code = md5(md5(time()).$salt);

        $query = $this->db->prepare("insert into recovery(email,hash,date,type) values(?,?,?,?) ON DUPLICATE KEY UPDATE hash=?");
        if ($query->execute(array($email,$code,time(),'confirm_mail', $code)))
        {
            $subject = "Регистрация";
            $message = $this->layout_get("elements/activate_mail.html",array(
                'domain' => get_full_domain_name($subdomain),
                'email' => $email,
                'password' => $password,
                'code' => $code
            ));
            if (send_mail(get_setting('email'), $email, $subject, $message, get_setting('site_name'))) return true;
        }
    }
}
