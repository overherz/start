<?php
namespace tasks;

require_once(__DIR__ .'../../../../index.php');

class clear_not_mailconfirm extends \Controller {

    private $id;

    function __construct($id,$key)
    {
        /** @var $task_cr \tasks\tasks */
        $task_cr = $this->get_controller("tasks");
        /** @var  $task_error_cr \tasks\error */
        $task_error_cr = $this->get_controller("tasks","error");
        if ($task = $task_cr->get_task($id))
        {
            $task_cr->get_options();
            $pid = getmypid();

            if ($key == get_setting('cron_key') && !$task_cr->is_process_running($task['pid']))
            {
                $this->id = $task['id'];
                $GLOBALS['cli_task_id'] = $this->id;
                $task_cr->set_status($id,"run",$task['period'],$pid);
                /* @var $company_ctr \company\company */
                $company_ctr = $this->get_controller("company","company");
                $company_ctr->clear_not_mailconfirm();
                $task_cr->set_status($id,"stand");
            }
        }
        else
        {
            $task_error_cr->set_error("Неверный ключ",$this->id);
        }
    }
}

new clear_not_mailconfirm($argv[1],$argv[2]);
