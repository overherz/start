<?php

use Phinx\Migration\AbstractMigration;

class FloatFields extends AbstractMigration
{
	const
		TABLE_RESULTS = 'results_uud',
		TABLE_DP_TO_CLASS = 'dp_to_class';

    public function up()
    {
		$table = $this->table(self::TABLE_RESULTS);
		$table->changeColumn('value', 'decimal', ['precision' => 2, 'scale' => 1, 'signed' => true])
			->save();

		$table = $this->table(self::TABLE_DP_TO_CLASS);
		$table->addColumn('float_fields', 'string')
			->save();

		$this->query('UPDATE dp_to_class
			set float_fields = "В"
            WHERE class = 3 AND fields = "А,Б,В"
        ');
    }

    public function down()
	{
		$table = $this->table(self::TABLE_RESULTS);
		$table->changeColumn('value', 'integer')
			->save();

		$table = $this->table(self::TABLE_DP_TO_CLASS);
		$table->removeColumn('float_fields')
			->save();
	}
}
