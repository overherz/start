<?php

namespace global_module;

class get_session extends \Global_module
{
    protected $type = \Global_module::TYPE_SITE;
    protected $on_ajax_not_run = false;

    function run_module()
    {
        $session_name = session_name();
        if ($_GET[$session_name])
        {
            if (session_id() && $_GET[$session_name] != session_id()) session_destroy();
            $_COOKIE[$session_name] = $_GET[$session_name];
            session_set_cookie_params(0, '/', get_cookie_domain());
            session_id($_GET[$session_name]);
            session_start();
        }

        $user_cr = \Controller::get_controller("users","login");

        if(defined('SITE_ACCESS_CHECK') && SITE_ACCESS_CHECK)
        {
            if(!session_id()) {
                session_set_cookie_params(0, '/', get_cookie_domain());
                session_start();
            }

            if (\Router::application() == "pages")
            {
                \Controller::set_global('user',$_SESSION['user']);
                return false;
            }
            if (\Router::application() == "uploader" && \Router::controller() == "uploader") return false;
            if (\Router::application() == "users" && \Router::controller() == "functions") return false;
            if (\Router::application() == "users" && \Router::controller() == "recovery") return false;
            if (\Router::application() == "users" && \Router::controller() == "registration") return false;
            if (\Router::application() == "captcha" && \Router::controller() == "get_image") return false;
            if (\Router::application() == "users" && ($_POST['act'] == "lost_pass" || $_POST['act'] == "get_lost_pass")) return false;
            if (\Router::application() == "captcha" && \Router::controller() == "captcha") return false;

            if (!array_key_exists('user',$_SESSION) && (\Router::application() != "users" || (\Router::application() == "users" && \Router::controller() != "login")))
            {
                if ($_COOKIE['login'] == "" || $_COOKIE['password'] == "" || !$user_cr->get_login($_COOKIE['login'],$_COOKIE['password']))
                {
                    if (defined(AJAX) && AJAX)
                    {
                        echo json_encode("LoginException");
                        exit();
                    }
                    else if (\Router::application() == 'start') {
                        \Router::set_route('users', 'login', 'start');
                    } else {
                        if (\Router::application() == 'index' && \Router::controller() == 'index') {
                            \Router::set_route('users', 'login');
                        } else {
                            \Controller::redirect(SITE_LOGIN_PAGE);
                        }
                    }
                }
            }

            if (array_key_exists('user', $_SESSION) && (\Router::application() == "start")) {
                \Router::set_route('index');
            }
        }

        /* @var $user_cr \users\login */
        $user_cr->update_access();
        \Controller::set_global('user',$_SESSION['user']);

        if ($_SESSION['user'] && !$_SESSION['user']['access_site']['authorization'] && $_SESSION['user']['id_group'] != 1)
            \Controller::get_controller("users","logout")->default_method();
    }
}
