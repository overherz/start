<?php
define('DS', DIRECTORY_SEPARATOR);
define(strtoupper('root'),dirname(__FILE__).DS);

require_once(ROOT.'config.php');
$GLOBALS['info'] = $INFO;

if (!session_id())
{
    session_set_cookie_params(0, '/', ".".$INFO['domain_name']);
    session_start();
}

header('Content-Type: text/html; charset=utf-8');
setlocale(LC_ALL, "ru_RU.UTF-8");
require_once('core/initdata.php');
