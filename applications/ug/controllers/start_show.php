<?php
namespace ug;

class start_show extends \Controller {
    
    function default_method()
    {
        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller("ug");
        if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $this->id)) {
            $this->error_page('denied');
        }

        $data['ug'] = $ug_ctr->get_ug($this->id);

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->id);

        $query = $this->db->prepare("select dp.*,gdp.name as gdp_name
            from dp as dp
            join gdp as gdp ON dp.id_gdp=gdp.id AND gdp.type=?
            order by position ASC
        ");

        $query->execute([TYPE_START]);

        while ($row = $query->fetch()) {
            $data['gdp'][$row['id_gdp']] = $row['gdp_name'];
            $data['dp'][$row['id_gdp']][] = $row;
        }

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        $data['results'] = $results_ctr->get_ug_result($this->id);

        crumbs("Мои группы","/ug/start");
        crumbs($data['ug']['name']);

        $this->layout_show("start_show.html",$data);
    }
}

