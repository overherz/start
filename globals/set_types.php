<?php

namespace global_module;

class set_types extends \Global_module
{
    protected $type = \Global_module::TYPE_BOTH;
    protected $on_ajax_not_run = false;

    function run_module()
    {
        define('TYPE_START', 1);
        define('TYPE_UUD', 2);

        \Controller::set_global("projects_map", [
            'start' => TYPE_START,
            'uud' => TYPE_UUD,
        ]);
    }
}
