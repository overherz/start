<?php
namespace results;

class results extends \Controller {

    // провайдер
    function get_student_result($id)
    {
        /** @var  $student_ctr \students\students */
        $student_ctr = $this->get_controller('students');
        $student = $student_ctr->get_student($id);
        if (!empty($student)) {
            /** @var  $ug_ctr \ug\ug */
            $ug_ctr = $this->get_controller("ug");
            $ug = $ug_ctr->get_ug($student['id_study_group']);

            if ($ug['type'] == TYPE_START) {
                return $this->get_student_result_start($id);
            } elseif ($ug['type'] == TYPE_UUD) {
                return $this->get_student_result_uud($id);
            }
        }
    }

    function get_student_result_start($id)
    {
        $results = array();

        $query = $this->db->prepare("select * from results where id_student=?");
        $query->execute(array($id));
        while ($row = $query->fetch()) {
            $results[$row['id_dp']] = $row['value'];
        }

        return $results;
    }

    function get_student_result_uud($id) {
        $results = array();

        $query = $this->db->prepare("select * from results_uud where id_student=?");
        $query->execute(array($id));
        while ($row = $query->fetch()) {
        	if ($row['value'] != '') {
				$results[$row['class']][$row['id_dp']][$row['field']] = $row['value'];
			}
        }

        return $results;
    }

    function get_all_result($group_gdp=false)
    {
        $results = array();

        /** @var  $student_ctr \students\students */
        $student_ctr = $this->get_controller("students");
        if ($students = $student_ctr->get_all()) {
            $ids = array();
            foreach ($students as $student)
            {
                $ids[] = $student['id'];
            }

            $query = $this->db->prepare("select r.*,dp.id_gdp
                from results as r
                left join dp as dp ON r.id_dp=dp.id"
            );
            $query->execute();
            while ($row = $query->fetch()) {
                if ($group_gdp) {
                    $results[$row['id_student']][$row['id_gdp']][$row['id_dp']] = $row['value'];
                } else {
                    $results[$row['id_student']][$row['id_dp']] = $row['value'];
                }
            }
        }

        return $results;
    }

    function get_ug_result($id, $group_gdp=false)
    {
        $results = array();

        /** @var  $student_ctr \students\students */
        $student_ctr = $this->get_controller("students");
        if ($students = $student_ctr->get_students_from_ug($id)) {
            $ids = array();
            foreach ($students as $student) {
                $ids[] = $student['id'];
            }

            $ids = implode(",",$ids);
            $query = $this->db->prepare("select r.*,dp.id_gdp
                from results as r
                left join dp as dp ON r.id_dp=dp.id
                where r.id_student IN ({$ids})"
            );
            $query->execute(array($id));
            while ($row = $query->fetch()) {
                if ($group_gdp) {
                    $results[$row['id_student']][$row['id_gdp']][$row['id_dp']] = $row['value'];
                } else {
                    $results[$row['id_student']][$row['id_dp']] = $row['value'];
                }
            }
        }

        return $results;
    }

    function get_ug_uud_result($id, $grouped = false, $number_class = false)
    {
        $results = [];
        $levels = [];
        $all_levels = [];

        $join = '';
        if (!empty($number_class)) {
        	$join = ' join dp_to_class as dc ON dc.id_dp = dp.id and dc.class=' . (int) $number_class;
		}

        /** @var  $student_ctr \students\students */
        $student_ctr = $this->get_controller("students");
        if ($students = $student_ctr->get_students_from_ug($id)) {
            $ids = array();
            foreach ($students as $student) {
                $ids[] = $student['id'];
            }

            $ids = implode(",", $ids);
            $query = $this->db->prepare("select r.*, dp.id_gdp
                from results_uud as r
                join dp as dp ON r.id_dp=dp.id
                {$join}
                where r.id_student IN ({$ids})
                "
            );
            $query->execute(array($id));
            while ($row = $query->fetch()) {
                $row['classes'] = explode(',', $row['classes']);
                if ($grouped) {
                    if ($row['class'] == 1) {
                        if (in_array($row['field'], ['Б', 'В'])) {
                            $row['field'] = 'БВ';
                        }
                    } elseif ($row['class'] == 2) {
                        if (in_array($row['field'], ['А', 'Б'])) {
                            $row['field'] = 'АБ';
                        }
                    }
                }

                if ($row['value'] != "")  {
					$results[$row['id_student']][$row['class']][$row['id_dp']][$row['field']] += $row['value'];
					$results[$row['id_student']][$row['class']][$row['id_dp']]['total'] += $row['value'];
					if ($results[$row['id_student']][$row['class']][$row['id_dp']]['total'] < 4) {
						$level = 0;
					} else {
						$level = 1;
					}

					$results[$row['id_student']][$row['class']][$row['id_dp']]['level'] = $level;
					$levels[$row['id_student']][$row['class']][$row['id_gdp']][$row['id_dp']] = $level;
					$all_levels[$row['id_student']][$row['class']][$row['id_dp']] = $level;

					$results[$row['id_student']][$row['class']]['total'] += $row['value'];
				}
            }
        }

        if (!empty($levels)) {
            foreach ($levels as $id_student => $student) {
                foreach ($student as $id_class => $class) {
                    foreach ($class as $id_dp => $levels1) {
                        $freq = array_count_values($levels1);
                        $level = $freq[1] > $freq[0] ? 1 : 0;
                        $levels[$id_student][$id_class][$id_dp]['total'] = $level;
                    }

                }
            }
        }

        if (!empty($all_levels)) {
            foreach ($all_levels as $id_student => $student) {
                foreach ($student as $id_class => $class) {
                    $freq = array_count_values($class);
                    arsort($freq);
                    $levels[$id_student][$id_class]['total'] = key($freq);
                }
            }
        }

        return [$results, $levels];
    }
	/**
	 * @param int $id_class id группы, 13765 например
	 * @param int|bool $number_class 1|2|3|4|5
	 * @param int|bool $id_student
	 * @return array
	 */
    function get_dp_groups($id_class, $number_class = false, $id_student = false)
    {
        $groups = [];
        $where = [];

		$where[] = 's.id_study_group=' . intval($id_class);

        if (!empty($number_class)) {
            $where[] = 'res.class=' . $this->db->quote($number_class);
        }
        if (!empty($id_student)) {
            $where[] = 'res.id_student=' . $this->db->quote($id_student);
        }

        if (!empty($where)) {
            $where = 'WHERE ' . implode(' AND ', $where);
        }

        $query = $this->db->query('
			SELECT res.* 
			FROM results_uud_group as res 
			JOIN students as s ON res.id_student = s.id ' . $where
		);

        while ($row = $query->fetch()) {
        	if ($row['value'] != "") {
				$groups[$row['id_student']][$row['class']][$row['id_dp']] = $row['value'];
			}
        }

        $groups_total = [];
        if (!empty($groups)) {
            foreach ($groups as &$student) {
                foreach ($student as $id_class => &$class) {
                    foreach ($class as $id_dp => $group) {
                        $groups_total[$id_class][$id_dp][] = $group;
                    }

                    $freq = array_count_values($class);
                    arsort($freq);
                    $key = key($freq);
                    $find_by_key = array_keys($freq, $freq[$key]);
                    sort($find_by_key);
                    $class['total'] = reset($find_by_key);
                }
            }
            unset($class);
            unset($student);
        }

        if (!empty($groups_total)) {
            foreach ($groups_total as $id_class => &$dps) {
                foreach ($dps as $id_dp => &$gr) {
                    $freq = array_count_values($gr);
                    arsort($freq);
                    $key = key($freq);
                    $find_by_key = array_keys($freq, $freq[$key]);
                    sort($find_by_key);
                    $gr['total'] = reset($find_by_key);
                }
            }
            unset($dps);
        }

        return [$groups, $groups_total];
    }
}

