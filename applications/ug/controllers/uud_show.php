<?php
namespace ug;

class uud_show extends \Controller {
    
    function default_method()
    {
        /** @var  $ug_ctr \ug\ug */
        $ug_ctr = $this->get_controller("ug");
        if (!$ug_ctr->check_access($_SESSION['user']['id_user'], $this->id)) {
            $this->error_page('denied');
        }

        $data['ug'] = $ug_ctr->get_ug($this->id);

		if ((int) $_GET['begin'] > 0) {
			$data['begin'] = (int) $_GET['begin'];
		} else {
			$data['begin'] = 1;
		}

        /** @var  $students_ctr \students\students */
        $students_ctr = $this->get_controller("students");
        $data['students'] = $students_ctr->get_students_from_ug($this->id, $data['begin']);

        /** @var  $dp_ctr \dp\dp */
        $dp_ctr = $this->get_controller('dp');
        $data += $dp_ctr->get_all_uud_dp();

        /** @var  $results_ctr \results\results */
        $results_ctr = $this->get_controller("results");
        list($data['results'], $data['levels']) = $results_ctr->get_ug_uud_result($this->id);

		$dp_ctr->filter_uud_data($data);

        /** @var  $report_ctr \results\report */
        $report_ctr = $this->get_controller('results', 'report');
        $data['labels'] = $report_ctr->getLabelsByClass($data['begin']);
        crumbs("Главная","/");
        crumbs("Учимся Учиться и Действовать","/ug/uud");
        crumbs($data['ug']['name'] . ". " . $data['begin'] . " класс");

        $data['inputResults'] = true;
        $this->layout_show("uud_show_all.html", $data);
    }
}

