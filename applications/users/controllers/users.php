<?php
namespace users;

class users extends \Controller {

    public $limit = 24;

    function default_method()
    {
        switch($_POST['act'])
        {
            case "lost_pass":
                $this->lost_pass();
                break;
            case "get_lost_pass":
                $this->get_lost_pass();
                break;
            default: $this->default_show();
        }
    }

    function default_show($id = false)
    {
        if ($id) $this->id = $id;
        $this->limit = get_setting('users_count_on_page',$this->limit);

        if (!$this->id)
        {
            $data = $this->users_search();

            if ($_POST)
            {
                $res['success'] = $this->layout_get("users_table.html",$data);
                echo json_encode($res);
            }
            else $this->layout_show('users.html',$data);
        }
        else
        {
            $id = intval($this->id);
            if ($id == $_SESSION['user']['id_user'])
            {
                if($_SESSION['user']['id_user'] == $id) $this->set_global("user_menu", "profile");
                $data = array($id);

                $result = $this->db->prepare("select u.fio,u.id_user,gr.id as id_group,gr.name as group_name,gr.color
                    from users as u
                    LEFT JOIN groups as gr ON u.id_group=gr.id
                    WHERE u.id_user = ? LIMIT 1
                ");
                $result->execute($data);
                if ($user = $result->fetch())
                {
                    $res2 = $this->db->prepare("select up.*, pr.* FROM userprofiles as up LEFT JOIN profile as pr ON up.idprof=pr.id WHERE up.iduser = ? and pr.share = '1'");
                    $res2->execute(array($id));
                    while ($row = $res2->fetch())
                    {
                        $user['profile'][$row['name']] = $row;
                    }

                    $this->layout_show('user.html', array(
                        'user'=> $user
                    ));
                }
                else $this->error_page();
            }
            else $this->error_page('denied');
        }
    }

    function get_user($id)
    {
        $query = $this->db->prepare("select * from users where id_user=?");
        $query->execute(array($id));
        $user = $query->fetch();

        return $user;
    }

    function get_user_from_email($email)
    {
        $query = $this->db->prepare("select * from users where email=?");
        $query->execute(array($email));
        $user = $query->fetch();

        return $user;
    }

    function lost_pass()
    {
        $res['success'] = $this->layout_get("lost_pass.html");
        echo json_encode($res);
    }

    function get_lost_pass()
    {
        $u_cr = $this->get_controller("users","recovery");
        $res = $u_cr->add_recovery($_POST['email']);

        echo json_encode($res);
    }

    function save_user_data($id_user,$id_key,$data)
    {
        $query = $this->db->prepare("insert into users_data(id_user,id_key,data) values(?,?,?) on duplicate key update data=?");
        if($query->execute(array($id_user,$id_key,$data,$data))) return true;
    }

    function get_user_data($id_user,$id_key)
    {
        $query = $this->db->prepare("select * from users_data where id_user=? and id_key=?");
        if ($query->execute(array($id_user,$id_key))) return $query->fetch();
    }

    function get_teachers()
    {
        $query = $this->db->query("select * from users where id_group=4");
        while ($row = $query->fetch())
        {
            $teachers[] = $row;
        }

        return $teachers;
    }

    function get_cities_region($region)
    {
        $query = $this->db->prepare("select * from d_fias_addrobj where regioncode=? and actstatus=1 and shortname='г' ORDER BY formalname ASC");
        $query->execute(array($region));

        if (defined('AJAX') && AJAX)
        {
            $html = "<option></option>";
            while ($row = $query->fetch())
            {
                $html .= "<option value='{$row['citycode']}|{$row['areacode']}'>{$row['formalname']}</option>";
            }

            $res['success'] = $html;
            echo json_encode($res);
        }
        else return $query->fetchAll();
    }

    function get_regions()
    {
        $query = $this->db->query("select * from d_fias_addrobj where aolevel=1 and actstatus=1 order by formalname ASC");
        return $query->fetchAll();
    }

    function get_region($region)
    {
        $query = $this->db->prepare("select * from d_fias_addrobj where regioncode=? and actstatus=1 and aolevel=1");
        $query->execute(array($region));
        return $query->fetch();
    }

    function get_city($city,$region,$areacode)
    {
        $query = $this->db->prepare("select * from d_fias_addrobj where regioncode=? and citycode=? and areacode=? and actstatus=1 and shortname='г'");
        $query->execute(array($region,$city,$areacode));
        return $query->fetch();
    }
}

