<?php
namespace admin\umk;

class umk extends \Admin {

    private $module_name = "УМК";
    private $limit = 20;

    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            case "add":
                $this->add();
                break;
            case "delete":
                $this->delete();
                break;
            case "edit":
                $this->edit();
                break;
            default: $this->default_show();
        }
    }

    function default_show()
    {
        if (isset($_POST['search']) && $_POST['search'] != '')
        {
            $s = $this->db->quote("%{$_POST['search']}%");
            $sql = "where u.name LIKE ".$s;
        }

        require_once(ROOT.'libraries/paginator/paginator.php');
        $data = $this->db->query("select SQL_CALC_FOUND_ROWS u.*,k.name_category_students
            from umk as u
            LEFT JOIN kug as k ON u.id_study_group_category=k.id
            {$sql}
            order by name_category_students,position
            LIMIT {$this->limit}
            OFFSET ".\Paginator::get_offset($this->limit,$_POST['page'])."
        ")->fetchAll();

        $paginator = new \Paginator($this->db->found_rows(), $_POST['page'], $this->limit);

        if (defined('AJAX') && AJAX)
        {
            $res['success'] = $this->layout_get('admin/table.html',array('data' => $data,'paginator' => $paginator));
            echo json_encode($res);
        }
        else $this->layout_show('admin/index.html',array('data' => $data,'paginator' => $paginator));
    }

    function add()
    {
        $res['success'] = $this->layout_get('admin/form.html',array('cats' => $this->get_controller('kug')->get_all()));
        echo json_encode($res);
    }

    function edit()
    {
        if ($_POST['id'] != "")
        {
            if ($element = $this->get($_POST['id']))
            {
                $res['success'] = $this->layout_get('admin/form.html',array("element" => $element,'cats' => $this->get_controller('kug')->get_all(),'mode' => 'edit'));
            }
        }
        else $res['error'] = "Переданые неверные данные";

        echo json_encode($res);
    }

    function save()
    {
        if ($_POST['name'] == '') $res['error'] = "Название не может быть пустым";
        if ($_POST['id_study_group_category'] == '') $res['error'] = "Категория учебной группы не может быть пустой";

        if (!$res['error'])
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            if ($element['name'] != $_POST['name'] && $_POST['id'] != "") $message = ". Название {$element['name']} изменено на \"{$_POST['name']}\"";

            $save_data = array(
                $_POST['name'],
                $_POST['id_study_group_category'],
                $_POST['position']
            );
            if ($_POST['id'] != "")
            {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE umk set name=?,id_study_group_category=?,position=? where id=?");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Отредактировано. \"{$element['name']}\"" . $message, $_SESSION['admin']['id_user']);
            }
            else
            {
                $query = $this->db->prepare("INSERT INTO umk(name,id_study_group_category,position) VALUES(?,?,?)");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Добавлено. \"{$_POST['name']}\"", $_SESSION['admin']['id_user']);
            }

            if (!$res['error']) $res['success'] = true;
        }

        echo json_encode($res);
    }

    function delete()
    {
        if ($_POST['id'] != "")
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            $query = $this->db->prepare("delete from umk where id=?");
            if ($query->execute(array($_POST['id'])))
            {
                $res['success'] = true;
                if ($log) $log->save_into_log("admin",$this->module_name,true,"Удалено. \"{$element['name']}\"",$_SESSION['admin']['id_user']);
            }
            else $res['error'] = "Ошибка удаления";
        }
        else $res['error'] = "Передан неверный id";

        echo json_encode($res);
    }

    function get($id)
    {
        $query = $this->db->prepare("select * from umk where id=?");
        $query->execute(array($id));
        $element = $query->fetch();

        return $element;
    }
}

