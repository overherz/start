<?php
namespace tasks;

use admin\black_list\black_list;

require_once(__DIR__ .'../../../../index.php');

class clear_login_ban extends \Controller {

    private $id;

    function default_method($id,$key)
    {
        /** @var $task_cr \tasks\tasks */
        $task_cr = $this->get_controller("tasks");
        /** @var  $task_error_cr \tasks\error */
        $task_error_cr = $this->get_controller("tasks","error");
        if ($task = $task_cr->get_task($id))
        {
            $task_cr->get_options();
            $pid = getmypid();

            if ($key == get_setting('cron_key') && !$task_cr->is_process_running($task['pid']))
            {
                $this->id = $task['id'];
                $GLOBALS['cli_task_id'] = $this->id;
                $task_cr->set_status($id,"run",$task['period'],$pid);
                $this->db = \MyPDO::connect();

                /** @var  $black_list_ctr black_list */
                $black_list_ctr = $this->get_controller("black_list",false,true);
                $black_list_ctr->delete_ban();

                $task_cr->set_status($id,"stand");
            }
        }
        else
        {
            $task_error_cr->set_error("Неверный ключ",$this->id);
        }
    }
}

/** @var  $run \tasks\clear_login_ban */
$run = \Controller::get_controller("tasks","clear_login_ban");
$run->default_method($argv[1],$argv[2]);
