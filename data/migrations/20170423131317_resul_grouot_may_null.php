<?php

use Phinx\Migration\AbstractMigration;

class ResulGrouotMayNull extends AbstractMigration
{
	const
		TABLE_RESULTS = 'results_uud_group';

	public function up()
	{
		$table = $this->table(self::TABLE_RESULTS);
		$table->changeColumn('value', 'integer', ['signed' => true, 'null' => true])
			->save();
	}

	public function down()
	{

	}
}
