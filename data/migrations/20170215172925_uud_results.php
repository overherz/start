<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class UudResults extends AbstractMigration
{
    const TABLE = 'results_uud';

    public function up()
    {
        $table = $this->table(self::TABLE);
        $table
            ->addColumn('id_student', 'integer')
            ->addColumn('class', 'integer')
            ->addColumn('id_dp', 'integer')
            ->addColumn('field', 'string')
            ->addColumn('value', 'integer')
            ->addIndex(array('id_student', 'class', 'id_dp', 'field'))
            ->addForeignKey('id_student', 'students', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
            ->addForeignKey('id_dp', 'dp', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
            ->save();
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
