<?php

use Phinx\Migration\AbstractMigration;

class RenameDps extends AbstractMigration
{
    public function up()
    {
		$this->query('UPDATE dp
			set name="Оценивание", short_name="Оценивание" 
            WHERE  id = 41
            LIMIT 1
        ');

		$this->query('UPDATE dp
			set name="Рефлексия", short_name="Рефлексия" 
            WHERE  id = 42
            LIMIT 1
        ');
    }
}
