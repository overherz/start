<?php
namespace admin\dp;

class dp extends \Admin {

    private $module_name = "Диагностические показатели";
    private $limit = 20;

    function default_method()
    {
        switch($_POST['act'])
        {
            case "save":
                $this->save();
                break;
            case "add":
                $this->add();
                break;
            case "delete":
                $this->delete();
                break;
            case "edit":
                $this->edit();
                break;
            default: $this->default_show();
        }
    }

    function default_show()
    {
        if (isset($_POST['search']) && $_POST['search'] != '')
        {
            $s = $this->db->quote("%{$_POST['search']}%");
            $sql = "where d.name LIKE ".$s;
        }

        require_once(ROOT.'libraries/paginator/paginator.php');
        $query = $this->db->prepare("select SQL_CALC_FOUND_ROWS d.*,g.name as gdp_name
            from dp as d
            JOIN gdp as g ON d.id_gdp=g.id AND g.type = ?
            {$sql}
            order by d.position ASC
            LIMIT {$this->limit}
            OFFSET ".\Paginator::get_offset($this->limit,$_POST['page'])."
        ");
        $types = \Controller::get_global("projects_map");
        $query->execute([$types[$this->id]]);
        $data = $query->fetchAll();

        $paginator = new \Paginator($this->db->found_rows(), $_POST['page'], $this->limit);

        if (defined('AJAX') && AJAX)
        {
            $res['success'] = $this->layout_get('admin/table.html',array('data' => $data,'paginator' => $paginator));
            echo json_encode($res);
        }
        else $this->layout_show('admin/index.html',array('data' => $data,'paginator' => $paginator));
    }

    function add()
    {
        $res['success'] = $this->layout_get('admin/form.html',array('groups' => $this->get_all_gdp($this->id), 'id' => $this->id));
        echo json_encode($res);
    }

    function get_all_gdp($type)
    {
        $types = \Controller::get_global("projects_map");
        $query = $this->db->prepare("select * from gdp WHERE type=?");
        $query->execute([$types[$type]]);
        return $query->fetchAll();
    }

    function edit()
    {
        if ($_POST['id'] != "")
        {
            if ($element = $this->get($_POST['id']))
            {
                $res['success'] = $this->layout_get('admin/form.html',array("element" => $element,'mode' => 'edit','groups' => $this->get_all_gdp($this->id), 'id' => $this->id));
            }
        }
        else $res['error'] = "Переданые неверные данные";

        echo json_encode($res);
    }

    function save()
    {
        if ($_POST['name'] == '') $res['error'] = "Название не может быть пустым";
        if ($_POST['short_name'] == '') $res['error'] = "Краткое название не может быть пустым";
        if ($_POST['id_gdp'] == '') $res['error'] = "ГДП не может быть пустой";

        if (!$res['error'])
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            if ($element['name'] != $_POST['name'] && $_POST['id'] != "") $message = ". Название {$element['name']} изменено на \"{$_POST['name']}\"";

            $save_data = array(
                $_POST['name'],
                $_POST['short_name'],
                $_POST['position'],
                $_POST['id_gdp']
            );
            if ($_POST['id'] != "")
            {
                $save_data[] = $_POST['id'];
                $query = $this->db->prepare("UPDATE dp set name=?,short_name=?,position=?,id_gdp=? where id=?");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Отредактировано. \"{$element['name']}\"" . $message, $_SESSION['admin']['id_user']);
            }
            else
            {
                $query = $this->db->prepare("INSERT INTO dp(name,short_name,position,id_gdp) VALUES(?,?,?,?)");
                if (!$query->execute($save_data)) $res['error'] = "Ошибка базы данных";
                elseif ($log) $log->save_into_log("admin", $this->module_name, true, "Добавлено. \"{$_POST['name']}\"", $_SESSION['admin']['id_user']);
            }

            if (!$res['error']) $res['success'] = true;
        }

        echo json_encode($res);
    }

    function delete()
    {
        if ($_POST['id'] != "")
        {
            $log = $this->get_controller("logs");
            $element = $this->get($_POST['id']);
            $query = $this->db->prepare("delete from dp where id=?");
            if ($query->execute(array($_POST['id'])))
            {
                $res['success'] = true;
                if ($log) $log->save_into_log("admin",$this->module_name,true,"Удалено. \"{$element['name']}\"",$_SESSION['admin']['id_user']);
            }
            else $res['error'] = "Ошибка удаления";
        }
        else $res['error'] = "Передан неверный id";

        echo json_encode($res);
    }

    function get($id)
    {
        $query = $this->db->prepare("select * from dp where id=?");
        $query->execute(array($id));
        $element = $query->fetch();

        return $element;
    }
}

