<?php

$GLOBALS['lang'] = array(
    // Общее
    'dictionary_not_found' => 'слово "%s" отсутствует в словаре',

    //проект
    'stats_tasks_all' => 'всего',
    'stats_tasks_new' => array('новая','новых','новых'),
    'stats_tasks_progress' => 'в процессе',
    'stats_tasks_closed' => array('завершенная','завершенных','завершенных'),
    'stats_tasks_rejected' => array('отклоненная','отклоненных','отклоненных'),
    'stats_tasks_open' => array('открытая','открытых','открытых'),
    'stats_news' => array('новость','новости','новостей'),
    'stats_files' => array('файл','файла','файлов'),
    'stats_users' => array('участник','участника','участников'),
    'stats_wiki' => array('документ','документа','документов'),
    'stats_discus' => array('обсуждение','обсуждения','обсуждений'),
    'stats_projects' => array('проект','проекта','проектов'),
    'stats_personal_tasks' => array('личная задача','личных задач','личных задач'),

    //'days' => array('день','дня','дней'),
    'days' => array('','',''),

    'month_1' => "Январь",
    'month_2' => "Февраль",
    'month_3' => "Март",
    'month_4' => "Апрель",
    'month_5' => "Май",
    'month_6' => "Июнь",
    'month_7' => "Июль",
    'month_8' => "Август",
    'month_9' => "Сентябрь",
    'month_10' => "Октябрь",
    'month_11' => "Ноябрь",
    'month_12' => "Декабрь",
    'day_1' => "Пн",
    'day_2' => "Вт",
    'day_3' => "Ср",
    'day_4' => "Чт",
    'day_5' => "Пт",
    'day_6' => "Сб",
    'day_7' => "Вс",

    //task's status
    'task_status_new' => 'новая',
    'task_status_in_progress' => 'в процессе',
    'task_status_closed' => 'завершена',
    'task_status_rejected' => 'отклоненная',
    'task_status_feedback' => 'обратная связь',

    //task's priority
    'task_priority_1' => 'низкий',
    'task_priority_2' => 'обычный',
    'task_priority_3' => 'высокий',
    'task_priority_4' => 'критический',

    'task_priority_label_1' => '<span class="label label-default">низкий</span>',
    'task_priority_label_2' => '<span class="label label-success">обычный</span>',
    'task_priority_label_3' => '<span class="label label-warning">высокий</span>',
    'task_priority_label_4' => '<span class="label label-danger">критический</span>',

    'log_task_change_name' => 'Название %s',
    'log_task_change_status' => 'Статус %s (<s>%s</s>)',
    'log_task_change_percent' => "%s%% (<s>%s%%</s>)",
    'log_task_change_priority' => "Приоритет %s (<s>%s</s>)",
    'log_task_change' => '%s',
    'log_task_create' => '%s',


    //logs types
    'type_project' => 'Проект',
    'type_task' => 'Задача',
    'type_file' => 'Файл',
    'type_news' => array('Новость','Новости','Новостей'),
    'type_wiki' => 'Wiki',
    'type_discus' => array('Обсуждение','Обсуждения','Обсуждений'),
    'type_comment' => 'Комментарий',
    'type_users' => 'Пользователи',

    'type_news_color' => 'black',
    'type_wiki_color' => 'foxtrot',
    'type_discus_color' => 'oscar',

    //roles
    'role_user' => 'участник',
    'role_manager' => 'менеджер',
    'role_admin' => 'администратор',

    "edit_news" => "Редактирование новости",
    "add_news" => "Создание новости",
    "news_not_found" => "Новостей нет",
    "edit_wiki" => "Редактирование документа",
    "add_wiki" => "Создание документа",
    "wiki_not_found" => "Документов нет",
    "edit_discus" => "Редактирование обсуждения",
    "add_discus" => "Создание обсуждения",
    "discus_not_found" => "Обсуждений нет",

    'comment' => array('комментарий','комментария','комментариев'),
    'new_message' => array('новое сообщение','новых сообщения','новых сообщений'),


    "PERFMON_DB_SERVER_TITLE" => "Монитор производительности: сервер БД",
    "PERFMON_STATUS_TITLE" => "Статистика сервера",
    "PERFMON_WAITS_TITLE" => "Статистика ожиданий сервера",
    "PERFMON_PARAMETERS_TITLE" => "Параметры сервера",
    "PERFMON_STATS_TITLE" => "Сбор статистики по объектам базы данных",
    "PERFMON_KPI_NAME" => "Показатель",
    "PERFMON_KPI_VALUE" => "Значение",
    "PERFMON_KPI_RECOMENDATION" => "Рекомендации",
    "PERFMON_KPI_NAME_VERSION" => "Версия",
    "PERFMON_KPI_REC_VERSION_OLD" => "Устаревшая версия MySQL. Обновитесь как можно скорее.",
    "PERFMON_KPI_REC_VERSION_OK" => "Эта версия MySQL поддерживается данной диагностикой.",
    "PERFMON_KPI_REC_VERSION_NEW" => "Эта версия MySQL не поддерживается данной диагностикой. Результаты могут быть неверными.",
    "PERFMON_KPI_NAME_UPTIME" => "Время",
    "PERFMON_KPI_VAL_UPTIME" => "%sд %sч %sм %sс",
    "PERFMON_KPI_REC_UPTIME_OK" => "Продолжительность работы сервера MySQL.",
    "PERFMON_KPI_REC_UPTIME_TOO_SHORT" => "Сервер MySQL проработал менее 24-х часов. Рекомендации могут быть не точными.",
    "PERFMON_KPI_NAME_QUERIES" => "Всего запросов к серверу",
    "PERFMON_KPI_REC_NO_QUERIES" => "Нет возможности диагностировать сервер не обслуживающий запросы.",
    "PERFMON_KPI_NAME_GBUFFERS" => "Глобальные буферы",
    "PERFMON_KPI_REC_GBUFFERS" => "Размер глобальных буферов (%s).",
    "PERFMON_KPI_NAME_CBUFFERS" => "Буферы подключений",
    "PERFMON_KPI_REC_CBUFFERS" => "Размер буфера одного подключения (%s).",
    "PERFMON_KPI_NAME_CONNECTIONS" => "Подключения",
    "PERFMON_KPI_REC_CONNECTIONS" => "Максимальное количество подключений (%s).",
    "PERFMON_KPI_NAME_MEMORY" => "Память",
    "PERFMON_KPI_REC_MEMORY" => "Максимально возможное использование памяти (Глобальные буферы + Буферы подключений * Подключения).<br> Убедитесь, что оно не превышает 85-90 процентов физической памяти сервера (за вычетом других процессов).",
    "PERFMON_KPI_NAME_MYISAM_IND" => "MyISAM индексы",
    "PERFMON_KPI_REC_MYISAM_IND" => "Размер MyISAM индексов.",
    "PERFMON_KPI_REC_MYISAM_NOIND" => "MyISAM индексы отсутствуют.",
    "PERFMON_KPI_REC_MYISAM4_IND" => "Нет возможности оценить размер индексов для MySQL версии ниже 5.",
    "PERFMON_KPI_NAME_KEY_MISS" => "Кеш индексов MyISAM (промахи)",
    "PERFMON_KPI_REC_KEY_MISS" => "Если показатель > 5%,  увеличить значение параметра %s (текущее значение: %s)",
    "PERFMON_KPI_NAME_QCACHE_SIZE" => "Кеш запросов (размер)",
    "PERFMON_KPI_REC_QCACHE_ZERO_SIZE" => "Включите кеширование запросов (установить значение параметра %s больше или равным %s, но не более %s).",
    "PERFMON_KPI_REC_QCACHE_TOOLARGE_SIZE" => "Размер кеша запросов (%s) более %s. Это может привести к падению производительности.",
    "PERFMON_KPI_REC_QCACHE_OK_SIZE" => "Размер кеша запросов (%s).",
    "PERFMON_KPI_NAME_QCACHE" => "Кеш запросов (эффективность)",
    "PERFMON_KPI_REC_QCACHE_NO" => "Кеш запросов не используется из-за отсутствия SELECT запросов.",
    "PERFMON_KPI_REC_QCACHE" => "Если эффективность использования кеша менее %s, то возможно требуется увеличить значение параметра %s (текущее значение: %s)",
    "PERFMON_KPI_NAME_QCACHE_PRUNES" => "Кеш запросов (вытеснения)",
    "PERFMON_KPI_CACHE_TYPE" => "Тип кеша",
    "PERFMON_KPI_REC_QCACHE_PRUNES" => "Количество запросов вытесненных из кеша (%s). Если значение быстро растет, то необходимо увеличить параметр %s (текущее значение: %s), но не более чем до %s.",
    "PERFMON_KPI_NAME_SORTS" => "Сортировки",
    "PERFMON_KPI_REC_SORTS" => "Общее количество сортировок (%s).",
    "PERFMON_KPI_NAME_SORTS_DISK" => "Сортировки (диск)",
    "PERFMON_KPI_REC_SORTS_DISK" => "Процент сортировок потребовавших создания временной таблицы на диске (%s). Если процент более %s, то требуется увеличить параметры %s (текущее значение: %s) и %s (текущее значение: %s).",
    "PERFMON_KPI_NAME_JOINS" => "Select_range_check + Select_full_join",
    "PERFMON_KPI_REC_JOINS" => "Количество объединений таблиц не использующих индексы. (%s). Если значение большое, то требуется увеличить параметр %s (текущее значение: %s) или добавить индексы для объединения таблиц.",
    "PERFMON_KPI_NAME_TMP_DISK" => "Временные таблицы (диск)",
    "PERFMON_KPI_REC_TMP_DISK_1" => "Процент временных таблиц потребовавших создание на диске (%s). Процент более %s и требуется увеличить параметры %s (текущее значение: %s) и %s (текущее значение: %s). Убедитесь, что значения этих параметров равны. Так же возможно требуется сократить количество SELECT DISTINCT запросов без LIMIT.",
    "PERFMON_KPI_REC_TMP_DISK_2" => "Процент временных таблиц потребовавших создание на диске (%s). Процент более %s и размеры временной таблицы достаточно большие. Возможно требуется сократить количество SELECT DISTINCT запросов без LIMIT.",
    "PERFMON_KPI_REC_TMP_DISK_3" => "Процент временных таблиц потребовавших создание на диске (%s) достаточно низкий (не более %s).",
    "PERFMON_KPI_NAME_THREAD_CACHE" => "Кеш потоков",
    "PERFMON_KPI_REC_THREAD_NO_CACHE" => "Кеш потоков (%s) отключен. В качестве начального значение установите значения этого параметра равным %s.",
    "PERFMON_KPI_REC_THREAD_CACHE" => "Эффективность кеша потоков (%s). Если значение эффективности меньше %s, то требуется увеличить значение параметра %s (текущее значение: %s).",
    "PERFMON_KPI_NAME_TABLE_CACHE" => "Кеш открытых таблиц",
    "PERFMON_KPI_REC_TABLE_CACHE" => "Эффективность кеша открытых таблиц (%s). Если значение эффективности менее %s, то требуется увеличить значение параметра %s (текущее значение: %s). Увеличивайте параметр постепенно чтобы избежать превышения лимитов на количество одновременно открытых файлов в операционной системе.",
    "PERFMON_KPI_NAME_OPEN_FILES" => "Открытые файлы",
    "PERFMON_KPI_REC_OPEN_FILES" => "Процент открытых файлов (%s). Если более %s, то требуется увеличить параметр %s (текущее значение: %s).",
    "PERFMON_KPI_NAME_LOCKS" => "Блокировки",
    "PERFMON_KPI_REC_LOCKS" => "Процент блокировок полученных без ожидания в очереди (%s). Если меньше %s, то необходимо оптимизировать запросы или использовать InnoDB.",
    "PERFMON_KPI_NAME_INSERTS" => "Одновременные вставки",
    "PERFMON_KPI_REC_INSERTS" => "Одновременные (конкурентные) вставки отключены. Включите с помощью параметра %s = %s.",
    "PERFMON_KPI_NAME_CONN_ABORTS" => "Обрывы подключений",
    "PERFMON_KPI_REC_CONN_ABORTS" => "Процент соединений не закрытых корректно. Если таких соединений больше 5%%, то необходимо исправить приложение.",
    "PERFMON_KPI_NAME_INNODB_BUFFER" => "Буфер InnoDB",
    "PERFMON_KPI_REC_INNODB_BUFFER" => "Эффективность буфера InnoDB (%s). Если значение эффективности меньше %s, рассмотрите возможность увеличить параметр %s (текущее значение: %s).",
    "PERFMON_KPI_REC_INNODB_FLUSH_LOG" => "Значение параметра %s желательно должно быть равным %s.",
    "PERFMON_KPI_REC_INNODB_FLUSH_METHOD" => "Значение параметра %s желательно должно быть равным %s.",
    "PERFMON_KPI_REC_SYNC_BINLOG" => "Значение параметра %s желательно должно быть равным %s или быть не менее %s.",
    "PERFMON_KPI_REC_TX_ISOLATION" => "Значение параметра %s должно быть равным %s.",
    "PERFMON_KPI_EMPTY" => "пусто",
    "PERFMON_KPI_NO" => "нет",
    "PERFMON_KPI_NAME_INNODB_LOG_WAITS" => "Количество ожиданий буфера журнала",
    "PERFMON_KPI_REC_INNODB_LOG_WAITS" => "Если показатель > 0 и растет, увеличить значение параметра <span class=\"perfmon_code\">innodb_log_file_size</span> (текущее значение: %s). Внимание! Сначала остановить веб сервер. Затем mysql. Изменить значение параметра в файле настроек. Переместить существующие файлы журнала в сторону. Стартовать сервер. И если все прошло успешно, то удалить старые файлы журнала.",
    "PERFMON_KPI_NAME_BINLOG" => "Binlog_cache_disk_use",
    "PERFMON_KPI_REC_BINLOG" => "Если показатель > 0,  увеличить значение параметра <span class=\"perfmon_code\">binlog_cache_size</span> (текущее значение: %s)",
    "PERFMON_WAIT_EVENT" => "События ожидания сервера",
    "PERFMON_WAIT_PCT" => "Процент от общего времени",
    "PERFMON_WAIT_AVERAGE_WAIT_MS" => "Среднее время события (мс)",
    "PERFMON_PARAMETER_NAME" => "Параметр",
    "PERFMON_PARAMETER_VALUE" => "Фактическое значение",
    "PERFMON_REC_PARAMETER_VALUE" => "Рекомендуемое значение",
    "PERFMON_KPI_ORA_PERMISSIONS" => "Недостаточно прав для отображения статистики. Необходимые права: <span class=\"perfmon_code\">SELECT ANY DICTIONARY</span>.",
    "PERFMON_KPI_ORA_REC_DB_FILE_SEQUENTIAL_READ" => "Нормальное событие. При среднем времени чтения > 10 ms следует обратить внимание на производительность подсистемы ввода/вывода и размер буфера кеша <span class=\"perfmon_code\">SGA</span>. Сессия ожидает окончания процесса последовательного чтения.",
    "PERFMON_KPI_ORA_REC_DB_FILE_SCATTERED_READ" => "Нормальное событие. При среднем времени чтения > 15 ms следует обратить внимание на производительность подсистемы ввода/вывода и размер буферного кеша <span class=\"perfmon_code\">SGA</span>. Сессия ожидает окончания процесса последовательного чтения которое выполняется по несколько блоков за раз.",
    "PERFMON_KPI_ORA_REC_ENQ__TX___ROW_LOCK_CONTENTION" => "Требуется трассировка сессий. При начале транзакции получена исключительная блокировка на ресурс и она удерживается до конца транзакции.",
    "PERFMON_KPI_ORA_REC_LOG_FILE_SYNC" => "Нормальное событие. При среднем времени чтения > 10 ms следует обратить внимание на производительность подсистемы ввода/вывода и значение параметра <span class=\"perfmon_code\">COMMIT_WRITE</span>. В это время ожидания включена запись в журнал транзакций и ожидание ее успешного выполнения.",
    "PERFMON_KPI_ORA_REC_LATCH__CACHE_BUFFERS_CHAINS" => "Постоянный доступ к одному и тому же блоку данных или их небольшому количеству, известно как \"горячий блок\".",
    "PERFMON_KPI_ORA_REC_LATCH__LIBRARY_CACHE" => "Следует обратить внимание на параметр <span class=\"perfmon_code\">CURSOR_SHARING</span>, статистику по объектам и значение <span class=\"perfmon_code\">METHOD_OPT</span> при сборе статистики. Проблемы с этими защелками обычно связаны с не использованием биндов в запросах или недостаточным размером разделяемого кеша запросов. Возможные причины: много отличающихся запросов, запросы не используют бинд, недостаточный размер кеша курсоров в приложении, курсоры явно закрываются поле каждого выполнения, частые подключения и отключения к серверу, маленький размер Shared Pool.",
    "PERFMON_KPI_ORA_REC_ENQ__TX___INDEX_CONTENTION" => "Требуется трассировка сессий. При начале транзакции получена исключительная блокировка на ресурс и она удерживается до конца транзакции.",
    "PERFMON_KPI_ORA_REC_LOG_FILE_SWITCH_COMPLETION" => "Относится к скорости ввода/вывода. Ожидание переключения журнала транзакций.",
    "PERFMON_KPI_ORA_REC_SQL_NET_MORE_DATA_FROM_CLIENT" => "Относится к скорости клиент-серверных сообщений. Сервер отсылает клиенту сообщение, а он еще не ответил на предыдущее.",
    "PERFMON_KPI_ORA_REC_LATCH__SHARED_POOL" => "Следует обратить внимание на параметр <span class=\"perfmon_code\">CURSOR_SHARING</span>, статистику по объектам и значение <span class=\"perfmon_code\">METHOD_OPT</span> при сборе статистики. Проблемы с этими защелками обычно связаны с не использованием биндов в запросах или недостаточным размером разделяемого кеша запросов. Возможные причины: много отличающихся запросов, запросы не используют бинд, недостаточный размер кеша курсоров в приложении, курсоры явно закрываются поле каждого выполнения, частые подключения и отключения к серверу, маленький размер Shared Pool.",
    "PERFMON_KPI_ORA_REC_CURSOR__PIN_S_WAIT_ON_X" => "Следует обратить внимание на параметр <span class=\"perfmon_code\">CURSOR_SHARING</span>, статистику по объектам и значение <span class=\"perfmon_code\">METHOD_OPT</span> при сборе статистики. Сессия пытается получить разделяемую блокировку которая удерживается другой сессией.",
    "PERFMON_KPI_ORA_REC_BUFFER_BUSY_WAITS" => "Может быть следствием низкой скорости ввода/вывода. Ожидание момента когда блок данных станет доступным. Или блок читается другой сессией или находится в процессе изменения.",
    "PERFMON_KPI_ORA_REC_READ_BY_OTHER_SESSION" => "Может быть следствием низкой скорости I/O. Это событие возникает когда сессия запрашивает блок который уже читается в кеш другой сессией.",
    "PERFMON_KPI_ORA_REC_EVENTS_IN_WAITCLASS_OTHER" => "Проблема поддержки Oracle Битриксом.",
    "PERFMON_KPI_ORA_REC_ROW_CACHE_LOCK" => "Может быть следствием низкой скорости ввода/вывода. Сессия пытается получить блокировку метаданных.",
    "PERFMON_KPI_ORA_REC_DB_FILE_PARALLEL_READ" => "Может быть следствием низкой скорости I/O.",
    "PERFMON_KPI_ORA_REC_DB_BLOCK_CHECKSUM" => "Для ускорения операций выполняемых процессом <span class=\"perfmon_code\">DBWR</span>.",
    "PERFMON_KPI_ORA_REC_SESSION_CACHED_CURSORS" => "Рекомендовано специалистами Битрикс.",
    "PERFMON_KPI_ORA_REC_CURSOR_SHARING_FORCE" => "Типичное значение для приложений не использующих бинд переменных в запросах.",
    "PERFMON_KPI_ORA_REC_PARALLEL_MAX_SERVERS" => "Рекомендовано специалистами Битрикс.",
    "PERFMON_KPI_ORA_REC_COMMIT_WRITE" => "Для ускорения операций выполняемых процессом <span class=\"perfmon_code\">LGWR</span>.",
);
